﻿using System.Threading.Tasks;
using BobCommerce.Module.Catalog.Models;

namespace BobCommerce.Module.Catalog.Services {
    public interface IProductService {
        void Create(Product product);

        void Update(Product product);

        Task Delete(Product product);
    }
}
