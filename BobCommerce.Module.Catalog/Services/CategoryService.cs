﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Module.Catalog.Areas.Catalog.ViewModels;
using BobCommerce.Module.Catalog.Models;
using BobCommerce.Module.Core.Services;
using BobCommerce.Module.Core.Models;

namespace BobCommerce.Module.Catalog.Services {
    public class CategoryService : ICategoryService {
        private const string CategoryEntityTypeId = "Category";

        private readonly IRepository<Category> _categoryRepository;
        private readonly IEntityService _entityService;
        private readonly IMediaService _mediaService;
        private readonly IRepository<User> _userRepository;

        public CategoryService(IRepository<Category> categoryRepository, IEntityService entityService, IMediaService mediaService, IRepository<User> userRepository) {
            _categoryRepository = categoryRepository;
            _entityService = entityService;
            _mediaService = mediaService;
            _userRepository = userRepository;
        }

        public async Task<IList<CategoryListItem>> GetAll() {
            var categories = await _categoryRepository.Query().Where(x => !x.IsDeleted).ToListAsync();
            var categoriesList = new List<CategoryListItem>();
            foreach (var category in categories) {
                var categoryListItem = new CategoryListItem {
                    Id = category.Id,
                    IsPublished = category.IsPublished,
                    IncludeInMenu = category.IncludeInMenu,
                    Name = category.Name,
                    DisplayOrder = category.DisplayOrder,
                    ParentId = category.ParentId
                };

                var parentCategory = category.Parent;
                while (parentCategory != null) {
                    categoryListItem.Name = $"{parentCategory.Name} >> {categoryListItem.Name}";
                    parentCategory = parentCategory.Parent;
                }

                categoriesList.Add(categoryListItem);
            }

            return categoriesList.OrderBy(x => x.Name).ToList();
        }

        public async Task Create(Category category) {
            using (var transaction = _categoryRepository.BeginTransaction()) {
                category.Slug = _entityService.ToSafeSlug(category.Slug, category.Id, CategoryEntityTypeId);
                _categoryRepository.Add(category);
                await _categoryRepository.SaveChangesAsync();

                _entityService.Add(category.Name, category.Slug, category.Id, CategoryEntityTypeId);
                await _categoryRepository.SaveChangesAsync();

                transaction.Commit();
            }
        }

        public async Task Update(Category category) {
            category.Slug = _entityService.ToSafeSlug(category.Slug, category.Id, CategoryEntityTypeId);
            _entityService.Update(category.Name, category.Slug, category.Id, CategoryEntityTypeId);
            await _categoryRepository.SaveChangesAsync();
        }

        public async Task Delete(Category category) {
            await _entityService.Remove(category.Id, CategoryEntityTypeId);
            category.IsDeleted = true;
            _categoryRepository.SaveChanges();
        }

        public async Task<IList<CategoryMobile>> GetAllCategories(string lang) {
            //var user = _userRepository.Query().Where(x => x.Id == userId).FirstOrDefault();
            var isHeb = lang.ToLower() == "he-il" ? true : false;

            var categories = await _categoryRepository.Query().Include(x => x.ThumbnailImage).Where(x => !x.IsDeleted && x.ParentId == null).ToListAsync();
            var categoriesList = new List<CategoryMobile>();
            foreach (var category in categories) {
                var categoryListItem = new CategoryMobile {
                    i = category.Id,
                    n = (isHeb ?  category.NameHE : category.Name),
                    t = _mediaService.GetThumbnailUrl(category.ThumbnailImage),
                    co = category.Color,
                    ca = GetCategRecursively(category.Id)
                };
                categoriesList.Add(categoryListItem);
            }

            return categoriesList.OrderBy(x => x.n).ToList();
        }

        public List<CategoryMobile> GetCategRecursively(long? parentId) {
            List<CategoryMobile> retVal = new List<CategoryMobile>();
            var categories = _categoryRepository.Query().Include(x => x.ThumbnailImage).Where(x => !x.IsDeleted && x.ParentId == parentId).ToList();
            if (categories.Count > 0) {
                foreach (var category in categories) {
                    var categoryListItem = new CategoryMobile {
                        i = category.Id,
                        n = category.Name,
                        t = _mediaService.GetThumbnailUrl(category.ThumbnailImage),
                        co = category.Color,
                        ca = GetCategRecursively(category.Id)
                    };
                    retVal.Add(categoryListItem);
                }
                return retVal;
            }
            else {
                return null;
            }
            
        }

        public async Task<IList<CategoryMobile>> GetMainCategories(string lang) {
            //var user = _userRepository.Query().Where(x => x.Id == userId).FirstOrDefault();
            var isHeb = lang.ToLower() == "he-il" ? true : false;

            var categories = await _categoryRepository.Query().Include(x => x.ThumbnailImage).Where(x => !x.IsDeleted && x.ParentId == null).ToListAsync();
            var categoriesList = new List<CategoryMobile>();
            foreach (var category in categories) {
                var categoryListItem = new CategoryMobile {
                    i = category.Id,
                    n = (isHeb ? category.NameHE : category.Name),
                    t = _mediaService.GetThumbnailUrl(category.ThumbnailImage),
                    co = category.Color
                };

                categoriesList.Add(categoryListItem);
            }

            return categoriesList.OrderBy(x => x.n).ToList();
        }

        public async Task<IList<CategoryMobile>> GetSubCategories(long id, string lang) {
            //var user = _userRepository.Query().Where(x => x.Id == userId).FirstOrDefault();
            var isHeb = lang.ToLower() == "he-il" ? true : false;

            var categories = await _categoryRepository.Query().Include(x => x.ThumbnailImage).Where(x => !x.IsDeleted && x.ParentId == id).ToListAsync();
            var categoriesList = new List<CategoryMobile>();
            foreach (var category in categories) {
                var categoryListItem = new CategoryMobile {
                    i = category.Id,
                    n = (isHeb ? category.NameHE : category.Name),
                    t = _mediaService.GetThumbnailUrl(category.ThumbnailImage),
                    co = category.Color
                };

                categoriesList.Add(categoryListItem);
            }

            return categoriesList.OrderBy(x => x.n).ToList();
        }
    }
}
