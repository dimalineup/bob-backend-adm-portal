﻿using System.Threading.Tasks;
using BobCommerce.Module.Catalog.Models;

namespace BobCommerce.Module.Catalog.Services
{
    public interface IBrandService {
        Task Create(Brand brand);

        Task Update(Brand brand);

        Task Delete(long id);

        Task Delete(Brand brand);
    }
}
