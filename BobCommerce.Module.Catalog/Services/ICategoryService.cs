﻿using System.Threading.Tasks;
using System.Collections.Generic;
using BobCommerce.Module.Catalog.Areas.Catalog.ViewModels;
using BobCommerce.Module.Catalog.Models;

namespace BobCommerce.Module.Catalog.Services {
    public interface ICategoryService {
        Task<IList<CategoryListItem>> GetAll();

        Task Create(Category category);

        Task Update(Category category);

        Task Delete(Category category);

        Task<IList<CategoryMobile>> GetMainCategories(string lang);

        Task<IList<CategoryMobile>> GetAllCategories(string lang);

        Task<IList<CategoryMobile>> GetSubCategories(long id, string lang);
    }
}
