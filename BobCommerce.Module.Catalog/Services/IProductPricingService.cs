﻿using System;
using BobCommerce.Module.Catalog.Areas.Catalog.ViewModels;
using BobCommerce.Module.Catalog.Models;

namespace BobCommerce.Module.Catalog.Services {
    public interface IProductPricingService {
        CalculatedProductPrice CalculateProductPrice(ProductThumbnail productThumbnail);

        CalculatedProductPrice CalculateProductPrice(Product product);

        CalculatedProductPrice CalculateProductPrice(decimal price, decimal? oldPrice, decimal? specialPrice, DateTimeOffset? specialPriceStart, DateTimeOffset? specialPriceEnd);
    }
}
