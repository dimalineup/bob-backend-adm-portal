﻿using Microsoft.EntityFrameworkCore;
using BobCommerce.Module.Catalog.Models;
using BobCommerce.Module.Core.Data;

namespace BobCommerce.Module.Catalog.Data {
    public class ProductTemplateProductAttributeRepository : IProductTemplateProductAttributeRepository {
        private readonly DbContext dbContext;

        public ProductTemplateProductAttributeRepository(SimplDbContext dbContext) {
            this.dbContext = dbContext;
        }

        public void Remove(ProductTemplateProductAttribute item) {
            dbContext.Set<ProductTemplateProductAttribute>().Remove(item);
        }
    }
}
