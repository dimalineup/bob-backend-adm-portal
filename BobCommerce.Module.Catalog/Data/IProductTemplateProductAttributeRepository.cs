﻿using BobCommerce.Module.Catalog.Models;

namespace BobCommerce.Module.Catalog.Data {
    public interface IProductTemplateProductAttributeRepository {
        void Remove(ProductTemplateProductAttribute item);
    }
}
