﻿using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using BobCommerce.Infrastructure.Modules;
using BobCommerce.Module.Catalog.Data;
using BobCommerce.Module.Catalog.Events;
using BobCommerce.Module.Catalog.Services;
using BobCommerce.Module.Core.Events;

namespace BobCommerce.Module.Catalog {
    public class ModuleInitializer : IModuleInitializer {
        public void ConfigureServices(IServiceCollection services) {
            services.AddTransient<IProductTemplateProductAttributeRepository, ProductTemplateProductAttributeRepository>();
            services.AddTransient<INotificationHandler<ReviewSummaryChanged>, ReviewSummaryChangedHandler>();
            services.AddTransient<IBrandService, BrandService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IProductPricingService, ProductPricingService>();
            services.AddTransient<IProductService, ProductService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {

        }
    }
}
