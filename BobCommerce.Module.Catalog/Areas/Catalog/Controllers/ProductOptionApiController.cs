﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Module.Catalog.Areas.Catalog.ViewModels;
using BobCommerce.Module.Catalog.Models;

namespace BobCommerce.Module.Catalog.Areas.Catalog.Controllers {
    [Area("Catalog")]
    [Authorize(Roles = "admin, vendor, kitchen")]
    [Route("api/product-options")]
    public class ProductOptionApiController : Controller {
        private readonly IRepository<ProductOption> _productOptionRepository;

        public ProductOptionApiController(IRepository<ProductOption> productOptionRepository) {
            _productOptionRepository = productOptionRepository;
        }

        [HttpGet]
        public IActionResult Get() {
            var options = _productOptionRepository.Query();
            return Json(options);
        }

        [HttpGet("{id}")]
        public IActionResult Get(long id) {
            var productOption = _productOptionRepository.Query().FirstOrDefault(x => x.Id == id);
            var model = new ProductOptionFormVm {
                Id = productOption.Id,
                Name = productOption.Name
            };

            return Json(model);
        }

        [HttpPost]
        [Authorize(Roles = "admin, kitchen")]
        public IActionResult Post([FromBody] ProductOptionFormVm model) {
            if (ModelState.IsValid) {
                var productOption = new ProductOption {
                    Name = model.Name
                };

                _productOptionRepository.Add(productOption);
                _productOptionRepository.SaveChanges();

                return Ok();
            }
            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin, kitchen")]
        public IActionResult Put(long id, [FromBody] ProductOptionFormVm model) {
            if (ModelState.IsValid) {
                var productOption = _productOptionRepository.Query().FirstOrDefault(x => x.Id == id);
                productOption.Name = model.Name;

                _productOptionRepository.SaveChanges();

                return Ok();
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin, kitchen")]
        public IActionResult Delete(long id) {
            var productOption = _productOptionRepository.Query().FirstOrDefault(x => x.Id == id);
            if (productOption == null) {
                return NotFound();
            }

            _productOptionRepository.Remove(productOption);
            return Json(true);
        }
    }
}
