﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Infrastructure.Web.SmartTable;
using BobCommerce.Module.Catalog.Areas.Catalog.ViewModels;
using BobCommerce.Module.Catalog.Models;
using BobCommerce.Module.Catalog.Services;
using BobCommerce.Module.Core.Models;
using BobCommerce.Module.Core.Services;
using System.Collections.Generic;

namespace BobCommerce.Module.Catalog.Areas.Catalog.Controllers {
    [Area("Catalog")]
    //[Authorize(Roles = "admin, vendor")]
    [Route("categories")]
    public class CategoryMobileController : Controller {
        private readonly IRepository<Category> _categoryRepository;
        private readonly IRepository<ProductCategory> _productCategoryRepository;
        private readonly ICategoryService _categoryService;
        private readonly IMediaService _mediaService;
        private readonly IRepository<User> _userRepository;

        public CategoryMobileController(IRepository<Category> categoryRepository, IRepository<ProductCategory> productCategoryRepository, ICategoryService categoryService, IMediaService mediaService,
            IRepository<User> userRepository) {
            _categoryRepository = categoryRepository;
            _productCategoryRepository = productCategoryRepository;
            _categoryService = categoryService;
            _mediaService = mediaService;
            _userRepository = userRepository;
        }

        [HttpGet("all/{lang}")]
        public async Task<IActionResult> GetAllCategories(string lang) {
            var gridData = await _categoryService.GetAllCategories(lang);
            return Json(gridData);
        }

        [HttpGet("main/{lang}")]
        public async Task<IActionResult> GetMainCategories(string lang) {
            var gridData = await _categoryService.GetMainCategories(lang);
            return Json(gridData);
        }

        [HttpGet("subcateg/{id}/{lang}")]
        public async Task<IActionResult> GetSubCategories(long id, string lang) {
            var gridData = await _categoryService.GetSubCategories(id, lang);
            return Json(gridData);
        }

        [HttpGet("{id}/{lang}")]
        public async Task<IActionResult> Get(long id, string lang) {
            //var user = _userRepository.Query().Where(x => x.Id == userId).FirstOrDefault();
            var isHeb = lang.ToLower() == "he-il" ? true : false;

            var category = await _categoryRepository.Query().Include(x => x.ThumbnailImage).FirstOrDefaultAsync(x => x.Id == id);
            var model = new CategoryMobile {
                i = category.Id,
                n = (isHeb ? category.NameHE : category.Name),
                t = _mediaService.GetThumbnailUrl(category.ThumbnailImage),
                co = category.Color
            };

            return Json(model);
        }

        [HttpGet("{id}/{vendorId}/products/{lang}")]
        public async Task<IActionResult> GetProducts(long id, long vendorId, string lang) {
            //var user = _userRepository.Query().Where(x => x.Id == userId).FirstOrDefault();
            var isHeb = lang.ToLower() == "he-il" ? true : false;

            var query = await _productCategoryRepository.Query().Include(x => x.Product).Include(x => x.Product.ThumbnailImage)
                .Where(x => x.CategoryId == id && x.Product.VendorId == vendorId  && !x.Product.IsDeleted && x.Product.IsVisibleIndividually).ToListAsync();

            List<ProductMobile> retVal = new List<ProductMobile>();

            foreach (ProductCategory item in query) {
                var product = new ProductMobile {
                    i = item.Product.Id,
                    n = (isHeb ? item.Product.NameHE : item.Product.Name),
                    t = _mediaService.GetThumbnailUrl(item.Product.ThumbnailImage),
                    p = item.Product.Price
                };
                retVal.Add(product);
            }

            return Ok(retVal);
        }

        [HttpGet("{vendorId}/products/{lang}")]
        public async Task<IActionResult> GetVendorProducts(long vendorId, string lang) {
            try {
                //var user = _userRepository.Query().Where(x => x.Id == userId).FirstOrDefault();
                var isHeb = lang.ToLower() == "he-il" ? true : false;

                var query = await _productCategoryRepository.Query()
                    .Include(x => x.Product)
                    .Include(x => x.Product.ThumbnailImage)
                    .Include(x => x.Product.ProductLinks).ThenInclude(x => x.LinkedProduct)
                    .Include(x => x.Category)
                    .OrderBy(x => x.CategoryId)
                    .Where(x => x.Product.VendorId == vendorId && !x.Product.IsDeleted && x.Product.IsVisibleIndividually).ToListAsync();

                List<ProductMobile> retVal = new List<ProductMobile>();
                foreach (ProductCategory item in query) {
                    List<RelatedProductMobile> extras = new List<RelatedProductMobile>();
                    if(item.Product.ProductLinks != null && item.Product.ProductLinks.Count > 0) {
                        extras = item.Product.ProductLinks.Where(x => x.LinkType == ProductLinkType.Related).Select(x => new RelatedProductMobile { 
                            i = x.LinkedProductId,
                            sku = x.LinkedProduct.Sku,
                            d = x.LinkedProduct.Description,
                            n = (isHeb ? x.LinkedProduct.NameHE : x.LinkedProduct.Name),
                            p = x.LinkedProduct.Price,
                            t = _mediaService.GetThumbnailUrl(x.LinkedProduct.ThumbnailImage)
                        }).ToList();
                    }
                    var categ = new CategoryMobile {
                        i = item.Category.Id,
                        n = (isHeb ? item.Category.NameHE : item.Category.Name)
                    };
                    var product = new ProductMobile {
                        i = item.Product.Id,
                        sku = item.Product.Sku,
                        d = item.Product.Description,
                        n = (isHeb ? item.Product.NameHE : item.Product.Name),
                        t = _mediaService.GetThumbnailUrl(item.Product.ThumbnailImage),
                        p = item.Product.Price,
                        e = extras
                    };
                    if (retVal.Any(x => x.i == item.Product.Id)) {
                        var updated = retVal.Where(x => x.i == item.Product.Id).FirstOrDefault();
                        if (updated.c == null) {
                            List<CategoryMobile> categs = new List<CategoryMobile>();
                            categs.Add(categ);
                            updated.c = categs;
                        }
                        else {
                            updated.c.Add(categ);
                        }
                    }
                    else {
                        List<CategoryMobile> categs = new List<CategoryMobile>();
                        categs.Add(categ);
                        product.c = categs;
                        retVal.Add(product);
                    }
                }

                return Ok(retVal);
            }
            catch(Exception ex) {
                throw ex;
            }
        }
    }
}
