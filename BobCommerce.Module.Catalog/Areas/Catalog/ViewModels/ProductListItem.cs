﻿using BobCommerce.Module.Catalog.Models;
using System;
using System.Collections.Generic;

namespace BobCommerce.Module.Catalog.Areas.Catalog.ViewModels {
    public class ProductListItem {
        public long Id { get; set; }

        public string Name { get; set; }

        public string SKU { get; set;}

        public bool HasOptions { get; set; }

        public bool IsVisibleIndividually { get; set; }

        public DateTimeOffset CreatedOn { get; set; }

        public bool IsPublished { get; set; }

        public bool IsFeatured { get; set; }

        public bool IsCallForPricing { get; set; }

        public bool IsAllowToOrder { get; set; }

        public int? StockQuantity { get; set; }

        public List<string> Categories { get; set; } = new List<string>();

        public string ThumbnailImageUrl { get; set; }

        public decimal Price { get; set; }

    }
}
