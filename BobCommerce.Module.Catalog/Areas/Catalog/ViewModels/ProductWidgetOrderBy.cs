﻿namespace BobCommerce.Module.Catalog.Areas.Catalog.ViewModels {
    public enum ProductWidgetOrderBy {
        Newest,

        BestSelling,

        Discount
    }
}
