﻿using BobCommerce.Module.Core.Areas.Core.ViewModels;

namespace BobCommerce.Module.Catalog.Areas.Catalog.ViewModels {
    public class CategoryWidgetForm : WidgetFormBase {
        public CategoryWidgetSettings Settings { get; set; }
    }
}
