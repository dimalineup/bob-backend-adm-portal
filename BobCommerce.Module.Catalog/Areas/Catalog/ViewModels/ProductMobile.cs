﻿using BobCommerce.Module.Catalog.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BobCommerce.Module.Catalog.Areas.Catalog.ViewModels {
    public class ProductMobile {
        public long i { get; set; }

        public decimal p { get; set; }

        public string n { get; set; }

        public string d { get; set; }

        public string t { get; set; }
        
        public string sku { get; set; }

        public List<CategoryMobile> c { get; set; }

        public List<RelatedProductMobile> e { get; set; }
    }
}
