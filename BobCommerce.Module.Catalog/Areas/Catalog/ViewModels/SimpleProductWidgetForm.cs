﻿using BobCommerce.Module.Core.Areas.Core.ViewModels;

namespace BobCommerce.Module.Catalog.Areas.Catalog.ViewModels {
    public class SimpleProductWidgetForm : WidgetFormBase {
        public SimpleProductWidgetSetting Setting { get; set; }
    }
}
