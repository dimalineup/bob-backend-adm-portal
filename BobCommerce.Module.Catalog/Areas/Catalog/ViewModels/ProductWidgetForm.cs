﻿using BobCommerce.Module.Core.Areas.Core.ViewModels;

namespace BobCommerce.Module.Catalog.Areas.Catalog.ViewModels {
    public class ProductWidgetForm : WidgetFormBase {
        public ProductWidgetSetting Setting { get; set; }
    }
}
