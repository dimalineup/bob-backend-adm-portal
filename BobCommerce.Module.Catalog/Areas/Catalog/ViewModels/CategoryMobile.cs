﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BobCommerce.Module.Catalog.Areas.Catalog.ViewModels {
    public class CategoryMobile {
        public long i { get; set; }

        public string n { get; set; }

        public string t { get; set; }

        public string co { get; set; }

        public List<CategoryMobile> ca { get; set; } = new List<CategoryMobile>();
    }
}
