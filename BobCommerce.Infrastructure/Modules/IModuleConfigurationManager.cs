﻿using System.Collections.Generic;

namespace BobCommerce.Infrastructure.Modules {
    public interface IModuleConfigurationManager {
        IEnumerable<ModuleInfo> GetModules();
    }
}
