﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace BobCommerce.Infrastructure.Helpers {
    public static class EnumHelper {
        public static IDictionary<Enum, string> ToDictionary(Type type) {
            if (type == null) {
                throw new ArgumentNullException(nameof(type));
            }

            var dics = new Dictionary<Enum, string>();
            var enumValues = Enum.GetValues(type);

            foreach (Enum value in enumValues) {
                dics.Add(value, GetDisplayName(value));
            }

            return dics;
        }

        public static IDictionary<Enum, string> ToDictionaryByCulture(Type type, string culture) {
            if (type == null) {
                throw new ArgumentNullException(nameof(type));
            }

            var dics = new Dictionary<Enum, string>();
            var enumValues = Enum.GetValues(type);

            foreach (Enum value in enumValues) {
                switch (culture) {
                    case "en-US":
                        dics.Add(value, GetDisplayName(value));
                        break;
                    case "he-IL":
                        dics.Add(value, GetDescription(value));
                        break;
                    default:
                        dics.Add(value, GetDisplayName(value));
                        break;
                }
            }

            return dics;
        }

        public static string GetDisplayName(this Enum value) {
            //if (value == null) {
            //    throw new ArgumentNullException(nameof(value));
            //}

            //var displayName = value.ToString();
            //var fieldInfo = value.GetType().GetField(displayName);
            //var attributes = (DisplayAttribute[])fieldInfo.GetCustomAttributes(typeof(DisplayNameAttribute), false);

            //if (attributes.Length > 0) {
            //    displayName = attributes[0].Description;
            //}

            //return displayName;
            Type type = value.GetType();
            MemberInfo[] memInfo = type.GetMember(value.ToString());
            if (memInfo != null && memInfo.Length > 0) {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DisplayNameAttribute), false);
                if (attrs != null && attrs.Length > 0)
                    return ((DisplayNameAttribute)attrs[0]).DisplayName;
            }
            return value.ToString();
        }

        public static string GetDescription(this Enum en) {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());
            if (memInfo != null && memInfo.Length > 0) {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }
            return en.ToString();
        }
    }
}
