﻿using Newtonsoft.Json.Linq;

namespace BobCommerce.Infrastructure.Web.SmartTable {
    public class Search {
        public JObject PredicateObject { get; set; }
    }
}
