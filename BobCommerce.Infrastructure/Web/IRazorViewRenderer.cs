﻿using System.Threading.Tasks;

namespace BobCommerce.Infrastructure.Web {
    public interface IRazorViewRenderer {
        Task<string> RenderViewToStringAsync<TModel>(string viewName, TModel model);
    }
}
