﻿using BobCommerce.Infrastructure.Models;

namespace BobCommerce.Infrastructure.Data {
    public interface IRepository<T> : IRepositoryWithTypedId<T, long> where T : IEntityWithTypedId<long> {
    }
}
