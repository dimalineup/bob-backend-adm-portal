﻿using Microsoft.EntityFrameworkCore;

namespace BobCommerce.Infrastructure.Data {
    public interface ICustomModelBuilder {
        void Build(ModelBuilder modelBuilder);
    }
}
