﻿namespace BobCommerce.Infrastructure.Localization {
    public enum LanguageDirection {
        LTR,
        RTL
    }
}
