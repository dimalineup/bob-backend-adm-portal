﻿using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
//using BobCommerce.Module.Orders.Events;
using BobCommerce.Infrastructure.Modules;
using BobCommerce.Module.Orders.Services;

namespace BobCommerce.Module.Orders {
    public class ModuleInitializer : IModuleInitializer {
        public void ConfigureServices(IServiceCollection services) {
            services.AddTransient<IOrderService, OrderService>();
            //services.AddTransient<IOrderEmailService, OrderEmailService>();
            //services.AddHostedService<OrderCancellationBackgroundService>();
            //services.AddTransient<INotificationHandler<OrderChanged>, OrderChangedCreateOrderHistoryHandler>();
            //services.AddTransient<INotificationHandler<OrderCreated>, OrderCreatedCreateOrderHistoryHandler>();
        }

        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env) {
        }
    }
}
