﻿using System.Collections.Generic;

namespace BobCommerce.Module.Orders.Areas.Orders.ViewModels
{
    public class MobileOrderHistoryProductVm {
        public string ProductName { get; set; }

        public int Quantity { get; set; }

        public string ThumbnailImage { get; set; }

        public decimal ProductPrice { get; set; }

        public decimal Total => Quantity * ProductPrice;
    }
}
