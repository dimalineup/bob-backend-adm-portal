﻿using System;
using System.Collections.Generic;

namespace BobCommerce.Module.Orders.Areas.Orders.ViewModels
{
    public class MobileOrderItemVm {
        public long Id { get; set; }

        public DateTimeOffset CreatedOn { get; set; }

        public string OrderContent { get; set; }

        public IList<MobileOrderHistoryProductVm> OrderItems { get; set; } = new List<MobileOrderHistoryProductVm>();

        public decimal SubTotal { get; set; }

        public decimal Total { get; set; }

        public decimal Tax { get; set; }

        public decimal LocationDiscount { get; set; }
    }
}
