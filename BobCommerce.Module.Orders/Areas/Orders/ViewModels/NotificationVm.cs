﻿namespace BobCommerce.Module.Orders.Areas.Orders.ViewModels {
    public class NotificationVm {

        public int MessageType { get; set; }

        public long OrderID { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

    }
}
