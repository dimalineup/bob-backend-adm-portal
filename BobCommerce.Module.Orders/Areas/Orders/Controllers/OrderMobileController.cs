﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Module.Core.Extensions;
using BobCommerce.Module.Core.Services;
using BobCommerce.Module.Orders.Areas.Orders.ViewModels;
using BobCommerce.Module.Orders.Models;
using BobCommerce.Module.Core.Models;

namespace BobCommerce.Module.Orders.Areas.Orders.Controllers
{
    [Area("Orders")]
    //[Authorize]
    [Route("history")]
    public class OrderMobileController : Controller {
        private readonly IMediaService _mediaService;
        private readonly IRepository<Order> _orderRepository;
        private readonly IWorkContext _workContext;
        private readonly IRepository<User> _userRepository;

        public OrderMobileController(IRepository<Order> orderRepository, IWorkContext workContext, IMediaService mediaService, IRepository<User> userRepository) {
            _orderRepository = orderRepository;
            _workContext = workContext;
            _mediaService = mediaService;
            _userRepository = userRepository;
        }

        [HttpGet("{userId}/orders/{lang}")]
        public async Task<IActionResult> OrderHistoryList(long userId, string lang) {
            //var user = await _workContext.GetCurrentUser();
            //var user = _userRepository.Query().Where(x => x.Id == userId).FirstOrDefault();
            var isHeb = lang.ToLower() == "he-il" ? true : false;

            var model = await _orderRepository
                .Query()
                //.Where(x => x.CustomerId == user.Id && x.ParentId == null)
                .Where(x => x.CustomerId == userId && x.ParentId == null)
                .Select(x => new MobileOrderItemVm {
                    Id = x.Id,
                    CreatedOn = x.CreatedOn,
                    OrderItems = x.OrderItems.Select(i => new MobileOrderHistoryProductVm {
                        ProductName = (isHeb ? i.Product.NameHE : i.Product.Name),
                        Quantity = i.Quantity,
                        ProductPrice = i.ProductPrice,
                        ThumbnailImage = _mediaService.GetThumbnailUrl(i.Product.ThumbnailImage)
                    }).ToList(),
                    SubTotal = x.SubTotal,
                    Tax = x.TaxAmount,
                    LocationDiscount = x.SubTotal - x.OrderTotal,
                    Total = x.OrderTotal
                })
                .OrderByDescending(x => x.CreatedOn).ToListAsync();

            foreach(var item in model) {
                foreach(var product in item.OrderItems) {
                    item.OrderContent += product.Quantity + " " + product.ProductName + ",";
                }
                if (!string.IsNullOrEmpty(item.OrderContent)) {
                    item.OrderContent = item.OrderContent.TrimEnd(',');
                }
            }
            return Json(model);
        }

        [HttpGet("{userId}/orders/{orderId}/{lang}")]
        public async Task<IActionResult> OrderDetails(long userId, long orderId, string lang) {
            //var user = await _workContext.GetCurrentUser();
            //var user = _userRepository.Query().Where(x => x.Id == userId).FirstOrDefault();
            var isHeb = lang.ToLower() == "he-il" ? true : false;

            var order = _orderRepository
                .Query()
                //.Include(x => x.ShippingAddress).ThenInclude(x => x.District)
                //.Include(x => x.ShippingAddress).ThenInclude(x => x.StateOrProvince)
                //.Include(x => x.ShippingAddress).ThenInclude(x => x.Country)
                .Include(x => x.OrderItems).ThenInclude(x => x.Product).ThenInclude(x => x.ThumbnailImage)
                .Include(x => x.OrderItems).ThenInclude(x => x.Product).ThenInclude(x => x.OptionCombinations).ThenInclude(x => x.Option)
                .Include(x => x.Customer)
                .FirstOrDefault(x => x.Id == orderId);

            if (order == null) {
                return NotFound();
            }

            if (order.CustomerId != userId) {
                return BadRequest(new { error = "You don't have permission to view this order" });
            }

            var model = new OrderDetailVm {
                Id = order.Id,
                IsMasterOrder = order.IsMasterOrder,
                CreatedOn = order.CreatedOn,
                OrderStatus = (int)order.OrderStatus,
                OrderStatusString = order.OrderStatus.ToString(),
                CustomerId = order.CustomerId,
                CustomerName = order.Customer.FullName,
                CustomerEmail = order.Customer.Email,
                ShippingMethod = order.ShippingMethod,
                PaymentMethod = order.PaymentMethod,
                PaymentFeeAmount = order.PaymentFeeAmount,
                Subtotal = order.SubTotal,
                DiscountAmount = order.DiscountAmount,
                SubTotalWithDiscount = order.SubTotalWithDiscount,
                TaxAmount = order.TaxAmount,
                ShippingAmount = order.ShippingFeeAmount,
                OrderTotal = order.OrderTotal,
                LocationDiscount = order.SubTotal - order.OrderTotal,
                OrderNote = order.OrderNote,
                //ShippingAddress = new ShippingAddressVm {
                //    AddressLine1 = order.ShippingAddress.AddressLine1,
                //    CityName = order.ShippingAddress.City,
                //    ZipCode = order.ShippingAddress.ZipCode,
                //    ContactName = order.ShippingAddress.ContactName,
                //    DistrictName = order.ShippingAddress.District?.Name,
                //    StateOrProvinceName = order.ShippingAddress.StateOrProvince.Name,
                //    Phone = order.ShippingAddress.Phone
                //},
                OrderItems = order.OrderItems.Select(x => new OrderItemVm {
                    Id = x.Id,
                    ProductId = x.Product.Id,
                    ProductName = (isHeb ? x.Product.NameHE : x.Product.Name),
                    ProductPrice = x.ProductPrice,
                    Quantity = x.Quantity,
                    DiscountAmount = x.DiscountAmount,
                    ProductImage = x.Product.ThumbnailImage == null ? "no-image.png" : x.Product.ThumbnailImage.FileName,
                    TaxAmount = x.TaxAmount,
                    TaxPercent = x.TaxPercent,
                    VariationOptions = OrderItemVm.GetVariationOption(x.Product)
                }).ToList()
            };

            foreach (var item in model.OrderItems) {
                item.ProductImage = _mediaService.GetMediaUrl(item.ProductImage);
            }

            return Json(model);
        }
    }
}
