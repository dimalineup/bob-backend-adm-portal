﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Module.Core.Extensions;
using Microsoft.Extensions.Configuration;
using BobCommerce.Module.Orders.Models;
using BobCommerce.Module.Orders.Areas.Orders.ViewModels;
using System.Linq;
using BobCommerce.Module.Vendors.Models;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Helpers;
using BobCommerce.Module.Core.Models;
using BobCommerce.Module.Core.Services;

namespace BobCommerce.Module.Orders.Areas.Orders.Controllers
{
    [Area("notification")]
    [Route("notification")]
    public class NotificationMobileController : Controller {
        private readonly IWorkContext _workContext;
        private readonly IConfiguration _configuration;
        private readonly IRepository<Notification> _notifRepository;
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<TableReservation> _tableReservationRepository;
        private readonly IRepository<TableMgmt> _tableMgmtRepository;
        private readonly IMediaService _mediaService;

        public NotificationMobileController(IWorkContext workContext, IConfiguration config, IRepository<Notification> notifRepository, IRepository<Order> orderRepository,
            IRepository<TableReservation> tableReservationRepository, IRepository<TableMgmt> tableMgmtRepository, IMediaService mediaService) {
            _workContext = workContext;
            _configuration = config;
            _notifRepository = notifRepository;
            _orderRepository = orderRepository;
            _tableReservationRepository = tableReservationRepository;
            _tableMgmtRepository = tableMgmtRepository;
            _mediaService = mediaService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] NotificationVm model) {
            if (ModelState.IsValid) {

                try {

                    //get the user that made the order
                    var order = _orderRepository.Query().Where(x => x.Id == model.OrderID).FirstOrDefault();
                    var customerId = order.CustomerId;

                    //get the table of the user that made the order
                    var res = _tableReservationRepository.Query().Where(x => x.UserId == customerId).FirstOrDefault();
                    var tableNo = res.TableNumber;
                    var vendorId = res.VendorId;

                    //get the waiter of that table
                    var tableMgmt = _tableMgmtRepository.Query().Where(x => x.VendorId == vendorId).ToList();
                    long waiterID = -1;
                    foreach (var item in tableMgmt) {
                        var assignedTables = item.AssignedTables;
                        var tables = assignedTables.Split(',');
                        if (tables != null && tables.Length > 0) {
                            for (var idx = 0; idx < tables.Length; idx++) {
                                if (tables[idx] == tableNo) {
                                    waiterID = item.UserId;

                                    var notif = new Notification {
                                        DateTime = DateTime.Now,
                                        Message = model.Message,
                                        MessageType = (NotificationType)model.MessageType,
                                        Title = model.Title,
                                        WaiterID = waiterID,
                                        UserID = customerId,
                                        TableNo = tableNo,
                                        OrderID = model.OrderID
                                    };

                                    _notifRepository.Add(notif);
                                }
                            }
                        }
                    }


                    await _notifRepository.SaveChangesAsync();
                    return Ok(true);
                }
                catch(Exception ex) {
                    throw ex;
                }
            }
            return Ok(false);
        }

        [HttpGet("{userId}")]
        public async Task<IActionResult> Get(long userId) {
            try {

                var currentUser = await _workContext.GetCurrentUser();
                var culture = currentUser.Culture;
                if (culture == null) {
                    culture = "en-US";
                }

                var notifications = await _notifRepository.Query().Include(x => x.Client).Where(x => x.WaiterID == userId).OrderByDescending(x => x.DateTime)
                    .Select(x => new {
                        ID = x.Id,
                        MessageType = culture == "he-IL" ? x.MessageType.GetDescription() : x.MessageType.GetDisplayName(),
                        UserName = x.Client.FullName,
                        TableNo = x.TableNo,
                        Message = x.Message,
                        Read = x.Read,
                        Date = x.DateTime,
                        ImageUrl = x.Client.AccountType == (int)UserAccountType.Facebook ? x.Client.FacebookImageUrl : _configuration["MySettings:AbsoluteUrl"] + _mediaService.GetThumbnailUrl(currentUser.UserImage),
                        OrderID = x.OrderID
                        
                    }).ToListAsync();
                return Json(notifications);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id) {
            var notif = await _notifRepository.Query().FirstOrDefaultAsync(x => x.Id == id);
            if (notif == null) {
                return NotFound();
            }

            try {
                _notifRepository.Remove(notif);
                await _notifRepository.SaveChangesAsync();
            }
            catch (DbUpdateException) {
                return BadRequest(new { Error = $"The table {notif.Id} can't not be deleted because it is referenced by other tables" });
            }

            return Ok(true);
        }

        [HttpPut("read/{id}")]
        public async Task<IActionResult> Put(long id) {
            try {
                if (ModelState.IsValid) {
                    var notif = await _notifRepository.Query().FirstOrDefaultAsync(x => x.Id == id);
                    if (notif == null) {
                        return NotFound();
                    }

                    notif.Read = true;
                    
                    await _notifRepository.SaveChangesAsync();
                    return Ok(true);
                }

                return Ok(false);
            }
            catch (Exception ex) {
                throw ex;
            }
        }
    }
}
