﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Module.ShoppingCart.Models;
using BobCommerce.Module.Orders.Areas.Orders.ViewModels;
using BobCommerce.Module.Core.Extensions;
using BobCommerce.Module.Orders.Services;
using Microsoft.EntityFrameworkCore;
/*using BobCommerce.Module.Core.Models;



using BobCommerce.Module.ShoppingCart.Services;*/

namespace BobCommerce.Module.Orders.Areas.Orders.Controllers {
    [Area("ShoppingCart")]
    public class CheckoutApiController : Controller {

        private readonly IRepository<Cart> _cartRepository;
        private readonly IWorkContext _workContext;
        private readonly IOrderService _orderService;

        public CheckoutApiController(
            IOrderService orderService,
            IWorkContext workContext,
            IRepository<Cart> cartRepository) {
            _orderService = orderService;
            _workContext = workContext;
            _cartRepository = cartRepository;
        }

        [HttpPost("api/cart/{cartId}/order")]
        public async Task<IActionResult> CreateOrder(long cartId, [FromBody] DeliveryInformationVm deliveryInformationVm) {
            var currentUser = await _workContext.GetCurrentUser();
            var cart = await _cartRepository.Query().FirstOrDefaultAsync(x => x.Id == cartId);
            if (cart == null) {
                return NotFound();
            }

            if (cart.CreatedById != currentUser.Id) {
                return Forbid();
            }

            cart.ShippingData = JsonConvert.SerializeObject(deliveryInformationVm);
            cart.OrderNote = deliveryInformationVm.OrderNote;
            _cartRepository.SaveChanges();
            var orderCreateResult = await _orderService.CreateOrder(cart.Id, "CashOnDelivery", 0, null, null);

            if (!orderCreateResult.Success) {
                return BadRequest(orderCreateResult.Error);
            }

            return Created($"/api/orders/{orderCreateResult.Value.Id}", new { id = orderCreateResult.Value.Id });
        }
    }
}
