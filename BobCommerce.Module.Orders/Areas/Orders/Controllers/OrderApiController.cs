﻿using BobCommerce.Infrastructure.Data;
using BobCommerce.Infrastructure.Helpers;
using BobCommerce.Infrastructure.Web.SmartTable;
using BobCommerce.Module.Catalog.Models;
using BobCommerce.Module.Core.Extensions;
using BobCommerce.Module.Core.Models;
using BobCommerce.Module.Orders.Areas.Orders.ViewModels;
using BobCommerce.Module.Orders.Events;
using BobCommerce.Module.Orders.Models;
using BobCommerce.Module.Tax.Services;
//using BobCommerce.Module.Orders.ViewModels;
using BobCommerce.Module.Vendors.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BobCommerce.Module.Orders.Areas.Orders.Controllers
{
    [Area("Orders")]
    //[Authorize(Roles = "admin, vendor, waiter, barman, kitchen, manager")]
    [Authorize(Roles = "admin, vendor, waiter, barman, kitchen")]
    [Route("api/orders")]
    public class OrderApiController : Controller {
        private const string ImpersonationCookieName = "ImpersonateVendor";
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<OrderItem> _orderItemRepository;
        private readonly IRepository<TableReservation> _tableReservationRepository;
        private readonly IWorkContext _workContext;
        private readonly IMediator _mediator;
        private HttpContext _httpContext;
        private readonly IRepository<Vendor> _vendorRepository;
        private IRepository<User> _userRepository;
        private IRepository<Role> _roleRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly ITaxService _taxService;
        private readonly bool _isProductPriceIncludeTax;

        public OrderApiController(IRepository<Order> orderRepository, IRepository<OrderItem> orderItemRepository, IRepository<TableReservation> tableReservationRepository, IWorkContext workContext, 
            IMediator mediator, IHttpContextAccessor contextAccessor, IRepository<Vendor> vendorRepository, IRepository<User> userRepository, IRepository<Role> roleRepository, 
            IRepository<Product> productRepository, ITaxService taxService, IConfiguration config) {
            _orderRepository = orderRepository;
            _orderItemRepository = orderItemRepository;
            _tableReservationRepository = tableReservationRepository;
            _workContext = workContext;
            _mediator = mediator;
            _httpContext = contextAccessor.HttpContext;
            _vendorRepository = vendorRepository;
            _userRepository = userRepository;
            _roleRepository = roleRepository;
            _productRepository = productRepository;
            _taxService = taxService;
            _isProductPriceIncludeTax = config.GetValue<bool>("Catalog.IsProductPriceIncludeTax");
        }

        [HttpGet]
        public async Task<ActionResult> Get(int status, int numRecords) {
            var orderStatus = (OrderStatus)status;
            if ((numRecords <= 0) || (numRecords > 100)) {
                numRecords = 5;
            }
            var vendorID = GetImpersonationFromCookies();
            var query = _orderRepository.Query();
            if (!string.IsNullOrEmpty(vendorID)) {
                query = query.Where(x => x.VendorId == int.Parse(vendorID));
            }

            if (orderStatus != 0) {
                query = query.Where(x => x.OrderStatus == orderStatus);
            }

            var currentUser = await _workContext.GetCurrentUser();
            var culture = currentUser.Culture;
            if(culture == null) {
                culture = "en-US";
            }
            if (!User.IsInRole("admin")) {
                query = query.Where(x => x.VendorId == currentUser.VendorId);
            }

            var model = query.OrderByDescending(x => x.CreatedOn)
                .Take(numRecords)
                .Select(x => new {
                    x.Id,
                    CustomerName = x.Customer.FullName,
                    x.OrderTotal,
                    OrderTotalString = x.OrderTotal.ToString("C"),
                    orderStatus = culture == "he-IL" ? x.OrderStatus.GetDescription() : x.OrderStatus.ToString(),
                    CreatedOn = x.CreatedOn == null ? "" : x.CreatedOn.ToString("MMM dd, yyyy HH:mm:ss tt", new CultureInfo(culture))
                });

            return Json(model);

        }

        private string GetImpersonationFromCookies() {
            if (_httpContext.Request.Cookies.ContainsKey(ImpersonationCookieName)) {
                return _httpContext.Request.Cookies[ImpersonationCookieName];
            }
            return null;
        }

        [HttpPost("grid")]
        public async Task<ActionResult> List([FromBody] SmartTableParam param)
        {
            var vendorID = GetImpersonationFromCookies();
            var query = _orderRepository
                .Query();

            if (!string.IsNullOrEmpty(vendorID)) {
                query = query.Where(x => x.VendorId == int.Parse(vendorID));
            }

            var currentUser = await _workContext.GetCurrentUser();
            var culture = currentUser.Culture;
            if(culture == null) {
                culture = "en-US";
            }
            if (!User.IsInRole("admin"))
            {
                query = query.Where(x => x.VendorId == currentUser.VendorId);
            }

            if (param.Search.PredicateObject != null)
            {
                dynamic search = param.Search.PredicateObject;
                if (search.Id != null)
                {
                    long id = search.Id;
                    query = query.Where(x => x.Id == id);
                }

                if (search.Status != null)
                {
                    var status = (OrderStatus)search.Status;
                    query = query.Where(x => x.OrderStatus == status);
                }

                if (search.CustomerName != null)
                {
                    string customerName = search.CustomerName;
                    query = query.Where(x => x.Customer.FullName.Contains(customerName));
                }

                if (search.CreatedOn != null)
                {
                    if (search.CreatedOn.before != null)
                    {
                        DateTimeOffset before = search.CreatedOn.before;
                        query = query.Where(x => x.CreatedOn <= before);
                    }

                    if (search.CreatedOn.after != null)
                    {
                        DateTimeOffset after = search.CreatedOn.after;
                        query = query.Where(x => x.CreatedOn >= after);
                    }
                }
            }

            var orders = query.ToSmartTableResult(
                param,
                order => new {
                    order.Id,
                    CustomerName = order.Customer.FullName,
                    UserId = order.Customer.Id,
                    order.OrderTotal,
                    OrderStatus = culture == "he-IL" ? order.OrderStatus.GetDescription() : order.OrderStatus.ToString(),
                    CreatedOn = order.CreatedOn.ToString("MMM dd, yyyy HH:mm:ss tt", new CultureInfo(culture))
                });

            return Json(orders);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            decimal locationDiscount = 0;
            var order = _orderRepository
                .Query()
                //.Include(x => x.ShippingAddress).ThenInclude(x => x.District)
                //.Include(x => x.ShippingAddress).ThenInclude(x => x.StateOrProvince)
                //.Include(x => x.ShippingAddress).ThenInclude(x => x.Country)
                .Include(x => x.OrderItems).ThenInclude(x => x.Product).ThenInclude(x => x.ThumbnailImage)
                .Include(x => x.OrderItems).ThenInclude(x => x.Product).ThenInclude(x => x.OptionCombinations).ThenInclude(x => x.Option)
                .Include(x => x.Customer)
                .FirstOrDefault(x => x.Id == id);

            if (order == null)
            {
                return NotFound();
            }

            var currentUser = await _workContext.GetCurrentUser();
            var culture = currentUser.Culture;
            if (culture == null) {
                culture = "en-US";
            }
            if (!User.IsInRole("admin") && order.VendorId != currentUser.VendorId)
            {
                return BadRequest(new { error = "You don't have permission to manage this order" });
            }

            //get vendor discount
            if(order.VendorId > 0) {
                var vendor = _vendorRepository.Query().Where(x => x.Id == order.VendorId).FirstOrDefault();
                if(vendor != null) {
                    locationDiscount = vendor.Discount;
                }
            }

            var model = new OrderDetailVm
            {
                Id = order.Id,
                IsMasterOrder = order.IsMasterOrder,
                CreatedOn = order.CreatedOn,
                OrderDate = order.CreatedOn.ToString("MMM dd, yyyy HH:mm:ss tt", new CultureInfo(culture)),
                OrderStatus = (int)order.OrderStatus,
                OrderStatusString = culture == "he-IL" ? order.OrderStatus.GetDescription() : order.OrderStatus.ToString(),
                CustomerId = order.CustomerId,
                CustomerName = order.Customer.FullName,
                CustomerEmail = order.Customer.Email,
                ShippingMethod = order.ShippingMethod,
                PaymentMethod = order.PaymentMethod,
                PaymentFeeAmount = order.PaymentFeeAmount,
                Subtotal = order.SubTotal,
                DiscountAmount = order.DiscountAmount,
                SubTotalWithDiscount = order.SubTotalWithDiscount,
                TaxAmount = order.TaxAmount,
                ShippingAmount = order.ShippingFeeAmount,
                OrderTotal = order.OrderTotal,
                OrderNote = order.OrderNote,
                LocationDiscount = locationDiscount / 100,
                Tips = order.Tips,
                //ShippingAddress = new ShippingAddressVm
                //{
                //    AddressLine1 = order.ShippingAddress.AddressLine1,
                //    CityName = order.ShippingAddress.City,
                //    ZipCode = order.ShippingAddress.ZipCode,
                //    ContactName = order.ShippingAddress.ContactName,
                //    DistrictName = order.ShippingAddress.District?.Name,
                //    StateOrProvinceName = order.ShippingAddress.StateOrProvince.Name,
                //    Phone = order.ShippingAddress.Phone
                //},
                OrderItems = order.OrderItems.Select(x => new OrderItemVm
                {
                    Id = x.Id,
                    ProductId = x.Product.Id,
                    ProductName = x.Product.Name,
                    ProductPrice = x.ProductPrice,
                    Quantity = x.Quantity,
                    DiscountAmount = x.DiscountAmount,
                    TaxAmount = x.TaxAmount,
                    TaxPercent = x.TaxPercent,
                    VariationOptions = OrderItemVm.GetVariationOption(x.Product)
                }).ToList()
            };

            if (order.IsMasterOrder)
            {
                model.SubOrderIds = _orderRepository.Query().Where(x => x.ParentId == order.Id).Select(x => x.Id).ToList();
            }

            await _mediator.Publish(new OrderDetailGot { OrderDetailVm = model });

            return Json(model);
        }

        [HttpPost("change-order-status/{id}")]
        public async Task<IActionResult> ChangeStatus(long id, [FromBody] OrderStatusForm model)
        {
            var order = _orderRepository.Query().FirstOrDefault(x => x.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            var currentUser = await _workContext.GetCurrentUser();
            if (!User.IsInRole("admin") && order.VendorId != currentUser.VendorId)
            {
                return BadRequest(new { error = "You don't have permission to manage this order" });
            }

            if (Enum.IsDefined(typeof(OrderStatus), model.StatusId))
            {
                var oldStatus = order.OrderStatus;
                order.OrderStatus = (OrderStatus)model.StatusId;
                await _orderRepository.SaveChangesAsync();

                var orderStatusChanged = new OrderChanged
                {
                    OrderId = order.Id,
                    OldStatus = oldStatus,
                    NewStatus = order.OrderStatus,
                    Order = order,
                    UserId = currentUser.Id,
                    Note = model.Note
                };

                await _mediator.Publish(orderStatusChanged);
                return Accepted();
            }

            return BadRequest(new { Error = "unsupported order status" });
        }

        [HttpGet("order-status")]
        public async Task<IActionResult> GetOrderStatus()
        {

            var currentUser = await _workContext.GetCurrentUser();
            var culture = currentUser.Culture;
            if (culture == null) {
                culture = "en-US";
            }
            var model = EnumHelper.ToDictionaryByCulture(typeof(OrderStatus), culture).Select(x => new { Id = x.Key, Name = x.Value });
            return Json(model);
        }

        [HttpPost("export")]
        public async Task<IActionResult> Export([FromBody] SmartTableParam param)
        {
            var query = _orderRepository.Query();

            var currentUser = await _workContext.GetCurrentUser();
            if (!User.IsInRole("admin"))
            {
                query = query.Where(x => x.VendorId == currentUser.VendorId);
            }

            if (param.Search.PredicateObject != null)
            {
                dynamic search = param.Search.PredicateObject;
                if (search.Id != null)
                {
                    long id = search.Id;
                    query = query.Where(x => x.Id == id);
                }

                if (search.Status != null)
                {
                    var status = (OrderStatus)search.Status;
                    query = query.Where(x => x.OrderStatus == status);
                }

                if (search.CustomerName != null)
                {
                    string customerName = search.CustomerName;
                    query = query.Where(x => x.Customer.FullName.Contains(customerName));
                }

                if (search.CreatedOn != null)
                {
                    if (search.CreatedOn.before != null)
                    {
                        DateTimeOffset before = search.CreatedOn.before;
                        query = query.Where(x => x.CreatedOn <= before);
                    }

                    if (search.CreatedOn.after != null)
                    {
                        DateTimeOffset after = search.CreatedOn.after;
                        query = query.Where(x => x.CreatedOn >= after);
                    }
                }
            }

            var orders = await query
                .Select(x => new OrderExportVm
                {
                    Id = x.Id,
                    OrderStatus = (int)x.OrderStatus,
                    IsMasterOrder = x.IsMasterOrder,
                    DiscountAmount = x.DiscountAmount,
                    CreatedOn = x.CreatedOn,
                    OrderStatusString = x.OrderStatus.GetDisplayName(),
                    PaymentFeeAmount = x.PaymentFeeAmount,
                    OrderTotal = x.OrderTotal,
                    Subtotal = x.SubTotal,
                    SubTotalWithDiscount = x.SubTotalWithDiscount,
                    PaymentMethod = x.PaymentMethod,
                    ShippingAmount = x.ShippingFeeAmount,
                    ShippingMethod = x.ShippingMethod,
                    TaxAmount = x.TaxAmount,
                    CustomerId = x.CustomerId,
                    CustomerName = x.Customer.FullName,
                    CustomerEmail = x.Customer.Email,
                    LatestUpdatedOn = x.LatestUpdatedOn,
                    Coupon = x.CouponCode,
                    Items = x.OrderItems.Count(),
                    BillingAddressId = x.BillingAddressId,
                    //BillingAddressAddressLine1 = x.BillingAddress.AddressLine1,
                    //BillingAddressAddressLine2 = x.BillingAddress.AddressLine2,
                    //BillingAddressContactName = x.BillingAddress.ContactName,
                    //BillingAddressCountryName = x.BillingAddress.Country.Name,
                    //BillingAddressDistrictName = x.BillingAddress.District.Name,
                    //BillingAddressZipCode = x.BillingAddress.ZipCode,
                    //BillingAddressPhone = x.BillingAddress.Phone,
                    //BillingAddressStateOrProvinceName = x.BillingAddress.StateOrProvince.Name,
                    //ShippingAddressAddressLine1 = x.ShippingAddress.AddressLine1,
                    //ShippingAddressAddressLine2 = x.ShippingAddress.AddressLine2,
                    ShippingAddressId = x.ShippingAddressId,
                    //ShippingAddressContactName = x.ShippingAddress.ContactName,
                    //ShippingAddressCountryName = x.ShippingAddress.Country.Name,
                    //ShippingAddressDistrictName = x.ShippingAddress.District.Name,
                    //ShippingAddressPhone = x.ShippingAddress.Phone,
                    //ShippingAddressStateOrProvinceName = x.ShippingAddress.StateOrProvince.Name,
                    //ShippingAddressZipCode = x.ShippingAddress.ZipCode
                })
                .ToListAsync();

            var csvString = CsvConverter.ExportCsv(orders);
            var csvBytes = Encoding.UTF8.GetBytes(csvString);
            // MS Excel need the BOM to display UTF8 Correctly
            var csvBytesWithUTF8BOM = Encoding.UTF8.GetPreamble().Concat(csvBytes).ToArray();
            return File(csvBytesWithUTF8BOM, "text/csv", "orders-export.csv");
        }

        [HttpPost("lines-export")]
        public async Task<IActionResult> OrderLinesExport([FromBody] SmartTableParam param, [FromServices] IRepository<OrderItem> orderItemRepository)
        {
            var query = orderItemRepository.Query();

            var currentUser = await _workContext.GetCurrentUser();
            if (!User.IsInRole("admin"))
            {
                query = query.Where(x => x.Order.VendorId == currentUser.VendorId);
            }

            if (param.Search.PredicateObject != null)
            {
                dynamic search = param.Search.PredicateObject;
                if (search.Id != null)
                {
                    long id = search.Id;
                    query = query.Where(x => x.Id == id);
                }

                if (search.Status != null)
                {
                    var status = (OrderStatus)search.Status;
                    query = query.Where(x => x.Order.OrderStatus == status);
                }

                if (search.CustomerName != null)
                {
                    string customerName = search.CustomerName;
                    query = query.Where(x => x.Order.Customer.FullName.Contains(customerName));
                }

                if (search.CreatedOn != null)
                {
                    if (search.CreatedOn.before != null)
                    {
                        DateTimeOffset before = search.CreatedOn.before;
                        query = query.Where(x => x.Order.CreatedOn <= before);
                    }

                    if (search.CreatedOn.after != null)
                    {
                        DateTimeOffset after = search.CreatedOn.after;
                        query = query.Where(x => x.Order.CreatedOn >= after);
                    }
                }
            }

            var orderItems = await query
                            .Select(x => new OrderLineExportVm
                            {
                                Id = x.Id,
                                OrderStatus = (int)x.Order.OrderStatus,
                                IsMasterOrder = x.Order.IsMasterOrder,
                                DiscountAmount = x.Order.DiscountAmount,
                                CreatedOn = x.Order.CreatedOn,
                                OrderStatusString = x.Order.OrderStatus.ToString(),
                                PaymentFeeAmount = x.Order.PaymentFeeAmount,
                                OrderTotal = x.Order.OrderTotal,
                                Subtotal = x.Order.SubTotal,
                                SubTotalWithDiscount = x.Order.SubTotalWithDiscount,
                                PaymentMethod = x.Order.PaymentMethod,
                                ShippingAmount = x.Order.ShippingFeeAmount,
                                ShippingMethod = x.Order.ShippingMethod,
                                TaxAmount = x.Order.TaxAmount,
                                CustomerId = x.Order.CustomerId,
                                CustomerName = x.Order.Customer.FullName,
                                CustomerEmail = x.Order.Customer.Email,
                                LatestUpdatedOn = x.Order.LatestUpdatedOn,
                                Coupon = x.Order.CouponCode,
                                Items = x.Order.OrderItems.Count(),
                                BillingAddressId = x.Order.BillingAddressId,
                                //BillingAddressAddressLine1 = x.Order.BillingAddress.AddressLine1,
                                //BillingAddressAddressLine2 = x.Order.BillingAddress.AddressLine2,
                                //BillingAddressContactName = x.Order.BillingAddress.ContactName,
                                //BillingAddressCountryName = x.Order.BillingAddress.Country.Name,
                                //BillingAddressDistrictName = x.Order.BillingAddress.District.Name,
                                //BillingAddressZipCode = x.Order.BillingAddress.ZipCode,
                                //BillingAddressPhone = x.Order.BillingAddress.Phone,
                                //BillingAddressStateOrProvinceName = x.Order.BillingAddress.StateOrProvince.Name,
                                //ShippingAddressAddressLine1 = x.Order.ShippingAddress.AddressLine1,
                                //ShippingAddressAddressLine2 = x.Order.ShippingAddress.AddressLine2,
                                ShippingAddressId = x.Order.ShippingAddressId,
                                //ShippingAddressContactName = x.Order.ShippingAddress.ContactName,
                                //ShippingAddressCountryName = x.Order.ShippingAddress.Country.Name,
                                //ShippingAddressDistrictName = x.Order.ShippingAddress.District.Name,
                                //ShippingAddressPhone = x.Order.ShippingAddress.Phone,
                                //ShippingAddressStateOrProvinceName = x.Order.ShippingAddress.StateOrProvince.Name,
                                //ShippingAddressZipCode = x.Order.ShippingAddress.ZipCode,
                                OrderLineDiscountAmount = x.DiscountAmount,
                                OrderLineQuantity = x.Quantity,
                                OrderLineTaxAmount = x.TaxAmount,
                                OrderLineTaxPercent = x.TaxPercent,
                                OrderLineId = x.Id,
                                ProductId = x.ProductId,
                                ProductName = x.Product.Name,
                                ProductPrice = x.ProductPrice
                            })
                            .ToListAsync();

            var csvString = CsvConverter.ExportCsv(orderItems);
            var csvBytes = Encoding.UTF8.GetBytes(csvString);
            // MS Excel need the BOM to display UTF8 Correctly
            var csvBytesWithUTF8BOM = Encoding.UTF8.GetPreamble().Concat(csvBytes).ToArray();
            return File(csvBytesWithUTF8BOM, "text/csv", "order-lines-export.csv");
        }

        [HttpGet("myrole")]
        public async Task<IActionResult> GetMyRole() {
            try {
                var role = "";
                var currentUser = await _workContext.GetCurrentUser();
                var user = _userRepository.Query().Include(x => x.Roles).ThenInclude(xx => xx.Role).FirstOrDefault(x => x.UserName == currentUser.UserName);
                var roles = _roleRepository.Query().ToList();
                foreach(var globalRole in roles) {
                    if (user.Roles.Any(x => x.Role.Name.Contains(globalRole.Name))) {
                        role = globalRole.Name;
                        break;
                    }
                }
                return Ok(role);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [HttpPost("update/{id}")]
        public async Task<IActionResult> UpdateOrder(long id, [FromBody] OrderItemEdit model) {
            var order = await _orderRepository.Query().Include(x => x.OrderItems).ThenInclude(x => x.Product).Where(x => x.Id == id).FirstOrDefaultAsync();
            if (order == null) {
                return Ok(false);
            }

            if(model != null) {
                if(model.ProductIdRemoved != null) {
                    foreach(long removedItem in model.ProductIdRemoved) {
                        var orderItem = await _orderItemRepository.Query().Where(x => x.Order.Id == id && x.Id == removedItem).FirstOrDefaultAsync();
                        _orderItemRepository.Remove(orderItem);

                    }
                    _orderItemRepository.SaveChanges();
                }
                if(model.ProductIdAdded != null) {
                    
                    for(var idx = 0; idx < model.ProductIdAdded.Count; idx++) {
                        var addedItem = model.ProductIdAdded[idx];
                        var prod = await _productRepository.Query().Where(x => x.Id == addedItem).FirstOrDefaultAsync();
                        var taxPercent = await _taxService.GetTaxPercent(prod.TaxClassId, null, null, null);
                        var productPrice = prod.Price;
                        if (_isProductPriceIncludeTax) {
                            productPrice = productPrice / (1 + (taxPercent / 100));
                        }

                        var orderItem = new OrderItem {
                            Product = prod,
                            ProductPrice = productPrice,
                            Quantity = model.QuantityAdded[idx],
                            TaxPercent = taxPercent,
                            TaxAmount = model.QuantityAdded[idx] * (productPrice * taxPercent / 100)
                        };

                        if (_isProductPriceIncludeTax) {
                            orderItem.ProductPrice = orderItem.ProductPrice - orderItem.TaxAmount;
                        }

                        order.AddOrderItem(orderItem);
                    }
                }

                order.TaxAmount = order.OrderItems.Sum(x => x.TaxAmount);
                order.SubTotal = order.OrderItems.Sum(x => x.ProductPrice * x.Quantity);
                order.SubTotalWithDiscount = order.SubTotal + order.Tips - order.DiscountAmount;

                decimal temp = order.SubTotal + order.Tips + order.TaxAmount + order.ShippingFeeAmount - order.DiscountAmount;

                decimal minusLocationDiscount = 0;
                if (order.VendorId > 0) {
                    var vendor = _vendorRepository.Query().Where(x => x.Id == order.VendorId).FirstOrDefault();
                    if (vendor != null) {
                        decimal locationDiscount = vendor.Discount;
                        minusLocationDiscount = temp * locationDiscount / 100;
                    }
                }
                order.OrderTotal = temp - minusLocationDiscount;

                _orderRepository.SaveChanges();
            }

            return Ok(true);
        }
    }
}
