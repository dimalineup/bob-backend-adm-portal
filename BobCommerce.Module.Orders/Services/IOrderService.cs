﻿using System.Threading.Tasks;
using BobCommerce.Infrastructure;
using BobCommerce.Module.Core.Models;
using BobCommerce.Module.Orders.Areas.Orders.ViewModels;
using BobCommerce.Module.Orders.Models;

namespace BobCommerce.Module.Orders.Services {
    public interface IOrderService {
        Task<Result<Order>> CreateOrder(long cartId, string paymentMethod, decimal paymentFeeAmount, string payerID, string paymentID, OrderStatus orderStatus = OrderStatus.New);
        Task<Result<Order>> CreateOrderIsracard(long cartId, string paymentMethod, decimal paymentFeeAmount, decimal tips, OrderStatus orderStatus = OrderStatus.New);
        Task<Result<Order>> CreateOrder(long cartId, string paymentMethod, decimal paymentFeeAmount, string shippingMethod, Address billingAddress, Address shippingAddress, string payerID, string paymentID, OrderStatus orderStatus = OrderStatus.New);
        Task<Result<Order>> CreateOrderIsracard(long cartId, string paymentMethod, decimal paymentFeeAmount, string shippingMethod, Address billingAddress, Address shippingAddress, decimal tips, OrderStatus orderStatus = OrderStatus.New);
        void CancelOrder(Order order);

        Task<decimal> GetTax(long cartId, string countryId, long stateOrProvinceId, string zipCode);

        //Task<OrderTaxAndShippingPriceVm> UpdateTaxAndShippingPrices(long cartId, TaxAndShippingPriceRequestVm model);

        Task<Result<Order>> CreateOrderWithDelivery(long cartId, string paymentMethod, decimal paymentFeeAmount, string payerID, string paymentID, OrderStatus orderStatus = OrderStatus.New);
        Task<Result<Order>> CreateOrderWithDelivery(long cartId, string paymentMethod, decimal paymentFeeAmount, decimal shippingAmount, Address billingAddress, Address shippingAddress, string payerID, string paymentID, OrderStatus orderStatus = OrderStatus.New);
        Task<Result<Order>> CreateOrderIsracardWithDelivery(long cartId, string paymentMethod, decimal paymentFeeAmount, decimal tips, OrderStatus orderStatus = OrderStatus.New);
        Task<Result<Order>> CreateOrderIsracardWithDelivery(long cartId, string paymentMethod, decimal paymentFeeAmount, decimal shippingAmount, Address billingAddress, Address shippingAddress, decimal tips, OrderStatus orderStatus = OrderStatus.New);
    }
}
