﻿using Newtonsoft.Json;
using BobCommerce.Infrastructure.Models;
using BobCommerce.Module.Catalog.Models;
using System.Collections.Generic;

namespace BobCommerce.Module.Orders.Models {
    public class OrderItemEdit {
        public List<long> ProductIdRemoved { get; set; }
        public List<long> ProductIdAdded { get; set; }
        public List<int> QuantityAdded { get; set; }
    }
}
