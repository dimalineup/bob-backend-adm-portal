﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BobCommerce.Infrastructure.Models;
using BobCommerce.Module.Core.Models;

namespace BobCommerce.Module.Orders.Models {
    public class Notification : EntityBase {
        public Notification() { }

        public Notification(long id) {
            Id = id;
        }

        public NotificationType MessageType { get; set; }

        public long UserID { get; set; }

        public long WaiterID { get; set; }

        public string TableNo { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public DateTime DateTime { get; set; }

        public bool Sent { get; set; }

        public int Retries { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public User Client { get; set; }

        public User Waiter { get; set; }

        public bool Read { get; set; }

        public long OrderID { get; set; }
    }
}
