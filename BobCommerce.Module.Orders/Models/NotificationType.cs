﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BobCommerce.Module.Orders.Models {
    public enum NotificationType {

        [Description("לא קיבלתי חלק מההזמנה שלי")]
        [DisplayName("I did not receive part of my order")]
        PartOrderNotReceived = 0,

        [Description("לא קיבלתי את כל ההזמנה שלי")]
        [DisplayName("I did not receive all my order")]
        AllOrderNotReceived = 1,

        [Description("שיניתי את השולחן שלי")]
        [DisplayName("I changed my table")]
        TableChanged = 2,

        [Description("התקשר למלצר")]
        [DisplayName("Call the waiter")]
        WaiterCalled = 3,

        [Description("חשבונית")]
        [DisplayName("Invoice")]
        Invoice = 4
    }
}
