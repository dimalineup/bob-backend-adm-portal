﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace BobCommerce.Module.Orders.Models {
    public enum OrderStatus {

        [Description("חדש")]
        [DisplayName("New")]
        New = 1,

        [Description("בהמתנה")]
        [DisplayName("On Hold")]
        OnHold = 10,

        [Description("בהמתנה לתשלום")]
        [DisplayName("Pending Payment")]
        PendingPayment = 20,

        [Description("תשלום התקבל")]
        [DisplayName("Payment Received")]
        PaymentReceived = 30,

        [Description("התשלום נכשל")]
        [DisplayName("Payment Failed")]
        PaymentFailed = 35,

        [Description("חשבונית")]
        [DisplayName("Invoiced")]
        Invoiced = 40,

        [Description("משלוח")]
        [DisplayName("Shipping")]
        Shipping = 50,

        [Description("נשלח")]
        [DisplayName("Shipped")]
        Shipped = 60,

        [Description("שלם")]
        [DisplayName("Complete")]
        Complete = 70,

        [Description("מבוטל")]
        [DisplayName("New")]
        Canceled = 80,

        [Description("החזר כספי")]
        [DisplayName("Refunded")]
        Refunded = 90,

        [Description("סגור")]
        [DisplayName("Closed")]
        Closed = 100
    }
}
