﻿using MediatR;
using BobCommerce.Module.Orders.Areas.Orders.ViewModels;

namespace BobCommerce.Module.Orders.Events
{
    public class OrderDetailGot : INotification {
        public OrderDetailVm OrderDetailVm { get; set; }
    }
}
