﻿using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using BobCommerce.Infrastructure.Modules;
using BobCommerce.Module.Core.Events;
using BobCommerce.Module.ShoppingCart.Services;
using BobCommerce.Module.ShoppingCart.Events;

namespace BobCommerce.Module.ShoppingCart
{
    public class ModuleInitializer : IModuleInitializer {
        public void ConfigureServices(IServiceCollection services) {
            services.AddTransient<ICartService, CartService>();
            services.AddTransient<INotificationHandler<UserSignedIn>, UserSignedInHandler>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {

        }
    }
}
