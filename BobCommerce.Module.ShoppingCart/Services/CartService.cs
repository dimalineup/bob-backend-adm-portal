﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using BobCommerce.Infrastructure;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Module.ShoppingCart.Models;
using BobCommerce.Module.Core.Services;
using BobCommerce.Module.Pricing.Services;
using BobCommerce.Module.ShoppingCart.Areas.ShoppingCart.ViewModels;
using BobCommerce.Module.Catalog.Models;
using BobCommerce.Module.Core.Models;

namespace BobCommerce.Module.ShoppingCart.Services
{
    public class CartService : ICartService {
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<Cart> _cartRepository;
        private readonly IRepository<CartItem> _cartItemRepository;
        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IMediaService _mediaService;
        private readonly ICouponService _couponService;
        private readonly bool _isProductPriceIncludeTax;

        public CartService(IRepository<Product> productRepository, IRepository<Cart> cartRepository, IRepository<CartItem> cartItemRepository, IRepository<Vendor> vendorRepository, 
            ICouponService couponService, IMediaService mediaService, IConfiguration config) {
            _productRepository = productRepository;
            _cartRepository = cartRepository;
            _cartItemRepository = cartItemRepository;
            _vendorRepository = vendorRepository;
            _couponService = couponService;
            _mediaService = mediaService;
            _isProductPriceIncludeTax = config.GetValue<bool>("Catalog.IsProductPriceIncludeTax");
        }

        public IQueryable<Cart> Query() {
            return _cartRepository.Query();
        }

        public Task<Cart> GetActiveCart(long customerId) {
            return GetActiveCart(customerId, customerId);
        }

        public async Task<Cart> GetActiveCart(long customerId, long createdById) {
            return await _cartRepository.Query()
                .Include(x => x.Items).ThenInclude(xx => xx.Product)
                //.Where(x => x.CustomerId == customerId && x.CreatedById == createdById && x.IsActive).FirstOrDefaultAsync();
                .Where(x => x.CustomerId == customerId && x.CreatedById == customerId && x.IsActive).FirstOrDefaultAsync();
        }

        public async Task<Result> AddToCart(long customerId, long productId, int quantity) {
            return await AddToCart(customerId, customerId, productId, quantity);
        }

        public async Task<Result> AddToCart(long customerId, long createdById, long productId, int quantity) {
            var cart = await GetActiveCart(customerId, createdById);
            if (cart == null) {
                cart = new Cart {
                    CustomerId = customerId,
                    //CreatedById = createdById,
                    CreatedById = customerId,
                    IsProductPriceIncludeTax = _isProductPriceIncludeTax
                };

                _cartRepository.Add(cart);
            }
            else {
                if (cart.LockedOnCheckout) {
                    return Result.Fail("Cart is being locked for checkout. Please complete the checkout first");
                }

                cart = await _cartRepository.Query().Include(x => x.Items).FirstOrDefaultAsync(x => x.Id == cart.Id);
            }

            
                var cartItem = cart.Items.FirstOrDefault(x => x.ProductId == productId);
                if (cartItem == null) {
                    cartItem = new CartItem {
                        Cart = cart,
                        ProductId = productId,
                        Quantity = quantity,
                        CreatedOn = DateTimeOffset.Now
                    };

                    cart.Items.Add(cartItem);
                }
                else {
                    cartItem.Quantity = cartItem.Quantity + quantity;
                }

            var mainProduct = _productRepository.Query().Include(x => x.ProductLinks).FirstOrDefault(x => x.Id == productId);
            if (mainProduct.ProductLinks != null && mainProduct.ProductLinks.Count > 0) {
                foreach (var prodLink in mainProduct.ProductLinks) {
                    if (prodLink.LinkType == ProductLinkType.Related) {
                        var cartItemRelated = cart.Items.FirstOrDefault(x => x.ProductId == prodLink.LinkedProductId);
                        if (cartItemRelated == null) {
                            cartItemRelated = new CartItem {
                                Cart = cart,
                                ProductId = prodLink.LinkedProductId,
                                Quantity = 1,
                                CreatedOn = DateTimeOffset.Now
                            };

                            cart.Items.Add(cartItemRelated);
                        }
                        else {
                            cartItemRelated.Quantity = cartItemRelated.Quantity + quantity;
                        }
                    }
                }
            }

            await _cartRepository.SaveChangesAsync();

            //return Result.Ok();
            return Result.Ok(cart.Id);
        }

        public async Task<Result> AddToCartItems(long customerId, long createdById, long productId, int quantity, bool dressing, bool extraDressing) {
            var cart = await GetActiveCart(customerId, createdById);
            if (cart == null) {
                cart = new Cart {
                    CustomerId = customerId,
                    CreatedById = customerId,
                    IsProductPriceIncludeTax = _isProductPriceIncludeTax
                };

                _cartRepository.Add(cart);
            }
            else {
                if (cart.LockedOnCheckout) {
                    return Result.Fail("Cart is being locked for checkout. Please complete the checkout first");
                }

                cart = await _cartRepository.Query().Include(x => x.Items).FirstOrDefaultAsync(x => x.Id == cart.Id);
            }

            //this one is included, add quantity 0
            if (dressing) {
                var cartItem = new CartItem {
                    Cart = cart,
                    ProductId = productId,
                    Quantity = 0,
                    CreatedOn = DateTimeOffset.Now
                };

                cart.Items.Add(cartItem);
            }

            //this one is extra, add quantity
            else if (extraDressing) {
                var cartItem = new CartItem {
                    Cart = cart,
                    ProductId = productId,
                    Quantity = quantity,
                    CreatedOn = DateTimeOffset.Now
                };

                cart.Items.Add(cartItem);
            }

            //not dressing, not extra dressing
            else if (!dressing && !extraDressing) {
                var cartItem = cart.Items.FirstOrDefault(x => x.ProductId == productId);
                if (cartItem == null) {
                    cartItem = new CartItem {
                        Cart = cart,
                        ProductId = productId,
                        Quantity = quantity,
                        CreatedOn = DateTimeOffset.Now
                    };

                    cart.Items.Add(cartItem);
                }
                else {
                    cartItem.Quantity = cartItem.Quantity + quantity;
                }
            }

            await _cartRepository.SaveChangesAsync();

            return Result.Ok(cart.Id);
        }

        public async Task<CartVm> GetActiveCartDetails(long customerId) {
            return await GetActiveCartDetails(customerId, customerId);
        }

        public async Task<CartVm> GetActiveCartDetailsWithTips(long customerId, decimal tips) {
            return await GetActiveCartDetailsWithTips(customerId, customerId, tips);
        }

        public async Task<CartVm> GetActiveCartDetailsWithTips(long customerId, long createdById, decimal tips) {
            var cart = await GetActiveCart(customerId, createdById);
            if (cart == null) {
                return null;
            }

            var cartVm = new CartVm() {
                Id = cart.Id,
                CouponCode = cart.CouponCode,
                IsProductPriceIncludeTax = cart.IsProductPriceIncludeTax,
                TaxAmount = cart.TaxAmount,
                ShippingAmount = cart.ShippingAmount,
                OrderNote = cart.OrderNote,
                Tips = tips
            };

            cartVm.Items = _cartItemRepository
            .Query()
            .Include(x => x.Product).ThenInclude(p => p.ThumbnailImage)
            .Include(x => x.Product).ThenInclude(p => p.OptionCombinations).ThenInclude(o => o.Option)
            .Where(x => x.CartId == cart.Id)
            .Select(x => new CartItemVm {
                Id = x.Id,
                ProductId = x.ProductId,
                ProductName = x.Product.Name,
                ProductPrice = x.Product.Price,
                ProductStockQuantity = x.Product.StockQuantity,
                ProductStockTrackingIsEnabled = x.Product.StockTrackingIsEnabled,
                IsProductAvailabeToOrder = x.Product.IsAllowToOrder && x.Product.IsPublished && !x.Product.IsDeleted,
                ProductImage = _mediaService.GetThumbnailUrl(x.Product.ThumbnailImage),
                Quantity = x.Quantity,
                VariationOptions = CartItemVm.GetVariationOption(x.Product)
            }).ToList();

            cartVm.SubTotal = cartVm.Items.Sum(x => x.Quantity * x.ProductPrice);

            if (!string.IsNullOrWhiteSpace(cartVm.CouponCode)) {
                var cartInfoForCoupon = new CartInfoForCoupon {
                    Items = cartVm.Items.Select(x => new CartItemForCoupon { ProductId = x.ProductId, Quantity = x.Quantity }).ToList()
                };
                var couponValidationResult = await _couponService.Validate(customerId, cartVm.CouponCode, cartInfoForCoupon);
                if (couponValidationResult.Succeeded) {
                    cartVm.Discount = couponValidationResult.DiscountAmount;
                }
                else {
                    cartVm.CouponValidationErrorMessage = couponValidationResult.ErrorMessage;
                }
            }

            var thisVendor = cart.Items.Where(x => x.Product.VendorId.HasValue).Select(x => x.Product.VendorId.Value).FirstOrDefault();
            if (thisVendor > 0) {
                var vendor = _vendorRepository.Query().Where(x => x.Id == thisVendor).FirstOrDefault();
                if (vendor != null) {
                    decimal locationDiscount = vendor.Discount;
                    cartVm.LocationDiscount = locationDiscount / 100 * (cartVm.SubTotal + cartVm.Tips - cartVm.Discount);
                }
            }

            return cartVm;
        }

        // TODO separate getting product thumbnail, varation options from here
        public async Task<CartVm> GetActiveCartDetails(long customerId, long createdById) {
            var cart = await GetActiveCart(customerId, createdById);
            if (cart == null) {
                return null;
            }

            var cartVm = new CartVm() {
                Id = cart.Id,
                CouponCode = cart.CouponCode,
                IsProductPriceIncludeTax = cart.IsProductPriceIncludeTax,
                TaxAmount = cart.TaxAmount,
                ShippingAmount = cart.ShippingAmount,
                OrderNote = cart.OrderNote
            };

                cartVm.Items = _cartItemRepository
                .Query()
                .Include(x => x.Product).ThenInclude(p => p.ThumbnailImage)
                .Include(x => x.Product).ThenInclude(p => p.OptionCombinations).ThenInclude(o => o.Option)
                .Where(x => x.CartId == cart.Id)
                .Select(x => new CartItemVm {
                    Id = x.Id,
                    ProductId = x.ProductId,
                    ProductName = x.Product.Name,
                    ProductPrice = x.Product.Price,
                    ProductStockQuantity = x.Product.StockQuantity,
                    ProductStockTrackingIsEnabled = x.Product.StockTrackingIsEnabled,
                    IsProductAvailabeToOrder = x.Product.IsAllowToOrder && x.Product.IsPublished && !x.Product.IsDeleted,
                    ProductImage = _mediaService.GetThumbnailUrl(x.Product.ThumbnailImage),
                    Quantity = x.Quantity,
                    VariationOptions = CartItemVm.GetVariationOption(x.Product)
                }).ToList();

            cartVm.SubTotal = cartVm.Items.Sum(x => x.Quantity * x.ProductPrice);

            if (!string.IsNullOrWhiteSpace(cartVm.CouponCode)) {
                var cartInfoForCoupon = new CartInfoForCoupon {
                    Items = cartVm.Items.Select(x => new CartItemForCoupon { ProductId = x.ProductId, Quantity = x.Quantity }).ToList()
                };
                var couponValidationResult = await _couponService.Validate(customerId, cartVm.CouponCode, cartInfoForCoupon);
                if (couponValidationResult.Succeeded) {
                    cartVm.Discount = couponValidationResult.DiscountAmount;
                }
                else {
                    cartVm.CouponValidationErrorMessage = couponValidationResult.ErrorMessage;
                }
            }

            var thisVendor = cart.Items.Where(x => x.Product.VendorId.HasValue).Select(x => x.Product.VendorId.Value).FirstOrDefault();
            if (thisVendor > 0) {
                var vendor = _vendorRepository.Query().Where(x => x.Id == thisVendor).FirstOrDefault();
                if (vendor != null) {
                    decimal locationDiscount = vendor.Discount;
                    cartVm.LocationDiscount = locationDiscount / 100 * (cartVm.SubTotal - cartVm.Discount);
                }
            }

            return cartVm;
        }

        public async Task<CouponValidationResult> ApplyCoupon(long cartId, string couponCode) {
            var cart = _cartRepository.Query().Include(x => x.Items).FirstOrDefault(x => x.Id == cartId);

            var cartInfoForCoupon = new CartInfoForCoupon {
                Items = cart.Items.Select(x => new CartItemForCoupon { ProductId = x.ProductId, Quantity = x.Quantity }).ToList()
            };
            var couponValidationResult = await _couponService.Validate(cart.CustomerId, couponCode, cartInfoForCoupon);
            if (couponValidationResult.Succeeded) {
                cart.CouponCode = couponCode;
                cart.CouponRuleName = couponValidationResult.CouponRuleName;
                _cartItemRepository.SaveChanges();
            }

            return couponValidationResult;
        }

        public async Task MigrateCart(long fromUserId, long toUserId) {
            var cartFrom = await GetActiveCart(fromUserId);
            if (cartFrom != null && cartFrom.Items.Any()) {
                var cartTo = await GetActiveCart(toUserId);
                if (cartTo == null) {
                    cartTo = new Cart {
                        CustomerId = toUserId,
                        CreatedById = toUserId
                    };

                    _cartRepository.Add(cartTo);
                }

                foreach (var fromItem in cartFrom.Items) {
                    var toItem = cartTo.Items.FirstOrDefault(x => x.ProductId == fromItem.ProductId);
                    if (toItem == null) {
                        toItem = new CartItem {
                            Cart = cartTo,
                            ProductId = fromItem.ProductId,
                            Quantity = fromItem.Quantity,
                            CreatedOn = DateTimeOffset.Now
                        };
                        cartTo.Items.Add(toItem);
                    }
                    else {
                        toItem.Quantity = toItem.Quantity + fromItem.Quantity;
                    }
                }

                await _cartRepository.SaveChangesAsync();
            }
        }
    }
}
