﻿using System.Linq;
using System.Threading.Tasks;
using BobCommerce.Infrastructure;
using BobCommerce.Module.Pricing.Services;
using BobCommerce.Module.ShoppingCart.Areas.ShoppingCart.ViewModels;
using BobCommerce.Module.ShoppingCart.Models;

namespace BobCommerce.Module.ShoppingCart.Services
{
    public interface ICartService {
        Task<Result> AddToCart(long customerId, long productId, int quantity);

        Task<Result> AddToCart(long customerId, long createdById, long productId, int quantity);

        Task<Result> AddToCartItems(long customerId, long createdById, long productId, int quantity, bool dressing, bool extraDressing);

        IQueryable<Cart> Query();

        Task<Cart> GetActiveCart(long customerId);

        Task<Cart> GetActiveCart(long customerId, long createdById);

        Task<CartVm> GetActiveCartDetails(long customerId);
        Task<CartVm> GetActiveCartDetailsWithTips(long customerId, decimal tips);

        Task<CartVm> GetActiveCartDetails(long customerId, long createdById);
        Task<CartVm> GetActiveCartDetailsWithTips(long customerId, long createdById, decimal tips);

        Task<CouponValidationResult> ApplyCoupon(long cartId, string couponCode);

        Task MigrateCart(long fromUserId, long toUserId);
    }
}
