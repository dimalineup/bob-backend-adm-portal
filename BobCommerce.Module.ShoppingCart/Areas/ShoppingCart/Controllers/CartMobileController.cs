﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Module.Core.Extensions;
using BobCommerce.Module.ShoppingCart.Areas.ShoppingCart.ViewModels;
using BobCommerce.Module.ShoppingCart.Models;
using BobCommerce.Module.ShoppingCart.Services;
using System.Collections.Generic;
using BobCommerce.Infrastructure;

namespace BobCommerce.Module.ShoppingCart.Areas.ShoppingCart.Controllers
{
    [Area("ShoppingCart")]
    //[Authorize(Roles = "admin")]
    public class CartMobileController : Controller {
        private readonly IRepository<CartItem> _cartItemRepository;
        private readonly ICartService _cartService;
        private readonly IWorkContext _workContext;

        public CartMobileController(
            IRepository<CartItem> cartItemRepository,
            ICartService cartService,
            IWorkContext workContext) {
            _cartItemRepository = cartItemRepository;
            _cartService = cartService;
            _workContext = workContext;
        }

        [HttpPost("customers/{customerId}/add-cart-item")]
        public async Task<IActionResult> AddToCart(long customerId, [FromBody] AddToCartModel model) {
            var currentUser = await _workContext.GetCurrentUser();
            Result res = await _cartService.AddToCart(customerId, currentUser.Id, model.ProductId, model.Quantity);

            return Ok(res);
        }

        [HttpPost("customers/{customerId}/add-cart-items")]
        public async Task<IActionResult> AddToCart(long customerId, [FromBody] List<AddToCartMobileModel> model) {
            var currentUser = await _workContext.GetCurrentUser();
            List<Result> final = new List<Result>();
            
            for(var i = 0; i < model.Count; i++) { 
                Result res = await _cartService.AddToCartItems(customerId, currentUser.Id, model[i].ProductId, model[i].Quantity, model[i].Dressing, model[i].ExtraDressing);
                final.Add(res);
            }

            return Ok(final);
        }
    }
}
