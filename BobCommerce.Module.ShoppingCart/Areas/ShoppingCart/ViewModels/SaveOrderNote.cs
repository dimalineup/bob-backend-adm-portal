﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BobCommerce.Module.ShoppingCart.Areas.ShoppingCart.ViewModels
{
    public class SaveOrderNote {
        public string OrderNote { get; set; }
    }
}
