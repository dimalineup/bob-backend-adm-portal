﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BobCommerce.Module.ShoppingCart.Areas.ShoppingCart.ViewModels
{
    public class CartQuantityUpdate {
        public long CartItemId { get; set; }

        public int Quantity { get; set; }
    }
}
