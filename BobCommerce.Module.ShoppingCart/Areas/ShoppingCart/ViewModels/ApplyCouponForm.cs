﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BobCommerce.Module.ShoppingCart.Areas.ShoppingCart.ViewModels
{
    public class ApplyCouponForm {
        public string CouponCode { get; set; }
    }
}
