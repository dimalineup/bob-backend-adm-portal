﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BobCommerce.Module.ShoppingCart.Areas.ShoppingCart.ViewModels
{
    public class AddToCartMobileModel {
        public long ProductId { get; set; }

        public string VariationName { get; set; }

        public int Quantity { get; set; }

        public bool Dressing { get; set; }

        public bool ExtraDressing { get; set; }
    }
}
