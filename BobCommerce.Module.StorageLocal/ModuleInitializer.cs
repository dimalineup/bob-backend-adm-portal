﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using BobCommerce.Infrastructure.Modules;
using BobCommerce.Module.Core.Services;

namespace BobCommerce.Module.StorageLocal {
    public class ModuleInitializer : IModuleInitializer {
        public void ConfigureServices(IServiceCollection serviceCollection) {
            serviceCollection.AddSingleton<IStorageService, LocalStorageService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {

        }
    }
}
