﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using BobCommerce.Module.Core.Extensions;
using System.IO;

namespace BobCommerce {
    public class Program {
        public static void Main(string[] args) {
            try {
                //BuildWebHost2(args).Run();

                var host = Microsoft.AspNetCore.WebHost.CreateDefaultBuilder(args)
                    //.UseContentRoot(Directory.GetCurrentDirectory())
                    //.UseIIS()
                    .UseStartup<Startup>()
                    .ConfigureAppConfiguration(SetupConfiguration)
                    .ConfigureLogging(SetupLogging)
                    .Build();

                host.Run();

                //CreateWebHostBuilder(args);
            }
            catch (Exception ex) {
                Console.WriteLine(ex);
            }
        }

        // Changed to BuildWebHost2 to make EF don't pickup during design time
        private static IWebHost BuildWebHost2(string[] args) =>
            Microsoft.AspNetCore.WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .ConfigureAppConfiguration(SetupConfiguration)
                .ConfigureLogging(SetupLogging)
                .Build();

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        //public static IWebHost CreateWebHostBuilder(string[] args) =>
            Microsoft.AspNetCore.WebHost.CreateDefaultBuilder(args)
            .UseSetting(WebHostDefaults.DetailedErrorsKey, "true")
            .UseIISIntegration()
            .UseIIS()
            .ConfigureAppConfiguration(SetupConfiguration)
            .ConfigureLogging(SetupLogging)
            .UseStartup<Startup>();

        private static void SetupConfiguration(WebHostBuilderContext hostingContext, IConfigurationBuilder configBuilder) {
            var env = hostingContext.HostingEnvironment;
            var configuration = configBuilder.Build();
            configBuilder.AddEntityFrameworkConfig(options =>
                    options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"))
                    //options.UseSqlServer("Server=PCMIHA\\SQLEXPRESS;Database=SimplCommerce;Trusted_Connection=True;MultipleActiveResultSets=true")
            );
            Log.Logger = new LoggerConfiguration()
                       .ReadFrom.Configuration(configuration)
                       .CreateLogger();
        }
        private static void SetupLogging(WebHostBuilderContext hostingContext, ILoggingBuilder loggingBuilder) {
            loggingBuilder.AddSerilog();
        }
    }
}
