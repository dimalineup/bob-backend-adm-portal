﻿using BobCommerce.Data;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Module.Core.Data;
using BobCommerce.Module.Core.Models;
using BobCommerce.Module.Orders.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace BobCommerce {

    public class NotificationExtended {
        public long ID { get; set; }

        public NotificationType MessageType { get; set; }

        public string UserFullName { get; set; }

        public long WaiterID { get; set; }

        public string TableNo { get; set; }

        public string Title { get; set; }

        public string Message { get; set; }

        public bool Sent { get; set; }

        public int Retries { get; set; }

        public string DeviceToken { get; set; }

        public string DeviceType { get; set; }
    }

    internal class TimedHostedService : IHostedService, IDisposable {
        private readonly ILogger _logger;
        private Timer _timer;
        private readonly IServiceScopeFactory scopeFactory;
        private readonly IOptions<MySettings> appSettings;
        private readonly HttpClient _httpClient;

        public TimedHostedService(ILogger<TimedHostedService> logger, IServiceScopeFactory scopeFactory, IOptions<MySettings> app) {
            this.scopeFactory = scopeFactory;
            appSettings = app;
            _httpClient = new HttpClient();
        }

        public Task StartAsync(CancellationToken cancellationToken) {

            WriteToLog("StartAsync");

            int notifTime = 300;
            try {
                var time = appSettings.Value.NotificationTime;
                if (!string.IsNullOrEmpty(time)) {
                    notifTime = int.Parse(time);
                }
            }
            catch (Exception ex) { }

            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(notifTime));

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken) {
            WriteToLog("StopAsync");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        private void DoWork(object state) {
            try {

                using (var scope = scopeFactory.CreateScope()) {
                    var dbContext = scope.ServiceProvider.GetRequiredService<SimpleDbContext>();
                    var notifs = dbContext.Notification.Where(x => x.DateTime < DateTime.Now && x.Sent == false).Join(dbContext.User, n => n.WaiterID, u => u.Id, (n, u) => new { n, u})
                        .Select(i => new NotificationExtended {
                            ID = i.n.Id,
                            MessageType = i.n.MessageType,
                            UserFullName = i.u.FullName,
                            WaiterID = i.n.WaiterID,
                            TableNo = i.n.TableNo,
                            Title = i.n.Title,
                            Message = i.n.Message,
                            DeviceToken = i.u.DeviceToken,
                            DeviceType = i.u.DeviceType,
                            Retries = i.n.Retries,
                            Sent = i.n.Sent
                        });
                    if(notifs != null) {
                        var all = notifs.ToList();
                        foreach(var item in all) {
                            var itemNotif = dbContext.Notification.Where(x => x.Id == item.ID).FirstOrDefault();
                            try {

                                HttpContent content = new FormUrlEncodedContent(new[]
                                        {
                                    new KeyValuePair<string, string>("authkey", appSettings.Value.NotificationAuthKey),
                                    new KeyValuePair<string, string>("to", item.DeviceToken),
                                    new KeyValuePair<string, string>("title", item.Title),
                                    new KeyValuePair<string, string>("body", item.Message),
                                    new KeyValuePair<string, string>("device", item.DeviceType),
                                    new KeyValuePair<string, string>("type", item.MessageType.ToString()),
                                    new KeyValuePair<string, string>("url", "")
                                });

                                var response = _httpClient.PostAsync(appSettings.Value.NotificationUrl, content).Result;

                                if (response.IsSuccessStatusCode) {
                                    var responseContent = response.Content;

                                    string responseString = responseContent.ReadAsStringAsync().Result;
                                    if (responseString == "true") {
                                        itemNotif.Sent = true;
                                        dbContext.SaveChanges();
                                        WriteToLog("Success Post DoWork");
                                    }
                                    else {
                                        itemNotif.Sent = true;
                                        itemNotif.Retries = itemNotif.Retries + 1;
                                        dbContext.SaveChanges();
                                        WriteToLog("Error Post DoWork");
                                    }
                                }
                                else {
                                    itemNotif.Sent = true;
                                    itemNotif.Retries = itemNotif.Retries + 1;
                                    dbContext.SaveChanges();
                                    WriteToLog("Error url Post DoWork");
                                }

                            }
                            catch(Exception ex) {
                                WriteToLog("DoWork: " + ex.Message + " " + ex.InnerException);
                                itemNotif.Retries = itemNotif.Retries + 1;
                                dbContext.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex) {
                WriteToLog("Error DoWork: " + ex.Message + " " + ex.InnerException);
            }

            WriteToLog("DoWork is running");
        }

        public static void WriteToLog(string log) {
            try {
                string path = Path.Combine(Directory.GetCurrentDirectory(), "applogs", DateTime.Now.ToString("yyyy-MM-dd_") + "log.txt");

                File.AppendAllText(path, DateTime.Now + "\r\n" + log + "\r\n");
            }
            catch (Exception ex) { }
        }

        public void Dispose() {
            _timer?.Dispose();
        }
    }
}
