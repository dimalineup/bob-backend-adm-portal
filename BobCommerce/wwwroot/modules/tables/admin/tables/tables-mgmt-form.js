﻿/*global angular, jQuery*/
(function ($) {
    angular
        .module('simplAdmin.tables')
        .controller('TablesMgmtFormCtrl', TablesMgmtFormCtrl);

    /* @ngInject */
    function TablesMgmtFormCtrl($state, $stateParams, tableService, translateService) {
        var vm = this;
        vm.translate = translateService;
        vm.tableMgmt = {};
        vm.vendors = [];
        vm.users = [];
        vm.vendorTables = [];
        vm.assignedTables = [];
        vm.tableId = $stateParams.id;
        vm.isEditMode = vm.tableId > 0;

        vm.save = function save() {
            var promise;
            if (vm.isEditMode) {
                promise = tableService.editTableMgmt(vm);
            } else {
                promise = tableService.createTableMgmt(vm);
            }

            promise
                .then(function (result) {
                    $state.go('tables');
                })
                .catch(function (response) {
                    var error = response.data;
                    vm.validationErrors = [];
                    if (error && angular.isObject(error)) {
                        for (var key in error) {
                            vm.validationErrors.push(error[key][0]);
                        }
                    } else {
                        vm.validationErrors.push('Could not add/update table assignment.');
                    }
                });
        };

        function getVendors() {
            tableService.getVendors().then(function (result) {
                vm.vendors = result.data;
            });
        }

        function getUsers(vendorId) {
            tableService.getUsers(vendorId).then(function (result) {
                vm.users = result.data;
            });
        }

        function getVendorTables(vendorId) {
            tableService.getVendorTables(vendorId).then(function (result) {
                vm.vendorTables = result.data;
            });
        }

        vm.onChangeVendor = function onChangeVendor(vendorId) {
            getUsers(vendorId);
            getVendorTables(vendorId);
        };

        vm.toggleTables = function toggleTables(tableId) {
            var index = vm.assignedTables.indexOf(tableId);
            if (index > -1) {
                vm.assignedTables.splice(index, 1);
            } else {
                vm.assignedTables.push(tableId);
            }
        };

        function init() {
            if (vm.isEditMode) {
                tableService.getTableMgmt(vm.tableId).then(function (result) {
                    vm = result.data;
                });
            }

            getVendors();

            tableService.getMyVendor().then(function (result) {
                if (result.data != null && result.data > 0) {
                    vm.vendorId = result.data;
                    vm.onChangeVendor(vm.vendorId);
                }
            });
        }

        init();
    }
})(jQuery);