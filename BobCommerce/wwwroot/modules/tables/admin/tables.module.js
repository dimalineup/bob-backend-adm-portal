﻿/*global angular*/
(function () {
    'use strict';

    angular.module('simplAdmin.tables', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('tables', {
                    url: '/tables',
                    templateUrl: 'modules/tables/admin/tables/tables-list.html',
                    controller: 'TablesListCtrl as vm'
                })
                .state('tables-create', {
                    url: '/tables/create',
                    templateUrl: 'modules/tables/admin/tables/tables-form.html',
                    controller: 'TablesFormCtrl as vm'
                })
                .state('tables-edit', {
                    url: '/tables/edit/:id',
                    templateUrl: 'modules/tables/admin/tables/tables-form.html',
                    controller: 'TablesFormCtrl as vm'
                })
                .state('tables-mgmt', {
                    url: '/tables/mgmt',
                    templateUrl: 'modules/tables/admin/tables/tables-list.html',
                    controller: 'TablesMgmtListCtrl as vm'
                })
                .state('tables-mgmt-create', {
                    url: '/tables/mgmt/create',
                    templateUrl: 'modules/tables/admin/tables/tables-mgmt-form.html',
                    controller: 'TablesMgmtFormCtrl as vm'
                })
                .state('tables-mgmt-edit', {
                    url: '/tables/mgmt/edit/:id',
                    templateUrl: 'modules/tables/admin/tables/tables-mgmt-form.html',
                    controller: 'TablesMgmtFormCtrl as vm'
                })
        }]);
})();