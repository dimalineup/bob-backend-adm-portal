﻿/*global angular*/
(function () {
    angular
        .module('simplAdmin.pricing')
        .factory('cartRuleService', cartRuleService);

    /* @ngInject */
    function cartRuleService($http, Upload) {
        var service = {
            getCartRule: getCartRule,
            createCartRule: createCartRule,
            editCartRule: editCartRule,
            deleteCartRule: deleteCartRule,
            getCartRules: getCartRules,
            getVendors: getVendors
        };
        return service;

        function getCartRule(id) {
            return $http.get('api/cart-rules/' + id);
        }

        function getCartRules(params) {
            return $http.post('api/cart-rules/grid', params);
        }

        //function createCartRule(cartRule) {
        //    return $http.post('api/cart-rules', cartRule);
        //}
        function createCartRule(cartRule, thumbnailImage) {
            return Upload.upload({
                url: 'api/cart-rules',
                data: {
                    cartRule: cartRule,
                    thumbnailImage: thumbnailImage
                }
            });
        }

        //function editCartRule(cartRule) {
        //    return $http.put('api/cart-rules/' + cartRule.id, cartRule);
        //}
        function editCartRule(cartRule, thumbnailImage) {
            return Upload.upload({
                url: 'api/cart-rules/' + cartRule.id,
                method: 'PUT',
                data: {
                    cartRule: cartRule,
                    thumbnailImage: thumbnailImage
                }
            });
        }

        function deleteCartRule(cartRule) {
            return $http.delete('api/cart-rules/' + cartRule.id, null);
        }

        function getVendors() {
            return $http.get('api/vendors');
        }
    }
})();