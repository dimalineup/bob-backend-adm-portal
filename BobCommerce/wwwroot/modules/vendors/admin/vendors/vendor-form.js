﻿ /*global angular*/
(function () {
    angular
        .module('simplAdmin.vendors')
        .controller('VendorFormCtrl', VendorFormCtrl);

    /* @ngInject */
    function VendorFormCtrl($state, $stateParams, vendorService, translateService) {
        var vm = this;
        vm.translate = translateService;
        vm.vendor = {};
        vm.vendorId = $stateParams.id;
        vm.isEditMode = vm.vendorId > 0;

        var isPlaceChanged = false;

        var input = document.getElementById('address');
        //var options = {
        //    types: ['establishment']
        //};

        var autocomplete = new google.maps.places.Autocomplete(input); //, options);

        autocomplete.addListener('place_changed', function () {
            isPlaceChanged = true;
            var place = autocomplete.getPlace();
            vm.vendor.address = $("#address").val();
        });

        $("#address").keydown(function () {
            isPlaceChanged = false;
        });

        vm.updateSlug = function () {
            vm.vendor.slug = slugify(vm.vendor.name);
        };

        vm.save = function save() {
            vm.vendor.nameHE = vm.vendor.nameHE === null ? '' : vm.vendor.nameHE;
            vm.vendor.descriptionHE = vm.vendor.descriptionHE === null ? '' : vm.vendor.descriptionHE;
            vm.vendor.addressHE = vm.vendor.addressHE === null ? '' : vm.vendor.addressHE;
            if (!isPlaceChanged) {
                vm.vendor.address = "Tel Aviv University, Tel Aviv-Yafo, Israel";
            }
            var promise;
            if (vm.isEditMode) {
                $.ajax({
                    url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + vm.vendor.address + '&key=' + vm.vendor.mapsGoogleKey,
                    type: 'get',
                    async: false,
                    data: {},
                    success: function (doc) {
                        if (doc != null && doc.status == "OK" && doc.results != null && doc.results.length > 0 && doc.results[0].geometry != null && doc.results[0].geometry.location != null) {
                            vm.vendor.latitude = doc.results[0].geometry.location.lat;
                            vm.vendor.longitude = doc.results[0].geometry.location.lng;

                            vm.vendor.workingHours = $('[name="workingHours"]').val();
                            promise = vendorService.editVendor(vm.vendor, vm.thumbnailImage);
                        }
                        else {
                            vm.vendor.workingHours = $('[name="workingHours"]').val();
                            promise = vendorService.editVendor(vm.vendor, vm.thumbnailImage);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("error: " + jqXHR.toString());

                        vm.vendor.workingHours = $('[name="workingHours"]').val();
                        promise = vendorService.editVendor(vm.vendor, vm.thumbnailImage);
                    }
                });
                
            } else {
                $.ajax({
                    url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + vm.vendor.address + '&key=' + vm.vendor.mapsGoogleKey,
                    type: 'get',
                    async: false,
                    data: {},
                    success: function (doc) {
                        if (doc != null && doc.status == "OK" && doc.results != null && doc.results.length > 0 && doc.results[0].geometry != null && doc.results[0].geometry.location != null) {
                            vm.vendor.latitude = doc.results[0].geometry.location.lat;
                            vm.vendor.longitude = doc.results[0].geometry.location.lng;

                            vm.vendor.workingHours = $('[name="workingHours"]').val();
                            promise = vendorService.createVendor(vm.vendor, vm.thumbnailImage);
                        }
                        else {
                            vm.vendor.workingHours = $('[name="workingHours"]').val();
                            promise = vendorService.createVendor(vm.vendor, vm.thumbnailImage);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log("error: " + jqXHR.toString());

                        vm.vendor.workingHours = $('[name="workingHours"]').val();
                        promise = vendorService.createVendor(vm.vendor, vm.thumbnailImage);
                    }
                });
            }

            promise
                .then(function (result) {
                    $state.go('vendors');
                })
                .catch(function (response) {
                    var error = response.data;
                    vm.validationErrors = [];
                    if (error && angular.isObject(error)) {
                        for (var key in error) {
                            vm.validationErrors.push(error[key][0]);
                        }
                    } else {
                        vm.validationErrors.push('Could not add vendor.');
                    }
                });
        };

        function init() {
            if (vm.isEditMode) {
                vendorService.getVendor(vm.vendorId).then(function (result) {
                    vm.vendor = result.data;
                    var allData = JSON.parse(vm.vendor.workingHours);
                    for (var i = 0; i < allData.length; i++) {
                        var item = allData[i];
                        if (item.open == 'Closed') {
                            $('#range-time-' + i).text(item.open);
                            $("#range-day-" + i + " .range-checkbox").prop("checked", false);
                            $("#range-day-" + i).addClass("range-day-disabled");
                        }
                        else {
                            $('#range-time-' + i).text(item.open + " - " + item.close);
                            var start = item.open.split(':')[0];
                            var stop = item.close.split(':')[0];
                            $("#range-slider-" + i).slider({
                                values: [parseInt(start) * 60, parseInt(stop) * 60],
                            });
                        }
                    }
                });
            }
        }

        init();
    }
})();