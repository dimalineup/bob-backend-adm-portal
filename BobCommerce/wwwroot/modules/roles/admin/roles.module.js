﻿/*global angular*/
(function () {
    'use strict';

    angular.module('simplAdmin.roles', [])
        .config(['$stateProvider', function ($stateProvider) {
            $stateProvider
                .state('roles', {
                    url: '/roles',
                    templateUrl: 'modules/roles/admin/roles/roles-list.html',
                    controller: 'RolesListCtrl as vm'
                })
                .state('roles-create', {
                    url: '/roles/create',
                    templateUrl: 'modules/roles/admin/roles/roles-form.html',
                    controller: 'RolesFormCtrl as vm'
                })
                .state('roles-edit', {
                    url: '/roles/edit/:id',
                    templateUrl: 'modules/roles/admin/roles/roles-form.html',
                    controller: 'RolesFormCtrl as vm'
                })
        }]);
})();