#pragma checksum "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "346af7718748c54fd1fbc036c5eddbe9f21ce871"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__SelectLanguagePartial), @"mvc.1.0.view", @"/Views/Shared/_SelectLanguagePartial.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Shared/_SelectLanguagePartial.cshtml", typeof(AspNetCore.Views_Shared__SelectLanguagePartial))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\_ViewImports.cshtml"
using BobCommerce.Module.Core;

#line default
#line hidden
#line 2 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#line 1 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
using Microsoft.AspNetCore.Localization;

#line default
#line hidden
#line 2 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
using BobCommerce.Infrastructure;

#line default
#line hidden
#line 3 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
using BobCommerce.Module.Core.Extensions;

#line default
#line hidden
#line 4 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
using Microsoft.AspNetCore.Mvc.Localization;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"346af7718748c54fd1fbc036c5eddbe9f21ce871", @"/Views/Shared/_SelectLanguagePartial.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"acb221deb7688ddb974f7596e8501e87611cc2da", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__SelectLanguagePartial : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-area", "Localization", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Localization", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "SetLanguage", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("lang-form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("role", new global::Microsoft.AspNetCore.Html.HtmlString("form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(167, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 8 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
  
    var uiCulture = Context.Features.Get<IRequestCultureFeature>().RequestCulture.UICulture.Name;
    var selectedCulture = (await WorkContext.GetCurrentUser()).Culture;

#line default
#line hidden
#line 12 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
 if (GlobalConfiguration.Cultures.Count() > 1) {

#line default
#line hidden
            BeginContext(466, 136, true);
            WriteLiteral("    <li class=\"nav-item dropdown\">\r\n        <a href=\"#\" class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\">\r\n            Language (");
            EndContext();
            BeginContext(603, 9, false);
#line 15 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
                 Write(uiCulture);

#line default
#line hidden
            EndContext();
            BeginContext(612, 108, true);
            WriteLiteral(")\r\n            <span class=\"caret\"></span>\r\n        </a>\r\n        <ul class=\"dropdown-menu lang-selector\">\r\n");
            EndContext();
#line 19 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
             foreach (var culture in GlobalConfiguration.Cultures) {

#line default
#line hidden
            BeginContext(790, 88, true);
            WriteLiteral("                <li class=\"dropdown-item\">\r\n                    <a href=\"#\" data-value=\"");
            EndContext();
            BeginContext(879, 10, false);
#line 21 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
                                       Write(culture.Id);

#line default
#line hidden
            EndContext();
            BeginContext(889, 28, true);
            WriteLiteral("\">\r\n                        ");
            EndContext();
            BeginContext(918, 12, false);
#line 22 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
                   Write(culture.Name);

#line default
#line hidden
            EndContext();
            BeginContext(930, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 23 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
                     if (culture.Id == selectedCulture) {

#line default
#line hidden
            BeginContext(991, 89, true);
            WriteLiteral("                        <span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span>\r\n");
            EndContext();
#line 25 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
                    }

#line default
#line hidden
            BeginContext(1103, 41, true);
            WriteLiteral("                </a>\r\n            </li>\r\n");
            EndContext();
#line 28 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
        }

#line default
#line hidden
            BeginContext(1155, 23, true);
            WriteLiteral("        </ul>\r\n        ");
            EndContext();
            BeginContext(1178, 258, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "346af7718748c54fd1fbc036c5eddbe9f21ce8719353", async() => {
                BeginContext(1348, 49, true);
                WriteLiteral("\r\n            <input type=\"hidden\" name=\"culture\"");
                EndContext();
                BeginWriteAttribute("value", " value=\"", 1397, "\"", 1415, 1);
#line 31 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
WriteAttributeValue("", 1405, uiCulture, 1405, 10, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(1416, 13, true);
                WriteLiteral(" />\r\n        ");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Area = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-returnUrl", "Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 30 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
                                                                                                      WriteLiteral(Context.Request.Path);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues["returnUrl"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-returnUrl", __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.RouteValues["returnUrl"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1436, 13, true);
            WriteLiteral("\r\n    </li>\r\n");
            EndContext();
#line 34 "F:\dotnetcore\projects\BobCommerce\BobCommerce\Views\Shared\_SelectLanguagePartial.cshtml"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IWorkContext WorkContext { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IViewLocalizer Localizer { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
