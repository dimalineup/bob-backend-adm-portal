﻿/*global angular*/
(function () {
    angular
        .module('simplAdmin.vendors')
        .factory('vendorService', vendorService);

    /* @ngInject */
    function vendorService($http, Upload) {
        var service = {
            getVendors: getVendors,
            getVendor: getVendor,
            createVendor: createVendor,
            editVendor: editVendor,
            deleteVendor: deleteVendor,
            impersVendor: impersVendor
        };
        return service;

        function getVendors(params) {
            return $http.post('api/vendors/grid', params);
        }

        function getVendor(id) {
            return $http.get('api/vendors/' + id);
        }

        /*function createVendor(vendor) {
            return $http.post('api/vendors', vendor);
        }*/
        function createVendor(vendor, thumbnailImage) {
            return Upload.upload({
                url: 'api/vendors',
                data: {
                    vendor: vendor,
                    thumbnailImage: thumbnailImage
                }
            });
        }

        /*function editVendor(vendor) {
            return $http.put('api/vendors/' + vendor.id, vendor);
        }*/
        function editVendor(vendor, thumbnailImage) {
            return Upload.upload({
                url: 'api/vendors/' + vendor.id,
                method: 'PUT',
                data: {
                    vendor: vendor,
                    thumbnailImage: thumbnailImage
                }
            });
        }

        function deleteVendor(vendor) {
            return $http.delete('api/vendors/' + vendor.id, null);
        }

        function impersVendor(vendor) {
            return $http.post('api/vendors/impersonate/' + vendor.id);
        }
    }
})();