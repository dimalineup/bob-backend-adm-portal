﻿/*global angular, confirm*/
(function () {
    angular
        .module('simplAdmin.roles')
            .controller('RolesListCtrl', RolesListCtrl);

    /* @ngInject */
    function RolesListCtrl(roleService, translateService) {
        var vm = this;
        vm.translate = translateService;
        vm.roles = [];

        vm.getRoles = function getRoles() {
            roleService.getRoles().then(function (result) {
                vm.roles = result.data;
            });
        };

        vm.deleteRole = function deleteRole(role) {
            bootbox.confirm('Are you sure you want to delete the role ' + role.name, function (result) {
                if (result) {
                    roleService.deleteRole(role)
                        .then(function (result) {
                            vm.getRoles();
                            toastr.success(role.name + ' has been deleted');
                        })
                        .catch(function (response) {
                            toastr.error(response.data.error);
                        });
                }
            });
        };

        roleService.getRoles().then(function (result) {
            vm.roles = result.data;
        });
    }
})();