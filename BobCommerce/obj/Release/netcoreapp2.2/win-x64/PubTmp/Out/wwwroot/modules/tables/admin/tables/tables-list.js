﻿/*global angular, confirm*/
(function () {
    angular
        .module('simplAdmin.tables')
            .controller('TablesListCtrl', TablesListCtrl);

    /* @ngInject */
    function TablesListCtrl(tableService, translateService) {
        var vm = this;
        vm.translate = translateService;
        vm.tables = [];
        vm.vendors = [];
        vm.tablesMgmt = [];
        vm.allUsers = [];

        vm.getTables = function getTables() {
            vm.impersonation = _impersonation == "" ? false : true;
            tableService.getTables().then(function (result) {
                vm.tables = result.data;
            });
        };

        vm.getTablesMgmt = function getTablesMgmt() {
            tableService.getTablesMgmt().then(function (result) {
                vm.tablesMgmt = result.data;
            });
        };

        vm.getAllUsers = function getAllUsers() {
            tableService.getAllUsers().then(function (result) {
                vm.allUsers = result.data;
            });
        };

        vm.deleteTable = function deleteTable(table) {
            bootbox.confirm('Are you sure you want to delete the table ' + table.tableNumber + ' for vendor ' + table.vendorName, function (result) {
                if (result) {
                    tableService.deleteTable(table)
                        .then(function (result) {
                            vm.getTables();
                            toastr.success(table.tableNumber + ' has been deleted');
                        })
                        .catch(function (response) {
                            toastr.error(response.data.error);
                        });
                }
            });
        };

        vm.deleteTableMgmt = function deleteTableMgmt(tableMgmt) {
            bootbox.confirm('Are you sure you want to delete table assignment for waiter ' + tableMgmt.userName, function (result) {
                if (result) {
                    tableService.deleteTableMgmt(tableMgmt)
                        .then(function (result) {
                            vm.getTablesMgmt();
                            toastr.success(tableMgmt.userName + ' has been deleted');
                        })
                        .catch(function (response) {
                            toastr.error(response.data.error);
                        });
                }
            });
        };

        tableService.getVendors().then(function (result) {
            vm.vendors = result.data;
        });

        vm.getTables();
        vm.getAllUsers();
        vm.getTablesMgmt();
    }
})();