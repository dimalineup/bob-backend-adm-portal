﻿/*global angular*/
(function () {
    angular
        .module('simplAdmin.tables')
        .factory('tableService', tableService);

    /* @ngInject */
    function tableService($http) {
        var service = {
            getTable: getTable,
            getTables: getTables,
            createTable: createTable,
            editTable: editTable,
            deleteTable: deleteTable,
            getVendors: getVendors,
            getTableMgmt: getTableMgmt,
            getTablesMgmt: getTablesMgmt,
            createTableMgmt: createTableMgmt,
            editTableMgmt: editTableMgmt,
            deleteTableMgmt: deleteTableMgmt,
            getUsers: getUsers,
            getAllUsers: getAllUsers,
            getVendorTables: getVendorTables,
            getMyVendor: getMyVendor
        };
        return service;

        function getTable(id) {
            return $http.get('api/tables/' + id);
        }

        function getTables() {
            return $http.get('api/tables');
        }

        function createTable(table) {
            return $http.post('api/tables', table);
        }

        function editTable(table) {
            return $http.put('api/tables/' + table.id, table);
        }

        function deleteTable(table) {
            return $http.delete('api/tables/' + table.id, null);
        }

        function getVendors() {
            return $http.get('api/vendors');
        }

        function getTableMgmt(id) {
            return $http.get('api/tables/mgmt/' + id);
        }

        function getTablesMgmt() {
            return $http.get('api/tables/mgmt');
        }

        function createTableMgmt(table) {
            return $http.post('api/tables/mgmt', table);
        }

        function editTableMgmt(table) {
            return $http.put('api/tables/mgmt/' + table.id, table);
        }

        function deleteTableMgmt(table) {
            return $http.delete('api/tables/mgmt/' + table.id, null);
        }

        function getUsers(vendorId) {
            return $http.get('api/tables/users/' + vendorId);
        }

        function getAllUsers() {
            return $http.get('api/tables/all-users');
        }

        function getVendorTables(vendorId) {
            return $http.get('api/tables/vendor/' + vendorId);
        }

        function getMyVendor() {
            return $http.get('api/tables/mgmt/myvendor/');
        }
    }
})();