﻿/*global angular*/
(function () {
    angular
        .module('simplAdmin.core')
        .factory('userService', userService);

    /* @ngInject */
    function userService($http) {
        var service = {
            getUsers: getUsers,
            getUser: getUser,
            quickSearchUsers: quickSearchUsers,
            createUser: createUser,
            editUser: editUser,
            deleteUser: deleteUser,
            getRoles: getRoles,
            getVendors: getVendors,
            getCustomerGroups: getCustomerGroups,
            getMyRole: getMyRole
        };
        return service;

        function getUsers(params) {
            return $http.post('api/users/grid', params);
        }

        function getUser(id) {
            return $http.get('api/users/' + id);
        }

        function quickSearchUsers(name) {
            return $http.get('api/users/quick-search/?name=' + name);
        }

        function createUser(user) {
            return $http.post('api/users', user);
        }

        //function createUser(user, userImage) {
        //    return Upload.upload({
        //        url: 'api/users',
        //        data: {
        //            user: user,
        //            userImage: userImage
        //        }
        //    });
        //}

        function editUser(user) {
            return $http.put('api/users/' + user.id, user);
        }

        //function editUser(user, userImage) {
        //    return Upload.upload({
        //        url: 'api/users/' + user.id,
        //        method: 'PUT',
        //        data: {
        //            user: user,
        //            userImage: userImage
        //        }
        //    });
        //}

        function deleteUser(user) {
            return $http.delete('api/users/' + user.id, null);
        }

        function getRoles() {
            return $http.get('api/roles');
        }

        function getVendors() {
            return $http.get('api/vendors');
        }

        function getCustomerGroups() {
            return $http.get('api/customergroups');
        }

        function getMyRole() {
            return $http.get('api/orders/myrole');
        }
    }
})();
