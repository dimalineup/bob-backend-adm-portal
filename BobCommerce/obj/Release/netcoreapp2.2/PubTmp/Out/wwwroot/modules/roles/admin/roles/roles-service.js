﻿/*global angular*/
(function () {
    angular
        .module('simplAdmin.roles')
        .factory('roleService', roleService);

    /* @ngInject */
    function roleService($http) {
        var service = {
            getRole: getRole,
            getRoles: getRoles,
            createRole: createRole,
            editRole: editRole,
            deleteRole: deleteRole,
            getMyRole: getMyRole
        };
        return service;

        function getRole(id) {
            return $http.get('api/roles/' + id);
        }

        function getRoles() {
            return $http.get('api/roles');
        }

        function createRole(role) {
            return $http.post('api/roles', role);
        }

        function editRole(role) {
            return $http.put('api/roles/' + role.id, role);
        }

        function deleteRole(role) {
            return $http.delete('api/roles/' + role.id, null);
        }

        function getMyRole() {
            return $http.get('api/orders/myrole');
        }
    }
})();