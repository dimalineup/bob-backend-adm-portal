﻿/*global angular, jQuery*/
(function ($) {
    angular
        .module('simplAdmin.tables')
        .controller('TablesFormCtrl', TablesFormCtrl);

    /* @ngInject */
    function TablesFormCtrl($state, $stateParams, tableService, translateService) {
        var vm = this;
        vm.translate = translateService;
        vm.table = {};
        vm.vendors = [];
        vm.tableId = $stateParams.id;
        vm.isEditMode = vm.tableId > 0;

        vm.save = function save() {
            var promise;
            if (vm.isEditMode) {
                promise = tableService.editTable(vm.table);
            } else {
                promise = tableService.createTable(vm.table);
            }

            promise
                .then(function (result) {
                    $state.go('tables');
                })
                .catch(function (response) {
                    var error = response.data;
                    vm.validationErrors = [];
                    if (error && angular.isObject(error)) {
                        for (var key in error) {
                            vm.validationErrors.push(error[key][0]);
                        }
                    } else {
                        vm.validationErrors.push('Could not add/update table.');
                    }
                });
        };

        function getVendors() {
            tableService.getVendors().then(function (result) {
                vm.vendors = result.data;
            });
        }

        function init() {
            if (vm.isEditMode) {
                tableService.getTable(vm.tableId).then(function (result) {
                    vm.table = result.data;
                });
            }

            getVendors();
        }

        init();
    }
})(jQuery);