﻿/*global angular*/
(function () {
    angular
        .module('simplAdmin.orders')
        .controller('OrderDetailCtrl', OrderDetailCtrl);

    /* @ngInject */
    function OrderDetailCtrl($state, $stateParams, orderService, translateService) {
        var vm = this;
        vm.translate = translateService;
        vm.orderId = $stateParams.id;
        vm.order = {};
        vm.orderStatus = [];
        vm.orderHistories = [];
        vm.orderProducts = [];
        vm.myRole = "";
        vm.orderItems = {
            "ProductIdRemoved": [],
            "ProductIdAdded": [],
            "QuantityAdded": []
        };

        vm.changeOrderStatus = function () {
            orderService.changeOrderStatus(vm.order.id, { statusId : vm.order.orderStatus, note : vm.orderStatusNote })
                .then(function () {
                    vm.order.orderStatusString = vm.orderStatus.find(function (item) { return item.id === vm.order.orderStatus; }).name;
                    toastr.success('The order now is ' + vm.order.orderStatusString);
                    vm.orderStatusNote = '';
                    getOrderHistory();
                })
                .catch(function (response) {
                    toastr.error(response.data.error);
                });
        };

        vm.chargeOrder = function () {
            orderService.chargeOrder(vm.order.id)
                .then(function () {
                    getOrder();
                })
                .catch(function (response) {
                    toastr.error(response.data.error);
                });
        };

        vm.editOrder = function () {
            if (vm.orderProducts != null) {
                for (var i = 0; i < vm.orderProducts.length; i++) {
                    vm.orderItems.ProductIdAdded.push(vm.orderProducts[i].id);
                    vm.orderItems.QuantityAdded.push(vm.orderProducts[i].quantity);
                }
            }
            orderService.editOrder(vm.order.id, vm.orderItems)
                .then(function () {
                    vm.orderProducts = [];
                    vm.orderItems = {
                        "ProductIdRemoved": [],
                        "ProductIdAdded": [],
                        "QuantityAdded": []
                    };
                    getOrder();
                })
                .catch(function (response) {
                    toastr.error(response.data.error);
                });
        };

        function getOrder() {
            orderService.getOrder(vm.orderId).then(function (result) {
                vm.order = result.data;
            });
        }

        function getOrderStatus() {
            orderService.getOrderStatus().then(function (result) {
                vm.orderStatus = result.data;
            });
        }

        function getOrderHistory() {
            orderService.getOrderHistory(vm.orderId).then(function (result) {
                vm.orderHistories = result.data;
            });
        }

        vm.delRow = function (item) {
            vm.orderItems.ProductIdRemoved.push(item.id);
            $('.orderItemList tbody tr[attr-id="' + item.id + '"]').hide();
        };

        function init() {
            getOrderStatus();
            getOrder();
            getOrderHistory();

            orderService.getMyRole().then(function (result) {
                vm.myRole = result.data;
            });
        }

        init();
    }
})();