﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using BobCommerce.Infrastructure.Modules;
using BobCommerce.Module.Vendors.Services;

namespace BobCommerce.Module.Vendors
{
    public class ModuleInitializer : IModuleInitializer {
        public void ConfigureServices(IServiceCollection serviceCollection) {
            serviceCollection.AddTransient<IVendorService, VendorService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {

        }
    }
}
