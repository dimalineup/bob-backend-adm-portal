﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BobCommerce.Infrastructure.Models;
using BobCommerce.Module.Core.Models;

namespace BobCommerce.Module.Vendors.Models
{
    public class Table : EntityBase {
        public Table() { }

        public Table(long id) {
            Id = id;
        }

        [Required(ErrorMessage = "The {0} field is required.")]
        [StringLength(10)]
        public string TableNumber { get; set; }

        public string TableShape { get; set; }

        public string TableColor { get; set; }

        public long VendorId { get; set; }

        public string UniqueId { get; set; }

        public string QRCode { get; set; }

        public Vendor vendor { get; set; }
    }
}
