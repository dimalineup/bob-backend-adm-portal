﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BobCommerce.Infrastructure.Models;
using BobCommerce.Module.Core.Models;

namespace BobCommerce.Module.Vendors.Models
{
    public class TableMgmt : EntityBase {
        public TableMgmt() { }

        public TableMgmt(long id) {
            Id = id;
        }

        public long UserId { get; set; }

        public string AssignedTables { get; set; }

        public long VendorId { get; set; }

        public User user { get; set; }

        public Vendor vendor { get; set; }

    }
}
