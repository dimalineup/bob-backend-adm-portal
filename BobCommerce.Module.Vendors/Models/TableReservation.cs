﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BobCommerce.Infrastructure.Models;
using BobCommerce.Module.Core.Models;

namespace BobCommerce.Module.Vendors.Models
{
    public class TableReservation : EntityBase {
        public TableReservation() { }

        public TableReservation(long id) {
            Id = id;
        }

        public string TableNumber { get; set; }

        public long VendorId { get; set; }

        public long UserId { get; set; }
    }
}
