﻿using System.Collections.Generic;

namespace BobCommerce.Module.Vendors.Areas.Vendors.ViewModels {
    public class TableMgmtForm {
        public long Id { get; set; }

        public long UserId { get; set; }

        //public string AssignedTables { get; set; }

        public long VendorId { get; set; }

        public IList<long> AssignedTables { get; set; } = new List<long>();
    }
}
