﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BobCommerce.Module.Vendors.Areas.Vendors.ViewModels {
    public class VendorMobile {
        public VendorMobile() {
        }

        public long i { get; set; }

        public string n { get; set; }

        public string d { get; set; }

        public string a { get; set; }

        public string w { get; set; }

        public string t { get; set; }

        public string ra { get; set; }

        public string re { get; set; }

        public string p { get; set; }

        public string s { get; set; }

        public int uc { get; set; }

        public string la { get; set; }

        public string lo { get; set; }

        public string lt { get; set; }

        public string dst { get; set; }

        public List<CheckedInUser> u { get; set; } = new List<CheckedInUser>();

    }
}
