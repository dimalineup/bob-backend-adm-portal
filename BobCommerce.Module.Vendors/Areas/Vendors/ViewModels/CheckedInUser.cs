﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BobCommerce.Module.Vendors.Areas.Vendors.ViewModels {
    public class CheckedInUser {
        public CheckedInUser() {
        }

        public string fn { get; set; }

        public string url { get; set; }
    }
}
