﻿namespace BobCommerce.Module.Vendors.Areas.Vendors.ViewModels {
    public class TableForm {
        public long Id { get; set; }

        public string TableNumber { get; set; }

        public string TableShape { get; set; }

        public string TableColor { get; set; }

        public long VendorId { get; set; }

        public string VendorName { get; set; }

        public string UniqueId { get; set; }

        public string QRCode { get; set; }
    }
}
