﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BobCommerce.Module.Vendors.Areas.Vendors.ViewModels {
    public class VendorVm {
        public VendorVm() {
            IsActive = true;
        }

        public long Id { get; set; }

        [Required(ErrorMessage = "The {0} field is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The {0} field is required.")]
        public string Slug { get; set; }

        [EmailAddress]
        [Required(ErrorMessage = "The {0} field is required.")]
        public string Email { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public IList<VendorManager> Managers { get; set; } = new List<VendorManager>();

        public string Address { get; set; }

        public string WorkingHours { get; set; }

        public string ThumbnailImageUrl { get; set; }

        public string RatingStars { get; set; }

        public string Reviewers { get; set; }

        public string PhoneNumber { get; set; }

        public string SiteUrl { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string LocationType { get; set; }

        public string MapsGoogleKey { get; set; }

        [Required(ErrorMessage = "The {0} field is required.")]
        [Range(10, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public decimal Discount { get; set; }

        public string NameHE { get; set; }
        public string DescriptionHE { get; set; }
        public string AddressHE { get; set; }
    }
}
