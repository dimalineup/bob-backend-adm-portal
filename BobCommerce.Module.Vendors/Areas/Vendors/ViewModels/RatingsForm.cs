﻿namespace BobCommerce.Module.Vendors.Areas.Vendors.ViewModels {
    public class RatingsForm {
        public long Id { get; set; }

        public long VendorId { get; set; }

        public long UserId { get; set; }

        public int RatingStars { get; set; }

        public string Review { get; set; }
    }
}
