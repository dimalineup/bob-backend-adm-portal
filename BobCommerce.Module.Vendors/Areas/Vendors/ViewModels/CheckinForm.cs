﻿namespace BobCommerce.Module.Vendors.Areas.Vendors.ViewModels {
    public class CheckinForm {
        public long Id { get; set; }

        public long UserId { get; set; }

        //public long VendorId { get; set; }

        public string UniqueID { get; set; }

    }
}
