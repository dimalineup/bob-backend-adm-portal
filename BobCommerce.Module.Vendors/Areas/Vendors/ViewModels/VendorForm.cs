﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BobCommerce.Module.Vendors.Areas.Vendors.ViewModels
{
    public class VendorForm {
        public VendorVm Vendor { get; set; }

        public IFormFile ThumbnailImage { get; set; }
    }
}
