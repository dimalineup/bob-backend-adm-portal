﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Module.Core.Models;
using BobCommerce.Module.Vendors.Services;
using BobCommerce.Module.Core.Services;
using BobCommerce.Module.Vendors.Models;
using Microsoft.Extensions.Configuration;
using BobCommerce.Module.Vendors.Areas.Vendors.ViewModels;
using Net.ConnectCode.BarcodeFontsStandard2D;

namespace BobCommerce.Module.Vendors.Areas.Vendors.Controllers
{
    [Area("Tables")]
    //[Authorize(Roles = "admin")]
    [Route("tables")]
    public class TableMobileController : Controller {
        private readonly IRepository<Table> _tableRepository;
        private readonly IRepository<TableMgmt> _tableMgmtRepository;
        private readonly IRepository<TableReservation> _tableReservationRepository;
        private readonly IRepository<User> _userRepository;

        public TableMobileController(IRepository<Table> tableRepository, IRepository<TableMgmt> tableMgmtRepository, IRepository<TableReservation> tableReservationRepository, IRepository<User> userRepository, IConfiguration config) {
            _tableRepository = tableRepository;
            _tableMgmtRepository = tableMgmtRepository;
            _tableReservationRepository = tableReservationRepository;
            _userRepository = userRepository;
        }

        [HttpPost("reserv")]
        public async Task<IActionResult> PostReserv([FromBody] TableReservationForm model) {
            try {

                //check if we have a table with this code
                var table = await _tableRepository.Query().FirstOrDefaultAsync(x => x.UniqueId == model.UniqueId);
                if (table != null) {

                    if (ModelState.IsValid) {

                        //check if the table with this code is free or already reserved by someone else
                        var tableRes = await _tableReservationRepository.Query().FirstOrDefaultAsync(x => x.VendorId == table.VendorId && x.TableNumber == table.TableNumber);
                        if (tableRes == null) {

                            //reserv the table for the user
                            var newRes = new TableReservation {
                                TableNumber = table.TableNumber,
                                VendorId = table.VendorId,
                                UserId = model.UserId
                            };

                            _tableReservationRepository.Add(newRes);
                            await _tableReservationRepository.SaveChangesAsync();

                            //return Ok(newRes.Id);
                            return Ok(true);
                        }
                        else {
                            //return Ok("Table is already reserved.");
                            return Ok(false);
                        }
                    }
                    //return Ok("Data provided is invalid.");
                    return Ok(false);
                }
                //return Ok("");
                return Ok(false);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [HttpPut("move")]
        public async Task<IActionResult> Put(string uniqueId, [FromBody] TableReservationForm model) {
            try {

                var tableNew = await _tableRepository.Query().FirstOrDefaultAsync(x => x.UniqueId == model.UniqueId);
                if (tableNew != null) {
                    if (ModelState.IsValid) {
                        //var table = await _tableReservationRepository.Query().FirstOrDefaultAsync(x => x.UserId == model.UserId && x.VendorId == model.VendorId && x.TableNumber == model.TableNumber);
                        string[] uniqueID = model.UniqueId.Split("#");
                        int vendorID = int.Parse(uniqueID[0]);
                        string tableNo = uniqueID[1];

                        //var table = await _tableReservationRepository.Query().FirstOrDefaultAsync(x => x.UserId == model.UserId && x.VendorId == vendorID && x.TableNumber == tableNo);
                        var table = await _tableReservationRepository.Query().FirstOrDefaultAsync(x => x.UserId == model.UserId && x.VendorId == vendorID);
                        if (table != null) {

                            _tableReservationRepository.Remove(table);
                            
                            //move the user to the new table
                            var newRes = new TableReservation {
                                TableNumber = tableNew.TableNumber,
                                VendorId = vendorID,
                                UserId = model.UserId
                            };

                            _tableReservationRepository.Add(newRes);
                            await _tableReservationRepository.SaveChangesAsync();

                            return Ok(newRes.Id);
                        }
                        else {
                            return Ok("Data provided is invalid.");
                        }
                    }
                    return Ok("Data provided is invalid.");
                }
                return Ok("Could not reserve new table.");
            }
            catch (Exception ex) {
                throw ex;
            }
        }
    }
}
