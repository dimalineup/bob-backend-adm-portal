﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Module.Core.Models;
using BobCommerce.Module.Vendors.Services;
using BobCommerce.Module.Core.Services;
using BobCommerce.Module.Vendors.Models;
using Microsoft.Extensions.Configuration;
using BobCommerce.Module.Vendors.Areas.Vendors.ViewModels;
using Net.ConnectCode.BarcodeFontsStandard2D;
using Microsoft.AspNetCore.Http;
using BobCommerce.Module.Core.Extensions;

namespace BobCommerce.Module.Vendors.Areas.Vendors.Controllers
{
    [Area("Tables")]
    //[Authorize(Roles = "admin")]
    [Route("api/tables")]
    public class TableApiController : Controller {
        private const string ImpersonationCookieName = "ImpersonateVendor";
        private readonly IRepository<Table> _tableRepository;
        private readonly IRepository<TableMgmt> _tableMgmtRepository;
        private readonly IRepository<TableReservation> _tableReservationRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IWorkContext _workContext;
        private HttpContext _httpContext;

        public TableApiController(IRepository<Table> tableRepository, IRepository<TableMgmt> tableMgmtRepository, IRepository<TableReservation> tableReservationRepository, 
            IRepository<User> userRepository, IConfiguration config, IHttpContextAccessor contextAccessor, IWorkContext workContext) {
            _tableRepository = tableRepository;
            _tableMgmtRepository = tableMgmtRepository;
            _tableReservationRepository = tableReservationRepository;
            _userRepository = userRepository;
            _httpContext = contextAccessor.HttpContext;
            _workContext = workContext;
        }

        [HttpGet]
        public async Task<IActionResult> Get() {
            try {
                long? tempVendorId = -1;
                var currentUser = await _workContext.GetCurrentUser();
                if (!User.IsInRole("admin")) {
                    tempVendorId = currentUser.VendorId;
                }

                var vendorID = GetImpersonationFromCookies();
                var tables = await _tableRepository
                    .Query().Include(x => x.vendor)
                    .Select(x => new { Id = x.Id, TableNumber = x.TableNumber, TableShape = x.TableShape, TableColor = x.TableColor, VendorName = x.vendor.Name, VendorId = x.vendor.Id, QRCode = x.UniqueId })
                    .ToListAsync();
                if (!string.IsNullOrEmpty(vendorID)) {
                    tables = tables.Where(x => x.VendorId == int.Parse(vendorID)).ToList();
                }
                if (!User.IsInRole("admin")) {
                    tables = tables.Where(x => x.VendorId == tempVendorId).ToList();
                }
                return Json(tables);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        private string GetImpersonationFromCookies() {
            if (_httpContext.Request.Cookies.ContainsKey(ImpersonationCookieName)) {
                return _httpContext.Request.Cookies[ImpersonationCookieName];
            }
            return null;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id) {
            var table = await _tableRepository.Query().Include(x => x.vendor).FirstOrDefaultAsync(x => x.Id == id);
            if (table == null) {
                return NotFound();
            }

            var model = new TableForm {
                Id = table.Id,
                TableNumber = table.TableNumber,
                TableShape = table.TableShape,
                TableColor = table.TableColor,
                VendorId = table.VendorId,
                UniqueId = table.UniqueId,
                QRCode = table.QRCode,
                VendorName = table.vendor.Name
            };

            return Json(model);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] TableForm model) {
            try {
                if (ModelState.IsValid) {
                    var table = await _tableRepository.Query().FirstOrDefaultAsync(x => x.Id == id);
                    if (table == null) {
                        return NotFound();
                    }

                    table.TableShape = model.TableShape;
                    table.TableColor = model.TableColor;
                    await _tableRepository.SaveChangesAsync();
                    return Accepted();
                }

                return BadRequest(ModelState);
            }
            catch(Exception ex) {
                throw ex;
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] TableForm model) {
            try {

                long? vendorId = -1;
                var currentUser = await _workContext.GetCurrentUser();
                if (!User.IsInRole("admin")) {
                    vendorId = currentUser.VendorId;
                }
                else {
                    vendorId = model.VendorId;
                }

                if (ModelState.IsValid) {

                    //int uniqueId = GenerateRandomNo();
                    //string uniqueScan = model.VendorId + "#" + model.TableNumber;
                    string uniqueScan = RandomString(4);
                    QR qrCode = new QR(uniqueScan, "M", 8);
                    string output = qrCode.Encode();

                    var table = new Table {
                        TableNumber = model.TableNumber,
                        TableShape = model.TableShape,
                        TableColor = model.TableColor,
                        VendorId = vendorId.Value,
                        UniqueId = uniqueScan,
                        QRCode = output
                    };

                    _tableRepository.Add(table);
                    await _tableRepository.SaveChangesAsync();

                    return CreatedAtAction(nameof(Get), new { id = table.Id }, null);
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id) {
            var table = await _tableRepository.Query().FirstOrDefaultAsync(x => x.Id == id);
            if (table == null) {
                return NotFound();
            }

            try {
                _tableRepository.Remove(table);
                await _tableRepository.SaveChangesAsync();
            }
            catch (DbUpdateException) {
                return BadRequest(new { Error = $"The table {table.TableNumber} can't not be deleted because it is referenced by other tables" });
            }

            return NoContent();
        }

        [HttpGet("mgmt")]
        public async Task<IActionResult> GetMgmt() {
            try {
                var tablesMgmt = await _tableMgmtRepository
                    .Query().Include(x => x.user).Include(x => x.vendor)
                    //.Select(x => new { Id = x.Id, UserName = x.user.FullName, AssignedTables = x.AssignedTables, VendorName = x.vendor.Name })
                    .ToListAsync();
                var currentUser = await _workContext.GetCurrentUser();
                if (!User.IsInRole("admin")) {
                    tablesMgmt = tablesMgmt.Where(x => x.VendorId == currentUser.VendorId).ToList();
                }
                var tblMgmt = tablesMgmt.Select(x => new { Id = x.Id, UserName = x.user.FullName, AssignedTables = x.AssignedTables, VendorName = x.vendor.Name }).ToList();
                return Json(tblMgmt);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [HttpGet("mgmt/{id}")]
        public async Task<IActionResult> GetMgmt(long id) {
            var tableMgmt = await _tableMgmtRepository.Query().FirstOrDefaultAsync(x => x.Id == id);
            if (tableMgmt == null) {
                return NotFound();
            }

            var model = new TableMgmtForm {
                Id = tableMgmt.Id,
                UserId = tableMgmt.UserId,
                //AssignedTables = tableMgmt.AssignedTables,
                VendorId = tableMgmt.VendorId
            };

            return Json(model);
        }

        [HttpPut("mgmt/{id}")]
        public async Task<IActionResult> PutMgmt(long id, [FromBody] TableMgmtForm model) {
            if (ModelState.IsValid) {
                var tableMgmt = await _tableMgmtRepository.Query().FirstOrDefaultAsync(x => x.Id == id);
                if (tableMgmt == null) {
                    return NotFound();
                }

                tableMgmt.UserId = model.UserId;
                //tableMgmt.AssignedTables = model.AssignedTables;
                tableMgmt.VendorId = model.VendorId;
                await _tableRepository.SaveChangesAsync();
                return Accepted();
            }

            return BadRequest(ModelState);
        }

        [HttpPost("mgmt")]
        public async Task<IActionResult> PostMgmt([FromBody] TableMgmtForm model) {
            try {
                if (ModelState.IsValid) {

                    string tables = string.Empty;
                    foreach(var tblAssigned in model.AssignedTables) {
                        tables += tblAssigned + ",";
                    }
                    if (!string.IsNullOrEmpty(tables)) {
                        tables = tables.TrimEnd(',');
                    }

                    var tableMgmt = new TableMgmt {
                        UserId = model.UserId,
                        AssignedTables = tables,
                        VendorId = model.VendorId,
                    };

                    _tableMgmtRepository.Add(tableMgmt);
                    await _tableMgmtRepository.SaveChangesAsync();

                    return CreatedAtAction(nameof(GetMgmt), new { id = tableMgmt.Id }, null);
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [HttpDelete("mgmt/{id}")]
        public async Task<IActionResult> DeleteMgmt(long id) {
            var tableMgmt = await _tableMgmtRepository.Query().FirstOrDefaultAsync(x => x.Id == id);
            if (tableMgmt == null) {
                return NotFound();
            }

            try {
                _tableMgmtRepository.Remove(tableMgmt);
                await _tableMgmtRepository.SaveChangesAsync();
            }
            catch (DbUpdateException) {
                return BadRequest(new { Error = $"The table {tableMgmt.Id} can't not be deleted because it is referenced by other tables" });
            }

            return NoContent();
        }

        [HttpGet("users/{vendorId}")]
        public async Task<IActionResult> GetUsers(long vendorId) {
            var users = await _userRepository.Query().Include(x => x.Roles).Where(x => x.VendorId == vendorId && x.Roles.Any(y => y.Role.Name.Contains("waiter"))).Select(x => new {
                Id = x.Id,
                Name = x.FullName
            }).ToListAsync();

            return Json(users);
        }

        [HttpGet("all-users")]
        public async Task<IActionResult> GetAllUsers() {
            var users = await _userRepository.Query().Include(x => x.Roles).Where(x => x.Roles.Any(y => y.Role.Name.Contains("waiter"))).Select(x => new {
                Id = x.Id,
                Name = x.UserName
            }).ToListAsync();

            return Json(users);
        }

        [HttpGet("vendor/{vendorId}")]
        public async Task<IActionResult> GetVendorTables(long vendorId) {
            var vendorTables = await _tableRepository.Query().Where(x => x.VendorId == vendorId).Select(x => new {
                Id = x.Id,
                TableNumber = x.TableNumber
            }).ToListAsync();

            return Json(vendorTables);
        }

        protected int GenerateRandomNo() {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }

        public  string RandomString(int length) {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        [HttpGet("reserv/{userId}")]
        public async Task<IActionResult> GetReserv(long userId) {
            try {
                var tableNo = "";
                var tableRes = await _tableReservationRepository.Query().Where(x => x.UserId == userId).FirstOrDefaultAsync();
                if(tableRes != null) {
                    tableNo = tableRes.TableNumber;
                }
                return Json(tableNo);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [HttpGet("mgmt/myvendor")]
        public async Task<IActionResult> GetMyVendor() {
            try {
                long? myVendorId = -1;
                var currentUser = await _workContext.GetCurrentUser();
                if (!User.IsInRole("admin")) {
                    myVendorId = currentUser.VendorId;
                }
                return Ok(myVendorId);
            }
            catch (Exception ex) {
                throw ex;
            }
        }
    }
}
