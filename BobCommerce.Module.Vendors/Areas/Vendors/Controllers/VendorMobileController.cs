﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Infrastructure.Web.SmartTable;
using BobCommerce.Module.Core.Models;
using BobCommerce.Module.Vendors.Areas.Vendors.ViewModels;
using BobCommerce.Module.Vendors.Services;
using BobCommerce.Module.Core.Services;
using System.Collections.Generic;
using BobCommerce.Module.Core.Extensions;
using Microsoft.Extensions.Configuration;

namespace BobCommerce.Module.Vendors.Areas.Vendors.Controllers
{
    [Area("Vendors")]
    //[Authorize(Roles = "admin")]
    [Route("vendors")]
    public class VendorMobileController : Controller {
        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IVendorService _vendorService;
        private readonly IMediaService _mediaService;
        private readonly IWorkContext _workContext;
        private readonly IConfiguration _configuration;
        private readonly IRepository<User> _userRepository;

        public VendorMobileController(IRepository<Vendor> vendorRepository, IVendorService vendorService, IMediaService mediaService, IWorkContext workContext, IConfiguration config, IRepository<User> userRepository) {
            _vendorRepository = vendorRepository;
            _vendorService = vendorService;
            _mediaService = mediaService;
            _workContext = workContext;
            _configuration = config;
            _userRepository = userRepository;
        }

        [HttpGet("search/{name}")]
        public async Task<IActionResult> Get(string name, string latitude, string longitude) {
            if(name == "*") {
                var vendors = await _vendorRepository.Query().Include(x => x.ThumbnailImage).Include(x => x.Rating).Where(x => !x.IsDeleted).Select(x => new VendorMobile {
                    i = x.Id,
                    n = x.Name,
                    d = x.Description,
                    a = x.Address,
                    w = x.WorkingHours,
                    p = x.PhoneNumber,
                    s = x.SiteUrl,
                    t = _mediaService.GetThumbnailUrl(x.ThumbnailImage),
                    la = x.Latitude,
                    lo = x.Longitude,
                    lt = x.LocationType
                }).ToListAsync();
                return Json(vendors);
            }
            else {
                var vendors = await _vendorRepository.Query().Where(x => !x.IsDeleted && !string.IsNullOrEmpty(name) && x.Name.Contains(name)).Include(x => x.ThumbnailImage).Select(x => new VendorMobile {
                    i = x.Id,
                    n = x.Name,
                    d = x.Description,
                    a = x.Address,
                    w = x.WorkingHours,
                    p = x.PhoneNumber,
                    s = x.SiteUrl,
                    t = _mediaService.GetThumbnailUrl(x.ThumbnailImage),
                    la = x.Latitude,
                    lo = x.Longitude,
                    lt = x.LocationType
                }).ToListAsync();

                return Json(vendors);
            }
        }

        [HttpGet("searchNearby/{name}/{latitude}/{longitude}/{lang}")]
        public IActionResult GetNearby(string name, string latitude, string longitude, string lang) {
            //var user = _userRepository.Query().Where(x => x.Id == userId).FirstOrDefault();
            var isHeb = lang.ToLower() == "he-il" ? true : false;

            double myLat = double.Parse(latitude);
            double myLon = double.Parse(longitude);

            if (name == "*") {
                var vendors = _vendorRepository.Query().Include(x => x.ThumbnailImage).Include(x => x.Rating).Where(x => !x.IsDeleted)
                   .AsEnumerable()
                   .Select(l => new { l, Dist = distanceInKilometers(myLon, myLat, double.Parse(l.Longitude), double.Parse(l.Latitude)) })
                   .OrderBy(l => l.Dist).ToList();
                var ret = vendors.Select(x => new VendorMobile {
                    i = x.l.Id,
                    n = (isHeb ? x.l.NameHE : x.l.Name),
                    d = (isHeb ? x.l.DescriptionHE : x.l.Description),
                    a = (isHeb ? x.l.AddressHE : x.l.Address),
                    w = x.l.WorkingHours,
                    ra = mediaStars(x.l.Rating).ToString("N2"),
                    re = x.l.Rating.Count.ToString(),
                    p = x.l.PhoneNumber,
                    s = x.l.SiteUrl,
                    t = _mediaService.GetThumbnailUrl(x.l.ThumbnailImage),
                    la = x.l.Latitude,
                    lo = x.l.Longitude,
                    lt = x.l.LocationType,
                    dst = x.Dist.ToString("N2")
                }).ToList();

                return Json(ret);
            }
            else {
                var vendors = _vendorRepository.Query().Include(x => x.ThumbnailImage).Include(x => x.Rating).Where(x => !x.IsDeleted && !string.IsNullOrEmpty(name) && x.Name.Contains(name))
                   .AsEnumerable()
                   .Select(l => new { l, Dist = distanceInKilometers(myLon, myLat, double.Parse(l.Longitude), double.Parse(l.Latitude)) })
                   .OrderBy(l => l.Dist).ToList();

                var ret = vendors.Select(x => new VendorMobile {
                    i = x.l.Id,
                    n = (isHeb ? x.l.NameHE : x.l.Name),
                    d = (isHeb ? x.l.DescriptionHE : x.l.Description),
                    a = (isHeb ? x.l.AddressHE : x.l.Address),
                    w = x.l.WorkingHours,
                    ra = mediaStars(x.l.Rating).ToString("N2"),
                    re = x.l.Rating.Count.ToString(),
                    p = x.l.PhoneNumber,
                    s = x.l.SiteUrl,
                    t = _mediaService.GetThumbnailUrl(x.l.ThumbnailImage),
                    la = x.l.Latitude,
                    lo = x.l.Longitude,
                    lt = x.l.LocationType,
                    dst = x.Dist.ToString("N2")
                }).ToList();

                return Json(ret);
            }
        }

        public float mediaStars(IList<Rating> ratings) {
            float media = 0;
            int sum = 0;
            if(ratings != null && ratings.Count > 0) {
                foreach(Rating item in ratings) {
                    sum += item.RatingStars;
                }
                media = sum / ratings.Count;
            }
            return media;
        }

        public double ToRadians(double degrees) => degrees * Math.PI / 180.0;
        public double distanceInMiles(double lon1d, double lat1d, double lon2d, double lat2d) {
            var lon1 = ToRadians(lon1d);
            var lat1 = ToRadians(lat1d);
            var lon2 = ToRadians(lon2d);
            var lat2 = ToRadians(lat2d);

            var deltaLon = lon2 - lon1;
            var c = Math.Acos(Math.Sin(lat1) * Math.Sin(lat2) + Math.Cos(lat1) * Math.Cos(lat2) * Math.Cos(deltaLon));
            var earthRadius = 3958.76;
            var distInMiles = earthRadius * c;

            return distInMiles;
        }

        public double distanceInKilometers(double lon1d, double lat1d, double lon2d, double lat2d) {
            var lon1 = ToRadians(lon1d);
            var lat1 = ToRadians(lat1d);
            var lon2 = ToRadians(lon2d);
            var lat2 = ToRadians(lat2d);

            var deltaLon = lon2 - lon1;
            var c = Math.Acos(Math.Sin(lat1) * Math.Sin(lat2) + Math.Cos(lat1) * Math.Cos(lat2) * Math.Cos(deltaLon));
            var earthRadius = 6371.01;
            var distInKm = earthRadius * c;

            return distInKm;
        }

        [HttpGet("{id}/{lang}")]
        public async Task<IActionResult> Get(long id, string lang) {
            //var user = _userRepository.Query().Where(x => x.Id == userId).FirstOrDefault();
            var isHeb = lang.ToLower() == "he-il" ? true : false;

            var vendor = await _vendorRepository.Query()
                .Include(x => x.ThumbnailImage)
                .Include(x => x.Rating)
                .Include(x => x.CheckedIn).ThenInclude(xx => xx.user)
                .Include(x => x.Users).FirstOrDefaultAsync(x => x.Id == id);
            if (vendor == null) {
                return NotFound();
            }

            List<CheckedInUser> users = new List<CheckedInUser>();
            if (vendor.CheckedIn.Count > 0) {
                for(var i = 0; i < vendor.CheckedIn.Count; i++) {
                    if (vendor.CheckedIn[i].IsVisible.HasValue && vendor.CheckedIn[i].IsVisible.Value == true) {
                        User currentUser = vendor.CheckedIn[i].user;
                        CheckedInUser usr = new CheckedInUser();
                        usr.fn = currentUser.FullName;
                        switch (currentUser.AccountType) {
                            case (int)UserAccountType.Regular:
                                usr.url = _configuration["MySettings:AbsoluteUrl"] + _mediaService.GetThumbnailUrl(currentUser.UserImage);
                                break;
                            case (int)UserAccountType.Facebook:
                                usr.url = currentUser.FacebookImageUrl;
                                break;
                            case (int)UserAccountType.Google:
                                break;
                            default:
                                usr.url = _configuration["MySettings:AbsoluteUrl"] + _mediaService.GetThumbnailUrl(currentUser.UserImage);
                                break;

                        }
                        users.Add(usr);
                    }
                }
            }

            var model = new VendorMobile {
                i = vendor.Id,
                n = (isHeb ? vendor.NameHE : vendor.Name),
                d = (isHeb ? vendor.DescriptionHE : vendor.Description),
                a = (isHeb ? vendor.AddressHE : vendor.Address),
                w = vendor.WorkingHours,
                ra = mediaStars(vendor.Rating).ToString("N2"),
                re = vendor.Rating.Count.ToString(),
                p = vendor.PhoneNumber,
                s = vendor.SiteUrl,
                t = _mediaService.GetThumbnailUrl(vendor.ThumbnailImage),
                la = vendor.Latitude,
                lo = vendor.Longitude,
                lt = vendor.LocationType,
                uc = users.Count,
                u = users
            };

            return Json(model);
        }

        [HttpGet("{id}/{lang}/{latitude}/{longitude}")]
        public async Task<IActionResult> Get(long id, string lang, string latitude, string longitude) {
            //var user = _userRepository.Query().Where(x => x.Id == userId).FirstOrDefault();
            var isHeb = lang.ToLower() == "he-il" ? true : false;

            double myLat = double.Parse(latitude);
            double myLon = double.Parse(longitude);

            var vendor = await _vendorRepository.Query()
                .Include(x => x.ThumbnailImage)
                .Include(x => x.Rating)
                .Include(x => x.CheckedIn).ThenInclude(xx => xx.user)
                .Include(x => x.Users).Where(x => x.Id == id)
                .Select(l => new { l, Dist = distanceInKilometers(myLon, myLat, double.Parse(l.Longitude), double.Parse(l.Latitude)) })
                .FirstOrDefaultAsync();
            if (vendor == null) {
                return NotFound();
            }

            List<CheckedInUser> users = new List<CheckedInUser>();
            if (vendor.l.CheckedIn.Count > 0) {
                for (var i = 0; i < vendor.l.CheckedIn.Count; i++) {
                    if (vendor.l.CheckedIn[i].IsVisible.HasValue && vendor.l.CheckedIn[i].IsVisible.Value == true) {
                        User currentUser = vendor.l.CheckedIn[i].user;
                        CheckedInUser usr = new CheckedInUser();
                        usr.fn = currentUser.FullName;
                        switch (currentUser.AccountType) {
                            case (int)UserAccountType.Regular:
                                usr.url = _configuration["MySettings:AbsoluteUrl"] + _mediaService.GetThumbnailUrl(currentUser.UserImage);
                                break;
                            case (int)UserAccountType.Facebook:
                                usr.url = currentUser.FacebookImageUrl;
                                break;
                            case (int)UserAccountType.Google:
                                break;
                            default:
                                usr.url = _configuration["MySettings:AbsoluteUrl"] + _mediaService.GetThumbnailUrl(currentUser.UserImage);
                                break;

                        }
                        users.Add(usr);
                    }
                }
            }

            var model = new VendorMobile {
                i = vendor.l.Id,
                n = (isHeb ? vendor.l.NameHE : vendor.l.Name),
                d = (isHeb ? vendor.l.DescriptionHE : vendor.l.Description),
                a = (isHeb ? vendor.l.AddressHE : vendor.l.Address),
                w = vendor.l.WorkingHours,
                ra = mediaStars(vendor.l.Rating).ToString("N2"),
                re = vendor.l.Rating.Count.ToString(),
                p = vendor.l.PhoneNumber,
                s = vendor.l.SiteUrl,
                t = _mediaService.GetThumbnailUrl(vendor.l.ThumbnailImage),
                la = vendor.l.Latitude,
                lo = vendor.l.Longitude,
                lt = vendor.l.LocationType,
                uc = users.Count,
                u = users,
                dst = vendor.Dist.ToString("N2")
            };

            return Json(model);
        }

        [HttpGet("vendorimage")]
        public async Task<IActionResult> VendorImage() {
            var img = string.Empty;
            try {

                var currentUser = await _workContext.GetCurrentUser();
                if (currentUser != null && currentUser.VendorId.HasValue) {
                    var vendor = _vendorRepository.Query().Include(x => x.ThumbnailImage).FirstOrDefault(x => x.Id == long.Parse(currentUser.VendorId.Value.ToString()));
                    if (vendor != null) {
                        img = _mediaService.GetThumbnailUrl(vendor.ThumbnailImage);
                    }
                }
            }
            catch (Exception ex) {
                throw ex;
            }
            return Json(img);
        }
    }
}
