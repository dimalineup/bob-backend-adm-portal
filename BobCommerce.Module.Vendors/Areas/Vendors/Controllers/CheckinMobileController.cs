﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Infrastructure.Web.SmartTable;
using BobCommerce.Module.Core.Models;
using BobCommerce.Module.Vendors.Areas.Vendors.ViewModels;
using BobCommerce.Module.Vendors.Services;
using Microsoft.AspNetCore.Http;
using BobCommerce.Module.Core.Services;
using Microsoft.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Identity;
using BobCommerce.Module.Vendors.Models;

namespace BobCommerce.Module.Vendors.Areas.Vendors.Controllers
{
    [Area("Vendors")]
    //[Authorize(Roles = "admin")]
    [Route("checkin")]
    public class CheckinMobileController : Controller {
        private readonly IRepository<Checkin> _checkinRepository;
        private readonly IRepository<Table> _tableRepository;
        private readonly IRepository<TableReservation> _tableReservationRepository;
        private readonly IRepository<Vendor> _vendorRepository;

        public CheckinMobileController(IRepository<Checkin> checkinRepository, IRepository<Table> tableRepository, IRepository<TableReservation> tableReservationRepository,
            IRepository<Vendor> vendorRepository) {
            _checkinRepository = checkinRepository;
            _tableRepository = tableRepository;
            _tableReservationRepository = tableReservationRepository;
            _vendorRepository = vendorRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Get() {
            try {
                var checkins = await _checkinRepository
                    .Query().Include(x => x.vendor).Include(x => x.user)
                    .ToListAsync();
                return Json(checkins);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CheckinForm model) {
            if (ModelState.IsValid) {

                var table = _tableRepository.Query().Where(x => x.UniqueId == model.UniqueID).FirstOrDefault();

                //string[] uniqueID = model.UniqueID.Split("#");
                //int vendorID = int.Parse(uniqueID[0]);
                var checkin = new Checkin {
                    UserId = model.UserId,
                    VendorId = table.VendorId,
                    CheckinDateTime = DateTime.Now,
                    IsVisible = true
                };

                _checkinRepository.Add(checkin);

                //checkout the user from all the other locations
                var oldCheckin = await _checkinRepository.Query().FirstOrDefaultAsync(x => x.UserId == model.UserId);
                if(oldCheckin != null) {
                    _checkinRepository.Remove(oldCheckin);
                }

                await _checkinRepository.SaveChangesAsync();

                //reserv a table for that user
                var tableRes = new TableReservation {
                    TableNumber = table.TableNumber,
                    UserId = model.UserId,
                    VendorId = table.VendorId
                };

                _tableReservationRepository.Add(tableRes);
                await _tableReservationRepository.SaveChangesAsync();

                return Ok(table.VendorId);
            }
            return Ok(-1);
        }

        [HttpPost("{latitude}/{longitude}")]
        public async Task<IActionResult> Post([FromBody] CheckinForm model, string latitude, string longitude) {
            if (ModelState.IsValid) {

                double myLat = double.Parse(latitude);
                double myLon = double.Parse(longitude);

                var table = _tableRepository.Query().Where(x => x.UniqueId == model.UniqueID).FirstOrDefault();

                var vendor = _vendorRepository.Query().Where(x => x.Id == table.VendorId)
                    .Select(l => new { l, Dist = distanceInKilometers(myLon, myLat, double.Parse(l.Longitude), double.Parse(l.Latitude)) })
                    .FirstOrDefault();

                if(vendor.Dist> 0.5) {
                    return Json(new { vendorId = 0, error = "Too far away" });
                }

                var checkin = new Checkin {
                    UserId = model.UserId,
                    VendorId = table.VendorId,
                    CheckinDateTime = DateTime.Now,
                    IsVisible = true
                };

                _checkinRepository.Add(checkin);

                //checkout the user from all the other locations
                var oldCheckin = await _checkinRepository.Query().FirstOrDefaultAsync(x => x.UserId == model.UserId);
                if (oldCheckin != null) {
                    _checkinRepository.Remove(oldCheckin);
                }

                await _checkinRepository.SaveChangesAsync();

                //reserv a table for that user
                var tableRes = new TableReservation {
                    TableNumber = table.TableNumber,
                    UserId = model.UserId,
                    VendorId = table.VendorId
                };

                _tableReservationRepository.Add(tableRes);
                await _tableReservationRepository.SaveChangesAsync();

                //return Ok(table.VendorId);
                return Json(new { vendorId = table.VendorId, vendorName = vendor.l.Name, vendorNameHe = vendor.l.NameHE, error = "All good" });
            }
            return Ok(-1);
        }

        [HttpPost("{userId}/{locationId}")]
        public async Task<IActionResult> ChangeVisibility(long userId, long locationId) {

            var checkin = _checkinRepository.Query().Where(x => x.UserId == userId && x.VendorId == locationId).FirstOrDefault();
            if(checkin != null) {
                var visibility = checkin.IsVisible;
                if(!visibility.HasValue) {
                    visibility = false;
                }
                else {
                    visibility = checkin.IsVisible.Value;
                }
                checkin.IsVisible = !visibility;
                await _checkinRepository.SaveChangesAsync();
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpPost("checkout/{userId}")]
        public async Task<IActionResult> Checkout(long userId) {
            var oldCheckin = await _checkinRepository.Query().FirstOrDefaultAsync(x => x.UserId == userId);
            if (oldCheckin != null) {
                _checkinRepository.Remove(oldCheckin);
                await _checkinRepository.SaveChangesAsync();

                var tableRes = _tableReservationRepository.Query().Where(x => x.UserId == userId).FirstOrDefault();
                _tableReservationRepository.Add(tableRes);

                return Ok(true);
            }
            else {
                return Ok(false);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id) {
            var table = await _checkinRepository.Query().FirstOrDefaultAsync(x => x.UserId == id);
            if (table == null) {
                return NotFound();
            }

            try {
                _checkinRepository.Remove(table);
                await _checkinRepository.SaveChangesAsync();
            }
            catch (DbUpdateException) {
                return BadRequest(new { Error = $"An error occured and checkout was not possible" });
            }

            return NoContent();
        }

        public double distanceInKilometers(double lon1d, double lat1d, double lon2d, double lat2d) {
            var lon1 = ToRadians(lon1d);
            var lat1 = ToRadians(lat1d);
            var lon2 = ToRadians(lon2d);
            var lat2 = ToRadians(lat2d);

            var deltaLon = lon2 - lon1;
            var c = Math.Acos(Math.Sin(lat1) * Math.Sin(lat2) + Math.Cos(lat1) * Math.Cos(lat2) * Math.Cos(deltaLon));
            var earthRadius = 6371.01;
            var distInKm = earthRadius * c;

            return distInKm;
        }

        public double ToRadians(double degrees) => degrees * Math.PI / 180.0;
    }
}
