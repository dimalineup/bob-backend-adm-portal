﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Module.Core.Models;
using BobCommerce.Module.Vendors.Services;
using BobCommerce.Module.Core.Services;
using BobCommerce.Module.Vendors.Models;
using Microsoft.Extensions.Configuration;
using BobCommerce.Module.Vendors.Areas.Vendors.ViewModels;
using Net.ConnectCode.BarcodeFontsStandard2D;

namespace BobCommerce.Module.Vendors.Areas.Vendors.Controllers
{
    [Area("Ratings")]
    //[Authorize(Roles = "admin")]
    [Route("ratings")]
    public class RatingsMobileController : Controller {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IRepository<Rating> _ratingsRepository;

        public RatingsMobileController(IRepository<User> userRepository, IRepository<Vendor> vendorRepository, IRepository<Rating> ratingsRepository) {
            _userRepository = userRepository;
            _vendorRepository = vendorRepository;
            _ratingsRepository = ratingsRepository;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id) {
            var rating = await _ratingsRepository.Query().FirstOrDefaultAsync(x => x.Id == id);
            if (rating == null) {
                return NotFound();
            }

            var model = new RatingsForm {
                Id = rating.Id,
                VendorId = rating.VendorId,
                UserId = rating.UserId,
                RatingStars = rating.RatingStars,
                Review = rating.Review
            };

            return Json(model);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RatingsForm model) {
            try {
                if (ModelState.IsValid) {

                    var rating = new Rating {
                        VendorId = model.VendorId,
                        UserId = model.UserId,
                        RatingStars = model.RatingStars,
                        Review = model.Review,
                        DateInserted = DateTime.Now
                    };

                    _ratingsRepository.Add(rating);
                    await _ratingsRepository.SaveChangesAsync();

                    //return CreatedAtAction(nameof(Get), new { id = rating.Id }, null);
                    return Ok(true);
                }
                return Ok(false);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [HttpGet("allratings/{vendorId}")]
        public async Task<IActionResult> GetRatings(long vendorId) {
            var reviews = await _ratingsRepository.Query().Include(x => x.user).Where(x => x.VendorId == vendorId).OrderByDescending(x => x.DateInserted).Select(x => new RatingMobile {
                UserName = x.user.UserName,
                RatingStars = x.RatingStars,
                Review = x.Review,
                DateInserted = x.DateInserted
                
            }).ToListAsync();
            return Json(reviews);
        }
    }
}
