﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Infrastructure.Web.SmartTable;
using BobCommerce.Module.Core.Models;
using BobCommerce.Module.Vendors.Areas.Vendors.ViewModels;
using BobCommerce.Module.Vendors.Services;
using Microsoft.AspNetCore.Http;
using BobCommerce.Module.Core.Services;
using Microsoft.Net.Http.Headers;
using System.IO;
using Microsoft.AspNetCore.Identity;
using BobCommerce.Module.Core.Extensions;
using Microsoft.Extensions.Configuration;
using System.Globalization;

namespace BobCommerce.Module.Vendors.Areas.Vendors.Controllers
{
    [Area("Vendors")]
    //[Authorize(Roles = "admin, manager, vendor")]
    [Authorize(Roles = "admin, vendor")]
    [Route("api/vendors")]
    public class VendorApiController : Controller {
        private const string ImpersonationCookieName = "ImpersonateVendor";
        private const string ImpersonationImageName = "ImpersonateImage";
        private readonly IRepository<Vendor> _vendorRepository;
        private readonly UserManager<User> _userManager;
        private readonly IVendorService _vendorService;
        private readonly IMediaService _mediaService;
        private HttpContext _httpContext;
        private readonly IWorkContext _workContext;
        private readonly IConfiguration _configuration;

        public VendorApiController(IRepository<Vendor> vendorRepository, UserManager<User> userManager, IVendorService vendorService, IMediaService mediaService, IHttpContextAccessor contextAccessor,
            IWorkContext workContext, IConfiguration configuration) {
            _vendorRepository = vendorRepository;
            _userManager = userManager;
            _vendorService = vendorService;
            _mediaService = mediaService;
            _httpContext = contextAccessor.HttpContext;
            _workContext = workContext;
            _configuration = configuration;
        }

        [HttpPost("grid")]
        public async Task<ActionResult> List([FromBody] SmartTableParam param) {
            var currentUser = await _workContext.GetCurrentUser();
            var culture = currentUser.Culture;
            if (culture == null) {
                culture = "en-US";
            }
            var vendorID = GetImpersonationFromCookies();
            var query = _vendorRepository.Query().Where(x => !x.IsDeleted);
            if (!string.IsNullOrEmpty(vendorID)){
                query = query.Where(x => x.Id == int.Parse(vendorID));
            }

            if (!User.IsInRole("admin")) {
                query = query.Where(x => x.Id == currentUser.VendorId);
            }

            if (param.Search.PredicateObject != null) {
                dynamic search = param.Search.PredicateObject;

                if (search.Email != null) {
                    string email = search.Email;
                    query = query.Where(x => x.Email.Contains(email));
                }

                if (search.CreatedOn != null) {
                    if (search.CreatedOn.before != null) {
                        DateTimeOffset before = search.CreatedOn.before;
                        query = query.Where(x => x.CreatedOn <= before);
                    }

                    if (search.CreatedOn.after != null) {
                        DateTimeOffset after = search.CreatedOn.after;
                        query = query.Where(x => x.CreatedOn >= after);
                    }
                }
            }

            var vendors = query.ToSmartTableResult(
                param,
                x => new {
                    Id = x.Id,
                    Name = x.Name,
                    Email = x.Email,
                    IsActive = x.IsActive,
                    Slug = x.Slug,
                    CreatedOn = x.CreatedOn.ToString("MMM dd, yyyy HH:mm:ss tt", new CultureInfo(culture))
                });

            return Json(vendors);
        }

        [HttpGet]
        public async Task<IActionResult> Get() {
            var vendors = await _vendorRepository.Query().Where(x => x.IsDeleted == false).Select(x => new {
                Id = x.Id,
                Name = x.Name,
                Slug = x.Slug
            }).ToListAsync();

            return Json(vendors);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id) {
            var vendor = await _vendorRepository.Query()
                .Include(x => x.ThumbnailImage)
                .Include(x => x.Users).FirstOrDefaultAsync(x => x.Id == id);
            if (vendor == null) {
                return NotFound();
            }

            var model = new VendorVm {
                Id = vendor.Id,
                Name = vendor.Name,
                NameHE = vendor.NameHE,
                Slug = vendor.Slug,
                Email = vendor.Email,
                Description = vendor.Description,
                DescriptionHE = vendor.DescriptionHE,
                IsActive = vendor.IsActive,
                Managers = vendor.Users.Select(x => new VendorManager { UserId = x.Id, Email = x.Email }).ToList(),
                Address = vendor.Address,
                AddressHE = vendor.AddressHE,
                WorkingHours = vendor.WorkingHours,
                PhoneNumber = vendor.PhoneNumber,
                SiteUrl = vendor.SiteUrl,
                Latitude = vendor.Latitude,
                Longitude = vendor.Longitude,
                LocationType = vendor.LocationType,
                ThumbnailImageUrl = _mediaService.GetThumbnailUrl(vendor.ThumbnailImage),
                MapsGoogleKey = _configuration["MySettings:MapsGoogleKey"],
                Discount = vendor.Discount
            };

            return Json(model);
        }

        [HttpPost]
        public async Task<IActionResult> Post(VendorForm model) {
            try {
                if (ModelState.IsValid) {

                    var vendor = new Vendor {
                        Name = model.Vendor.Name,
                        NameHE = model.Vendor.NameHE,
                        Slug = model.Vendor.Slug,
                        Email = model.Vendor.Email,
                        Description = model.Vendor.Description,
                        DescriptionHE = model.Vendor.DescriptionHE,
                        IsActive = model.Vendor.IsActive,
                        Address = model.Vendor.Address,
                        AddressHE = model.Vendor.AddressHE,
                        WorkingHours = model.Vendor.WorkingHours,
                        PhoneNumber = model.Vendor.PhoneNumber,
                        SiteUrl = model.Vendor.SiteUrl,
                        Latitude = model.Vendor.Latitude,
                        Longitude = model.Vendor.Longitude,
                        LocationType = model.Vendor.LocationType,
                        Discount = model.Vendor.Discount
                    };

                    await SaveVendorMedia(model, vendor);

                    await _vendorService.Create(vendor);

                    //create user with vendor role
                    var user = new User {
                        UserName = model.Vendor.Email,
                        Email = model.Vendor.Email,
                        FullName = model.Vendor.Email,
                        VendorId = vendor.Id
                    };

                    var userRole = new UserRole { RoleId = 4 };

                    user.Roles.Add(userRole);
                    userRole.User = user;

                    var result = await _userManager.CreateAsync(user, "vendor");

                    return CreatedAtAction(nameof(Get), new { id = vendor.Id }, null);
                }
                return BadRequest(ModelState);
            }
            catch(Exception ex) {
                throw ex;
            }
        }

        private string GetImpersonationFromCookies() {
            if (_httpContext.Request.Cookies.ContainsKey(ImpersonationCookieName)) {
                return _httpContext.Request.Cookies[ImpersonationCookieName];
            }
            return null;
        }

        [HttpPost("impersonate/{id}")]
        public ActionResult Impersonate(string id) {
            try {
                _httpContext.Response.Cookies.Append(ImpersonationCookieName, id, new CookieOptions {
                    Expires = DateTime.UtcNow.AddYears(5),
                    HttpOnly = true,
                    IsEssential = true
                });

                var vendor = _vendorRepository.Query().Include(x => x.ThumbnailImage).FirstOrDefault(x => x.Id == int.Parse(id));
                _httpContext.Response.Cookies.Append(ImpersonationImageName, _mediaService.GetThumbnailUrl(vendor.ThumbnailImage), new CookieOptions {
                    Expires = DateTime.UtcNow.AddYears(5),
                    HttpOnly = true,
                    IsEssential = true
                });
            }
            catch(Exception ex) {
                throw ex;
            }
            return Ok("");
        }

        [HttpPost("exitimpersonation")]
        public ActionResult ExitImpersonation() {
            _httpContext.Response.Cookies.Delete(ImpersonationCookieName);
            _httpContext.Response.Cookies.Delete(ImpersonationImageName);
            ViewBag.Impersonation = null;
            ViewBag.Image = null;
            return Ok("");
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, VendorForm model) {
            if (ModelState.IsValid) {

                var vendor = _vendorRepository.Query().FirstOrDefault(x => x.Id == id);
                vendor.Name = model.Vendor.Name;
                vendor.NameHE = model.Vendor.NameHE;
                vendor.Slug = model.Vendor.Slug;
                vendor.Email = model.Vendor.Email;
                vendor.Description = model.Vendor.Description;
                vendor.DescriptionHE = model.Vendor.DescriptionHE;
                vendor.IsActive = model.Vendor.IsActive;
                vendor.LatestUpdatedOn = DateTimeOffset.Now;
                vendor.Address = model.Vendor.Address;
                vendor.AddressHE = model.Vendor.AddressHE;
                vendor.WorkingHours = model.Vendor.WorkingHours;
                vendor.PhoneNumber = model.Vendor.PhoneNumber;
                vendor.SiteUrl = model.Vendor.SiteUrl;
                vendor.Latitude = model.Vendor.Latitude;
                vendor.Longitude = model.Vendor.Longitude;
                vendor.LocationType = model.Vendor.LocationType;
                vendor.Discount = model.Vendor.Discount;

                await SaveVendorMedia(model, vendor);

                await _vendorService.Update(vendor);

                return Accepted();
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id) {
            var vendor = await _vendorRepository.Query().FirstOrDefaultAsync(x => x.Id == id);
            if (vendor == null) {
                return NotFound();
            }

            await _vendorService.Delete(vendor);
            return NoContent();
        }

        private async Task SaveVendorMedia(VendorForm model, Vendor vendor) {
            if (model.ThumbnailImage != null) {
                var fileName = await SaveFile(model.ThumbnailImage);
                if (vendor.ThumbnailImage != null) {
                    vendor.ThumbnailImage.FileName = fileName;
                }
                else {
                    vendor.ThumbnailImage = new Media { FileName = fileName };
                }
            }
        }

        private async Task<string> SaveFile(IFormFile file) {
            var originalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Value.Trim('"');
            var fileName = $"{Guid.NewGuid()}{Path.GetExtension(originalFileName)}";
            await _mediaService.SaveMediaAsync(file.OpenReadStream(), fileName, file.ContentType);
            return fileName;
        }
    }
}
