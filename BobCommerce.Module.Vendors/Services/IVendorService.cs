﻿using System.Threading.Tasks;
using BobCommerce.Module.Core.Models;

namespace BobCommerce.Module.Vendors.Services
{
    public interface IVendorService {
        Task Create(Vendor brand);

        Task Update(Vendor brand);

        Task Delete(long id);

        Task Delete(Vendor brand);
    }
}
