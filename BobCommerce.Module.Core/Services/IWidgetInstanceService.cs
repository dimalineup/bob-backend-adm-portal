﻿using System.Linq;
using BobCommerce.Module.Core.Models;

namespace BobCommerce.Module.Core.Services {
    public interface IWidgetInstanceService {
        IQueryable<WidgetInstance> GetPublished();
    }
}
