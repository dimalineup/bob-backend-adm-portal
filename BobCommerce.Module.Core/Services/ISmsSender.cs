﻿using System.Threading.Tasks;

namespace BobCommerce.Module.Core.Services {
    public interface ISmsSender {
        Task SendSmsAsync(string number, string message);
    }
}
