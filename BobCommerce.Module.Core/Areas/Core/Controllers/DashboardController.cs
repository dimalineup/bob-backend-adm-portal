﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BobCommerce.Module.Core.Areas.Core.Controllers
{
    [Area("Core")]
    //[Authorize(Roles = "admin, vendor, waiter, barman, kitchen, manager")]
    [Authorize(Roles = "admin, vendor, waiter, barman, kitchen")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class DashboardController : Controller {
        [Route("admin/dashboard-tpl")]
        public IActionResult HomeTemplate() {
            return View();
        }
    }
}
