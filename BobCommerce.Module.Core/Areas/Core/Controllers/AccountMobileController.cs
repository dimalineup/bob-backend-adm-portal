﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using BobCommerce.Module.Core.Areas.Core.ViewModels.Account;
using BobCommerce.Module.Core.Models;
using BobCommerce.Module.Core.Areas.Core.ViewModels;
using BobCommerce.Infrastructure.Data;
using System;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Module.Core.Services;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Net.Http.Headers;
using Microsoft.Extensions.Configuration;

namespace BobCommerce.Module.Core.Areas.Core.Controllers {

    public class LoginItem {
        public string userId { get; set; }
        public string role { get; set; }
    }

    [Area("Core")]
    //[Authorize]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("core")]
    public class AccountMobileController : Controller {

        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly IRepository<User> _userRepository;
        private readonly ILogger _logger;
        private readonly IMediaService _mediaService;
        private readonly IConfiguration _configuration;

        public AccountMobileController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IRepository<User> userRepository,
            IEmailSender emailSender,
            //ISmsSender smsSender,
            ILoggerFactory loggerFactory,
            IMediaService mediaService,
            IConfiguration config) {
            _userManager = userManager;
            _signInManager = signInManager;
            _userRepository = userRepository;
            _emailSender = emailSender;
            //_smsSender = smsSender;
            _logger = loggerFactory.CreateLogger<AccountController>();
            _mediaService = mediaService;
            _configuration = config;
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("login")]
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        [Route("login")]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null) {
            LoginItem ret = new LoginItem();
            if (ModelState.IsValid) {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded) {
                    _logger.LogInformation(1, "User logged in.");

                    //update lastlogindate
                    var user = await _userRepository.Query().Include(x => x.Roles).ThenInclude(xx => xx.Role).FirstOrDefaultAsync(x => x.Email == model.Email);
                    ret.userId = user.Id.ToString();
                    foreach (var role in user.Roles) {
                        ret.role = role.Role.Name;
                    }

                    user.LastLoginDate = DateTime.Now;
                    await _userManager.UpdateAsync(user);
                    return Ok(ret);
                }
                if (result.RequiresTwoFactor) {
                    //return Ok("Two-steps authentication required.");
                    return Ok(result);
                }
                if (result.IsLockedOut) {
                    //return Ok("User account locked out.");
                    return Ok(result);
                }
                else {
                    //return Ok("Invalid login attempt.");
                    return Ok(result);
                }
            }
            return Ok(ret);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("updatedevicetoken")]
        public async Task<IActionResult> UpdateDeviceToken([FromBody] DeviceTokenVm model) {
            if (ModelState.IsValid) {
                var user = await _userRepository.Query().Where(x => x.Id == model.UserId).FirstOrDefaultAsync();
                user.DeviceToken = model.DeviceToken;
                user.DeviceType = model.DeviceType;
                await _userRepository.SaveChangesAsync();
                return Ok(true);
            }
            return Ok(false);
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("register")]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        private IActionResult RedirectToLocal(string returnUrl) {
            if (Url.IsLocalUrl(returnUrl)) {
                return Redirect(returnUrl);
            }
            else {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        [Route("register")]
        public async Task<IActionResult> Register(IFormFile file, string email, string fullName, string password, DateTime? birthday) {
            string userID = "";
            if (ModelState.IsValid) {
                var user = new User { UserName = email, Email = email, FullName = fullName, Birthday = birthday, AccountType = (int)UserAccountType.Regular };
                var result = await _userManager.CreateAsync(user, password);
                if (result.Succeeded) {
                    await _userManager.AddToRoleAsync(user, "customer");

                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, "User created a new account with password.");

                    if (file != null && file.Length > 0) {
                        var fileName = await SaveFile(file);

                        if (user.UserImage != null) {
                            user.UserImage.FileName = fileName;
                        }
                        else {
                            user.UserImage = new Media { FileName = fileName };
                        }
                        await _userRepository.SaveChangesAsync();
                    }

                    userID = user.Id.ToString();
                    return Ok(userID);
                }
                else {
                    return Ok(result);
                }
            }

            return Ok(userID);
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        [Route("loginwithfacebook")]
        public async Task<IActionResult> LoginWithFacebook(string image, string email, string fullName, string password) {
            string userID = "";
            if (ModelState.IsValid) {
                var user = new User { UserName = email, Email = email, FullName = fullName, AccountType = (int)UserAccountType.Facebook };
                var result = await _userManager.CreateAsync(user, password);
                if (result.Succeeded) {
                    await _userManager.AddToRoleAsync(user, "customer");

                    await _signInManager.SignInAsync(user, isPersistent: false);

                    user.FacebookImageUrl = image;
                    await _userRepository.SaveChangesAsync();
                    userID = user.Id.ToString();
                    return Ok(userID);
                }
                else {
                    return Ok(result);
                }
            }
            return Ok(userID);
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        [Route("forgotpassword")]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model) {
            if (ModelState.IsValid) {
                var user = await _userManager.FindByNameAsync(model.Email);
                //if (user == null || !(await _userManager.IsEmailConfirmedAsync(user))) {
                if (user == null) {
                    // Don't reveal that the user does not exist or is not confirmed
                    //return View("ForgotPasswordConfirmation");
                    return Ok(false);
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=532713
                // Send an email with this link
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: HttpContext.Request.Scheme);
                await _emailSender.SendEmailAsync(model.Email, "Reset Password",
                   $"Please reset your password by clicking <a href='{callbackUrl}'>here</a>.", true);
                //return View("ForgotPasswordConfirmation");
                return Ok(true);
            }

            // If we got this far, something failed, redisplay form
            //return View(model);
            return Ok(false);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation() {
            return View();
        }

        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        [Route("Account/ExternalLogin/{provider}")]
        public IActionResult ExternalLogin(string provider, string returnUrl = null) {
            // Request a redirect to the external login provider.
            var redirectUrl = Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            var x = Challenge(properties, provider);
            return x;
        }

        // GET: /Account/ExternalLoginCallback
        [HttpGet]
        [AllowAnonymous]
        [Route("Account/ExternalLoginCallback")]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null) {
            if (remoteError != null) {
                ModelState.AddModelError(string.Empty, $"Error from external provider: {remoteError}");
                return View(nameof(Login));
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null) {
                return RedirectToAction(nameof(Login));
            }

            // Sign in the user with this external login provider if the user already has a login.
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false);
            if (result.Succeeded) {
                _logger.LogInformation(5, "User logged in with {Name} provider.", info.LoginProvider);
                return RedirectToLocal(returnUrl);
            }
            if (result.RequiresTwoFactor) {
                //return RedirectToAction(nameof(SendCode), new { ReturnUrl = returnUrl });
            }
            if (result.IsLockedOut) {
                return View("Lockout");
            }
            else {
                // If the user does not have an account, then ask the user to create an account.
                ViewData["ReturnUrl"] = returnUrl;
                ViewData["LoginProvider"] = info.LoginProvider;
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                var name = info.Principal.FindFirstValue(ClaimTypes.Name);
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = email, FullName = name });
            }
        }

        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl = null) {
            if (ModelState.IsValid) {
                // Get the information about the user from the external login provider
                var info = await _signInManager.GetExternalLoginInfoAsync();
                if (info == null) {
                    return View("ExternalLoginFailure");
                }
                var user = new User { UserName = model.Email, Email = model.Email, FullName = model.FullName };
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded) {
                    await _userManager.AddToRoleAsync(user, "customer");

                    result = await _userManager.AddLoginAsync(user, info);
                    if (result.Succeeded) {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation(6, "User created an account using {Name} provider.", info.LoginProvider);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        [Route("updateprofile")]
        public async Task<IActionResult> UpdateUserProfile(IFormFile file, long userId, string fullName, long? age) {
            var user = _userRepository.Query().FirstOrDefault(x => x.Id == userId);
            if (user != null) {
                if (!string.IsNullOrEmpty(fullName)) {
                    user.FullName = fullName;
                }
                if(age != null && age.HasValue) {
                    user.Age = age.Value;
                }
                if (file != null && file.Length > 0) {
                    var fileName = await SaveFile(file);

                    if (user.UserImage != null) {
                        user.UserImage.FileName = fileName;
                    }
                    else {
                        user.UserImage = new Media { FileName = fileName };
                    }
                }
                await _userRepository.SaveChangesAsync();
                return Ok(true);
            }

            return Ok(false);
        }

        [HttpGet]
        [Route("getprofile/{userId}")]
        public async Task<IActionResult> GetProfile(long userId) {
            ProfileViewModel retUser = new ProfileViewModel();
            var user = _userRepository.Query().Include(x => x.UserImage).FirstOrDefault(x => x.Id == userId);
            if (user != null) {
                switch (user.AccountType) {
                    case (int)UserAccountType.Regular:
                        retUser = new ProfileViewModel {
                            FullName = user.FullName,
                            Age = user.Age,
                            ThumbnailImage = _configuration["MySettings:AbsoluteUrl"] + _mediaService.GetThumbnailUrl(user.UserImage),

                        };
                        break;
                    case (int)UserAccountType.Facebook:
                        retUser = new ProfileViewModel {
                            FullName = user.FullName,
                            Age = user.Age,
                            ThumbnailImage = user.FacebookImageUrl,

                        };
                        break;
                    case (int)UserAccountType.Google:
                        break;
                    default:
                        retUser = new ProfileViewModel {
                            FullName = user.FullName,
                            Age = user.Age,
                            ThumbnailImage = _configuration["MySettings:AbsoluteUrl"] + _mediaService.GetThumbnailUrl(user.UserImage),

                        };
                        break;
                }
            }

            return Ok(retUser);
        }

        private async Task<string> SaveFile(IFormFile file) {
            var originalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Value.Trim('"');
            var fileName = $"{Guid.NewGuid()}{Path.GetExtension(originalFileName)}";
            await _mediaService.SaveMediaAsync(file.OpenReadStream(), fileName, file.ContentType);
            return fileName;
        }

        [HttpPut("updatelang/{userId}/{lang}")]
        public async Task<IActionResult> Put(long userId, string lang) {

            var user = await _userRepository.Query().FirstOrDefaultAsync(x => x.Id == userId);
            if (user == null) {
                return Ok(false);
            }

            user.Culture = lang;
            await _userRepository.SaveChangesAsync();
            return Ok(true);
        }

        [HttpPost("changepassword/{userId}")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model, long userId) {
            if (!ModelState.IsValid) {
                return View(model);
            }
            //var user = await GetCurrentUserAsync();
            var user = await _userRepository.Query().FirstOrDefaultAsync(x => x.Id == userId);
            if (user != null) {//old password not good
                var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                if (result.Succeeded) {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, "User changed their password successfully.");
                    return Ok(true);
                }
                AddErrors(result);
                return Ok(false);
            }
            return Ok(false);
        }

        [HttpPost("changepassword2/{userId}")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword2(ChangePasswordViewModel model, long userId) {
            if (!ModelState.IsValid) {
                return Json(new { success = false, message = "model not valid" });
            }
            var user = await _userRepository.Query().FirstOrDefaultAsync(x => x.Id == userId);
            if (user != null) {
                var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                if (result.Succeeded) {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, "User changed their password successfully.");
                    return Json(new { success = true, message = "" });
                }
                return Json(new { success = false, message = result.Errors.FirstOrDefault().Description });
            }
            return Json(new { success = false, message = "error" });
        }

        #region Helpers
        private void AddErrors(IdentityResult result) {
            foreach (var error in result.Errors) {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
        #endregion
    }
}
