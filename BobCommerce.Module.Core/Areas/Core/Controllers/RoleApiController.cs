﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Module.Core.Models;
using System;
using BobCommerce.Module.Core.Areas.Core.ViewModels;
using BobCommerce.Module.Core.Extensions;

namespace BobCommerce.Module.Core.Areas.Core.Controllers {
    [Area("Core")]
    [Authorize(Roles = "admin, vendor")]
    [Route("api/roles")]
    public class RoleApiController : Controller {
        private readonly IRepository<Role> _roleRepository;
        private readonly IWorkContext _workContext;
        private IRepository<User> _userRepository;

        public RoleApiController(IRepository<Role> roleRepository, IWorkContext workContext, IRepository<User> userRepository) {
            _roleRepository = roleRepository;
            _workContext = workContext;
            _userRepository = userRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Get() {

            var currentUser = await _workContext.GetCurrentUser();
            var user = _userRepository.Query().Include(x => x.Roles).ThenInclude(xx => xx.Role).FirstOrDefault(x => x.UserName == currentUser.UserName);
            if(user.Roles.Any(x => x.Role.Name.Contains("admin"))) {
                var roles = await _roleRepository.Query().Select(x => new {
                    x.Id,
                    x.Name
                }).ToListAsync();

                return Json(roles);
            }
            else {
                var roles = await _roleRepository.Query().Where(x => x.Name != "admin").Select(x => new {
                    x.Id,
                    x.Name
                }).ToListAsync();

                return Json(roles);
            }

            
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] RoleForm model) {
            try {
                if (ModelState.IsValid) {
                    var role = await _roleRepository.Query().FirstOrDefaultAsync(x => x.Id == id);
                    if (role == null) {
                        return NotFound();
                    }

                    role.Name = model.RoleName;
                    await _roleRepository.SaveChangesAsync();
                    return Accepted();
                }

                return BadRequest(ModelState);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RoleForm model) {
            try {
                if (ModelState.IsValid) {

                    var role = new Role {
                        Name = model.RoleName
                    };

                    _roleRepository.Add(role);
                    await _roleRepository.SaveChangesAsync();

                    return CreatedAtAction(nameof(Get), new { id = role.Id }, null);
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id) {
            var role = await _roleRepository.Query().FirstOrDefaultAsync(x => x.Id == id);
            if (role == null) {
                return NotFound();
            }

            try {
                _roleRepository.Remove(role);
                await _roleRepository.SaveChangesAsync();
            }
            catch (DbUpdateException) {
                return BadRequest(new { Error = $"The role {role.Name} can't not be deleted because it is referenced by other tables" });
            }

            return NoContent();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id) {
            var role = await _roleRepository.Query().FirstOrDefaultAsync(x => x.Id == id);
            if (role == null) {
                return NotFound();
            }

            var model = new RoleForm {
                Id = role.Id,
                RoleName = role.Name
            };

            return Json(model);
        }
    }
}
