﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Infrastructure.Web.SmartTable;
using BobCommerce.Module.Core.Areas.Core.ViewModels;
using BobCommerce.Module.Core.Models;
using BobCommerce.Infrastructure.Helpers;

namespace BobCommerce.Module.Core.Areas.Core.Controllers {
    [Area("Core")]
    //[Authorize(Roles = "admin")]
    [Route("chat")]
    public class ChatMobileController : Controller {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<ChatMessage> _chatRepository;

        public ChatMobileController(IRepository<User> userRepository, IRepository<ChatMessage> chatRepository) {
            _userRepository = userRepository;
            _chatRepository = chatRepository;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ChatMessageForm model) {
            try {
                if (ModelState.IsValid) {

                    var chatMsg = new ChatMessage {
                        FromUserId = model.FromUserId,
                        ToUserId = model.ToUserId,
                        MessageType = model.MessageType.ToString(),
                        Message = model.Message
                    };

                    _chatRepository.Add(chatMsg);
                    await _chatRepository.SaveChangesAsync();

                    return CreatedAtAction(nameof(Get), new { id = chatMsg.Id }, null);
                }
                return BadRequest(ModelState);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get() {
            try {
                var chatMsgs = await _chatRepository
                    .Query()
                    .Select(x => new { Id = x.Id, FromUserId = x.FromUserId, ToUserId = x.ToUserId, MessageType = x.MessageType, Message = x.Message })
                    .ToListAsync();
                return Json(chatMsgs);
            }
            catch (Exception ex) {
                throw ex;
            }
        }

        [HttpGet("{fromUserId}/{toUserId}")]
        public async Task<IActionResult> GetConversation(long fromUserId, long toUserId) {
            try {
                var chatMsgs = await _chatRepository
                    .Query().Include(x => x.FromUser).Include(x => x.ToUser)
                    .OrderBy(x => x.Id)
                    .Where(x => x.FromUserId == fromUserId && x.ToUserId == toUserId)
                    .Select(x => new { i = x.Id, fu = x.FromUser.FullName, tu = x.ToUser.FullName, t = x.MessageType, m = x.Message, d = x.DateInserted.ToString("MM/dd/yyyy hh:mm:ss") })
                    .ToListAsync();
                return Json(chatMsgs);
            }
            catch (Exception ex) {
                throw ex;
            }
        }
    }
}
