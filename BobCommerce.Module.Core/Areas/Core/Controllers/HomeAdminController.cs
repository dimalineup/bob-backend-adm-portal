﻿using BobCommerce.Infrastructure.Data;
using BobCommerce.Module.Core.Extensions;
using BobCommerce.Module.Core.Models;
using BobCommerce.Module.Core.Services;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BobCommerce.Module.Core.Areas.Core.Controllers
{
    [Area("Core")]
    //[Authorize(Roles = "admin, vendor, waiter, barman, kitchen, manager")]
    [Authorize(Roles = "admin, vendor, waiter, barman, kitchen")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeAdminController : Controller {
        private const string ImpersonationCookieName = "ImpersonateVendor";
        private const string ImpersonationImageName = "ImpersonateImage";
        private readonly IAntiforgery _antiforgery;
        private HttpContext _httpContext;

        public HomeAdminController(IAntiforgery antiforgery, IHttpContextAccessor contextAccessor) {
            _antiforgery = antiforgery;
            _httpContext = contextAccessor.HttpContext;
        }

        [Route("admin")]
        public IActionResult Index() {

            ViewBag.Impersonation = GetImpersonationFromCookies();
            ViewBag.Image = GetImageFromCookies();
            var tokens = _antiforgery.GetAndStoreTokens(HttpContext);
            HttpContext.Response.Cookies.Append("XSRF-TOKEN",
                tokens.RequestToken, new CookieOptions { HttpOnly = false, IsEssential = true }
            );

            return View();
        }

        public string GetImpersonationFromCookies() {
            if (_httpContext.Request.Cookies.ContainsKey(ImpersonationCookieName)) {
                return _httpContext.Request.Cookies[ImpersonationCookieName];
            }
            return null;
        }

        public string GetImageFromCookies() {
            if (_httpContext.Request.Cookies.ContainsKey(ImpersonationImageName)) {
                return _httpContext.Request.Cookies[ImpersonationImageName];
            }
            return null;
        }
    }
}
