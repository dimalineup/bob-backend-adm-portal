﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Infrastructure.Web.SmartTable;
using BobCommerce.Module.Core.Areas.Core.ViewModels;
using BobCommerce.Module.Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using BobCommerce.Module.Core.Services;
using BobCommerce.Module.Core.Extensions;
using BobCommerce.Infrastructure.Localization;
using System.Globalization;

namespace BobCommerce.Module.Core.Areas.Core.Controllers {
    [Area("Core")]
    [Authorize(Roles = "admin, vendor")]
    [Route("api/users")]
    public class UserApiController : Controller {
        private const string ImpersonationCookieName = "ImpersonateVendor";
        private readonly IRepository<User> _userRepository;
        private readonly UserManager<User> _userManager;
        private readonly IMediaService _mediaService;
        private readonly IWorkContext _workContext;
        private readonly IRepository<Resource> _resourceRepository;
        private HttpContext _httpContext;

        public UserApiController(IRepository<User> userRepository, UserManager<User> userManager, IMediaService mediaService, IHttpContextAccessor contextAccessor, IWorkContext workContext, 
            IRepository<Resource> resourceRepository) {
            _userRepository = userRepository;
            _userManager = userManager;
            _mediaService = mediaService;
            _httpContext = contextAccessor.HttpContext;
            _workContext = workContext;
            _resourceRepository = resourceRepository;
        }

        [HttpGet("quick-search")]
        public async Task<IActionResult> QuickSearch(UserSearchOption searchOption) {
            var query = _userRepository.Query().Where(x => !x.IsDeleted);
            if (!string.IsNullOrWhiteSpace(searchOption.Name)) {
                query = query.Where(x => x.FullName.Contains(searchOption.Name));
            }

            if (!string.IsNullOrWhiteSpace(searchOption.Email)) {
                query = query.Where(x => x.Email.Contains(searchOption.Email));
            }

            var users = await query.Take(5).Select(x => new {
                x.Id,
                x.FullName,
                x.Email,
                x.PhoneNumber
            }).ToListAsync();

            return Ok(users);
        }

        private string GetImpersonationFromCookies() {
            if (_httpContext.Request.Cookies.ContainsKey(ImpersonationCookieName)) {
                return _httpContext.Request.Cookies[ImpersonationCookieName];
            }
            return null;
        }

        private string GetTranslation(string lang, string key) {
            var translation = _resourceRepository.Query().Where(x => x.CultureId.ToLower() == lang.ToLower() && x.Key == key).FirstOrDefault();
            if(translation != null) {
                return translation.Value;
            }
            return null;
        }

        [HttpPost("grid")]
        public async Task<IActionResult> List([FromBody] SmartTableParam param) {
            var vendorID = GetImpersonationFromCookies();
            var query = _userRepository.Query()
                .Include(x => x.Roles)
                    .ThenInclude(x => x.Role)
                .Include(x => x.CustomerGroups)
                    .ThenInclude(x => x.CustomerGroup)
                .Where(x => !x.IsDeleted && x.FullName != "guest");

            if (!string.IsNullOrEmpty(vendorID)) {
                query = query.Where(x => x.VendorId == int.Parse(vendorID));
            }

            var currentUser = await _workContext.GetCurrentUser();
            if (!User.IsInRole("admin")) {
                query = query.Where(x => x.VendorId == currentUser.VendorId);
            }

            if (param.Search.PredicateObject != null) {
                dynamic search = param.Search.PredicateObject;

                if (search.Email != null) {
                    string email = search.Email;
                    query = query.Where(x => x.Email.Contains(email));
                }

                if (search.FullName != null) {
                    string fullName = search.FullName;
                    query = query.Where(x => x.FullName.Contains(fullName));
                }

                if (search.RoleId != null) {
                    long roleId = search.RoleId;
                    query = query.Where(x => x.Roles.Any(r => r.RoleId == roleId));
                }

                if (search.CustomerGroupId != null) {
                    long customerGroupId = search.CustomerGroupId;
                    query = query.Where(x => x.CustomerGroups.Any(g => g.CustomerGroupId == customerGroupId));
                }

                if (search.CreatedOn != null) {
                    if (search.CreatedOn.before != null) {
                        DateTimeOffset before = search.CreatedOn.before;
                        query = query.Where(x => x.CreatedOn <= before);
                    }

                    if (search.CreatedOn.after != null) {
                        DateTimeOffset after = search.CreatedOn.after;
                        query = query.Where(x => x.CreatedOn >= after);
                    }
                }
            }

            var culture = currentUser.Culture;
            if (culture == null) {
                culture = "en-US";
            }

            var users = query.ToSmartTableResultNoProjection(
                param,
                user => new {
                    user.Id,
                    user.Email,
                    user.FullName,
                    CreatedOn = user.CreatedOn.ToString(),
                    LastLoginDate = (user.LastLoginDate.HasValue ? user.LastLoginDate.Value.ToString("MMM dd, yyyy HH:mm:ss tt", new CultureInfo(culture)) : ""),
                    Roles = user.Roles.Select(x => GetTranslation(culture, x.Role.Name) == null ? x.Role.Name : GetTranslation(culture, x.Role.Name)),
                    CustomerGroups = user.CustomerGroups.Select(x => x.CustomerGroup.Name)
                });

            return Json(users);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id) {
            var user = await _userRepository.Query()
                .Include(x => x.Roles)
                .Include(x => x.CustomerGroups)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (user == null) {
                return NotFound();
            }

            var model = new UserVm {
                Id = user.Id,
                FullName = user.FullName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                VendorId = user.VendorId,
                RoleIds = user.Roles.Select(x => x.RoleId).ToList(),
                CustomerGroupIds = user.CustomerGroups.Select(x => x.CustomerGroupId).ToList()
            };

            return Json(model);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserForm model) {
            if (ModelState.IsValid) {
                var user = new User {
                    UserName = model.Email,
                    Email = model.Email,
                    FullName = model.FullName,
                    PhoneNumber = model.PhoneNumber,
                    VendorId = model.VendorId
                };

                var currentUser = await _workContext.GetCurrentUser();
                if (!User.IsInRole("admin")) {
                    user.VendorId = currentUser.VendorId;
                }

                foreach (var roleId in model.RoleIds) {
                    var userRole = new UserRole {
                        RoleId = roleId
                    };

                    user.Roles.Add(userRole);
                    userRole.User = user;
                }

                foreach (var customergroupId in model.CustomerGroupIds) {
                    var userCustomerGroup = new CustomerGroupUser {
                        CustomerGroupId = customergroupId
                    };
                }

                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded) {
                    return CreatedAtAction(nameof(Get), new { id = user.Id }, null);
                }

                AddErrors(result);
            }

            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] UserForm model) {
            if (ModelState.IsValid) {
                var user = await _userRepository.Query()
                    .Include(x => x.Roles)
                    .Include(x => x.CustomerGroups)
                    .FirstOrDefaultAsync(x => x.Id == id);

                if (user == null) {
                    return NotFound();
                }

                user.Email = model.Email;
                user.UserName = model.Email;
                user.FullName = model.FullName;
                user.PhoneNumber = model.PhoneNumber;
                user.VendorId = model.VendorId;
                AddOrDeleteRoles(model, user);
                AddOrDeleteCustomerGroups(model, user);

                //await SaveUserMedia(model, user);

                var result = await _userManager.UpdateAsync(user);

                if (result.Succeeded) {
                    return Accepted();
                }

                AddErrors(result);
            }

            return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id) {
            var user = await _userRepository.Query().FirstOrDefaultAsync(x => x.Id == id);
            if (user == null) {
                return NotFound();
            }

            user.IsDeleted = true;
            user.LockoutEnabled = true;
            user.LockoutEnd = DateTime.Now.AddYears(200);
            await _userRepository.SaveChangesAsync();
            return NoContent();
        }

        private void AddOrDeleteRoles(UserForm model, User user) {
            foreach (var roleId in model.RoleIds) {
                if (user.Roles.Any(x => x.RoleId == roleId)) {
                    continue;
                }

                var userRole = new UserRole {
                    RoleId = roleId,
                    User = user
                };
                user.Roles.Add(userRole);
            }

            var deletedUserRoles =
                user.Roles.Where(userRole => !model.RoleIds.Contains(userRole.RoleId))
                    .ToList();

            foreach (var deletedUserRole in deletedUserRoles) {
                deletedUserRole.User = null;
                user.Roles.Remove(deletedUserRole);
            }
        }

        private void AddOrDeleteCustomerGroups(UserForm model, User user) {
            foreach (var customergroupId in model.CustomerGroupIds) {
                if (user.CustomerGroups.Any(x => x.CustomerGroupId == customergroupId)) {
                    continue;
                }

                var userCustomerGroup = new CustomerGroupUser {
                    CustomerGroupId = customergroupId,
                    User = user
                };
                user.CustomerGroups.Add(userCustomerGroup);
            }

            var deletedUserCustomerGroups =
                user.CustomerGroups.Where(userCustomerGroup => !model.CustomerGroupIds.Contains(userCustomerGroup.CustomerGroupId))
                    .ToList();

            foreach (var deletedUserCustomerGroup in deletedUserCustomerGroups) {
                deletedUserCustomerGroup.User = null;
                user.CustomerGroups.Remove(deletedUserCustomerGroup);
            }
        }

        private void AddErrors(IdentityResult result) {
            foreach (var error in result.Errors) {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        //private async Task SaveUserMedia(UserForm model, User user) {
        //    if (model.UserImage != null) {
        //        var fileName = await SaveFile(model.UserImage);
        //        if (user.UserImage != null) {
        //            user.UserImage.FileName = fileName;
        //        }
        //        else {
        //            user.UserImage = new Media { FileName = fileName };
        //        }
        //    }
        //}

        private async Task<string> SaveFile(IFormFile file) {
            var originalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Value.Trim('"');
            var fileName = $"{Guid.NewGuid()}{Path.GetExtension(originalFileName)}";
            await _mediaService.SaveMediaAsync(file.OpenReadStream(), fileName, file.ContentType);
            return fileName;
        }
    }
}
