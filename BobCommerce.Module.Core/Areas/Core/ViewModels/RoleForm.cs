﻿namespace BobCommerce.Module.Core.Areas.Core.ViewModels
{
    public class RoleForm
    {
        public long Id { get; set; }

        public string RoleName { get; set; }
    }
}
