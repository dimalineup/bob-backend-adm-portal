﻿using System.ComponentModel.DataAnnotations;

namespace BobCommerce.Module.Core.Areas.Core.ViewModels.Account {
    public class DeviceTokenVm {
        public long UserId { get; set; }

        public string DeviceToken { get; set; }

        public string DeviceType { get; set; }
    }
}
