﻿using System.ComponentModel.DataAnnotations;

namespace BobCommerce.Module.Core.Areas.Core.ViewModels.Account {
    public class ProfileViewModel {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public long? Age { get; set; }
        public string ThumbnailImage { get; set; }
   }
}
