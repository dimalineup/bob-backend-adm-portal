﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace BobCommerce.Module.Core.Areas.Core.ViewModels {
    public class ChatMessageForm {
        public long Id { get; set; }
        public long FromUserId { get; set; }
        public long ToUserId { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ChatMessageType MessageType { get; set; }
        public string Message { get; set; }
    }
}
