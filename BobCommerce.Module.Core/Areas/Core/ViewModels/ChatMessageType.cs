﻿namespace BobCommerce.Module.Core.Areas.Core.ViewModels {
    public enum ChatMessageType {
        Invitation,

        Conversation
    }
}
