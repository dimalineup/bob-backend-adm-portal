﻿using System.Collections.Generic;

namespace BobCommerce.Module.Core.Areas.Core.ViewModels {
    public class HomeViewModel {
        public IList<WidgetInstanceViewModel> WidgetInstances { get; set; } = new List<WidgetInstanceViewModel>();
    }
}
