﻿namespace BobCommerce.Module.Catalog.Areas.Catalog.ViewModels {
    public class MediaViewModel {
        public string Url { get; set; }

        public string ThumbnailUrl { get; set; }
    }
}
