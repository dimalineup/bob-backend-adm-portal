﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using BobCommerce.Infrastructure.Modules;
using BobCommerce.Module.Core.Extensions;
using BobCommerce.Module.Core.Models;
using BobCommerce.Module.Core.Services;

namespace BobCommerce.Module.Core {
    public class ModuleInitializer : IModuleInitializer {
        public void ConfigureServices(IServiceCollection serviceCollection) {
            serviceCollection.AddTransient<IEntityService, EntityService>();
            serviceCollection.AddTransient<IMediaService, MediaService>();
            serviceCollection.AddTransient<IThemeService, ThemeService>();
            serviceCollection.AddTransient<ITokenService, TokenService>();
            serviceCollection.AddTransient<IWidgetInstanceService, WidgetInstanceService>();
            serviceCollection.AddScoped<SignInManager<User>, SimplSignInManager<User>>();
            serviceCollection.AddScoped<IWorkContext, WorkContext>();
            serviceCollection.AddScoped<ISmsSender, SmsSender>();
            serviceCollection.AddSingleton<SettingDefinitionProvider>();
            serviceCollection.AddScoped<ISettingService, SettingService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
        }
    }
}
