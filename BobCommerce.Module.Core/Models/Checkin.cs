﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BobCommerce.Infrastructure.Models;
using BobCommerce.Module.Core.Models;

namespace BobCommerce.Module.Core.Models {

    public class Checkin : EntityBase {
        public Checkin() { }

        public Checkin(long id) {
            Id = id;
        }

        public long UserId { get; set; }
        public User user { get; set; }
        public long VendorId { get; set; }
        public Vendor vendor { get; set; }
        public DateTimeOffset CheckinDateTime { get; set; }
        public bool? IsVisible { get; set; }
    }
}
