﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BobCommerce.Infrastructure.Models;

namespace BobCommerce.Module.Core.Models {
    public class MySettings {
        public string NotificationUrl { get; set; }
        public string NotificationAuthKey { get; set; }
        public string NotificationTime { get; set; }
    }
}
