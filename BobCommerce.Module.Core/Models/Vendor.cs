﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BobCommerce.Infrastructure.Models;

namespace BobCommerce.Module.Core.Models
{
    public class Vendor : EntityBase {
        public Vendor() {
            CreatedOn = DateTimeOffset.Now;
        }

        [Required(ErrorMessage = "The {0} field is required.")]
        [StringLength(450)]
        public string Name { get; set; }

        [Required(ErrorMessage = "The {0} field is required.")]
        [StringLength(450)]
        public string Slug { get; set; }

        public string Description { get; set; }

        public string Email { get; set; }

        public DateTimeOffset CreatedOn { get; set; }

        public DateTimeOffset LatestUpdatedOn { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public IList<User> Users { get; set; } = new List<User>();

        public string Address { get; set; }

        public string WorkingHours { get; set; }

        public Media ThumbnailImage { get; set; }

        public string PhoneNumber { get; set; }

        public string SiteUrl { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string LocationType { get; set; }

        public IList<Checkin> CheckedIn { get; set; } = new List<Checkin>();

        public IList<Rating> Rating { get; set; } = new List<Rating>();

        [Required(ErrorMessage = "The {0} field is required.")]
        [Range(10, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public decimal Discount { get; set; }

        public string NameHE { get; set; }
        public string DescriptionHE { get; set; }
        public string AddressHE { get; set; }
    }
}
