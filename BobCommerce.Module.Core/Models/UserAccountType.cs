﻿namespace BobCommerce.Module.Core.Models {
    public enum UserAccountType {
        Regular = 0,

        Facebook = 1,

        Google = 2
    }
}
