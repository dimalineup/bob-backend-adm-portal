﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BobCommerce.Infrastructure.Models;
using BobCommerce.Module.Core.Models;

namespace BobCommerce.Module.Core.Models
{
    public class RatingMobile : EntityBase {
        public RatingMobile() { }

        public long VendorId { get; set; }

        public long UserId { get; set; }

        public int RatingStars { get; set; }

        public string Review { get; set; }

        public User user { get; set; }

        public string UserName { get; set; }

        public DateTime DateInserted { get; set; }
    }
}
