﻿using BobCommerce.Infrastructure.Models;
using System;

namespace BobCommerce.Module.Core.Models {
    public class ChatMessage : EntityBase {
        public ChatMessage() { }

        public ChatMessage(long id) {
            Id = id;
        }

        public long FromUserId { get; set; }

        public long ToUserId { get; set; }

        public string MessageType { get; set; }

        public string Message { get; set; }

        public DateTime DateInserted { get; set; }

        public User FromUser { get; set; }

        public User ToUser { get; set; }
    }
}
