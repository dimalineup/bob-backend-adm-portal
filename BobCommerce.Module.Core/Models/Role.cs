﻿using Microsoft.AspNetCore.Identity;
using BobCommerce.Infrastructure.Models;
using System.Collections.Generic;

namespace BobCommerce.Module.Core.Models {
    public class Role : IdentityRole<long>, IEntityWithTypedId<long> {
        public IList<UserRole> Users { get; set; } = new List<UserRole>();
    }
}
