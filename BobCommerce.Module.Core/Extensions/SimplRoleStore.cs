﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using BobCommerce.Module.Core.Data;
using BobCommerce.Module.Core.Models;

namespace BobCommerce.Module.Core.Extensions {
    public class SimplRoleStore : RoleStore<Role, SimplDbContext, long, UserRole, IdentityRoleClaim<long>> {
        public SimplRoleStore(SimplDbContext context) : base(context) {
        }
    }
}
