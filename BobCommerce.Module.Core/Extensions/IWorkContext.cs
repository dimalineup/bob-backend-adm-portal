﻿using System.Threading.Tasks;
using BobCommerce.Module.Core.Models;

namespace BobCommerce.Module.Core.Extensions {
    public interface IWorkContext {
        Task<User> GetCurrentUser();
    }
}
