﻿using MediatR;

namespace BobCommerce.Module.Core.Events {
    public class EntityDeleting : INotification {
        public long EntityId { get; set; }
    }
}
