﻿using MediatR;

namespace BobCommerce.Module.Core.Events {
    public class UserSignedIn : INotification {
        public long UserId { get; set; }
    }
}
