﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Module.Core.Extensions;
using BobCommerce.Module.Core.Models;
using BobCommerce.Infrastructure.Localization;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace BobCommerce.Module.Localization.Areas.Localization.Controllers
{
    [Area("Localization")]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("mobile")]
    public class LocalizationMobileController : Controller
    {
        private readonly IRepositoryWithTypedId<User, long> _userRepository;
        private readonly IWorkContext _workContext;
        private readonly IRepository<Resource> _resourceRepository;

        public LocalizationMobileController(IRepositoryWithTypedId<User, long> userRepository, IWorkContext workContext, IRepository<Resource> resourceRepository)
        {
            _userRepository = userRepository;
            _workContext = workContext;
            _resourceRepository = resourceRepository;
        }

        [HttpGet("translation/{lang}")]
        public async Task<IActionResult> Get(string lang)
        {
            var translation = _resourceRepository.Query().Where(x => x.CultureId.ToLower() == lang.ToLower() && x.Key.Contains("Mobile Translation ")).FirstOrDefault();
            //dynamic retVal = JObject.Parse(translation.Value);
            //return Json(retVal);
            return Ok(translation.Value);
        }
    }
}
