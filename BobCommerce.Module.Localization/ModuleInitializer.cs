﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using MediatR;
using BobCommerce.Infrastructure.Modules;
using BobCommerce.Module.Core.Events;
using BobCommerce.Module.Localization.Events;

namespace BobCommerce.Module.Localization {
    public class ModuleInitializer : IModuleInitializer {
        public void ConfigureServices(IServiceCollection services) {
            services.AddTransient<INotificationHandler<UserSignedIn>, UserSignedInHandler>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {

        }
    }
}
