﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using BobCommerce.Infrastructure.Modules;
using BobCommerce.Module.Pricing.Services;

namespace BobCommerce.Module.Pricing
{
    public class ModuleInitializer : IModuleInitializer {
        public void ConfigureServices(IServiceCollection services) {
            services.AddTransient<ICouponService, CouponService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {

        }
    }
}
