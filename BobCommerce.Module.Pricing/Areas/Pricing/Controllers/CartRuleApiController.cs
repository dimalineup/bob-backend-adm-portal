﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Infrastructure.Web.SmartTable;
using BobCommerce.Module.Pricing.Areas.Pricing.ViewModels;
using BobCommerce.Module.Pricing.Models;
using BobCommerce.Module.Pricing.ViewModels;
using BobCommerce.Module.Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using System;
using System.IO;
using BobCommerce.Module.Core.Services;
using System.Globalization;
using BobCommerce.Module.Core.Extensions;

namespace BobCommerce.Module.Pricing.Areas.Pricing.Controllers
{
    [Area("Pricing")]
    [Authorize(Roles = "admin")]
    [Route("api/cart-rules")]
    public class CartRuleApiController : Controller {
        private readonly IRepository<CartRule> _cartRuleRepository;
        private readonly IMediaService _mediaService;
        private readonly IWorkContext _workContext;

        public CartRuleApiController(IRepository<CartRule> cartRuleRepository, IMediaService mediaService, IWorkContext workContext) {
            _cartRuleRepository = cartRuleRepository;
            _mediaService = mediaService;
            _workContext = workContext;
        }

        [HttpPost("grid")]
        public async Task<IActionResult> List([FromBody] SmartTableParam param) {
            var currentUser = await _workContext.GetCurrentUser();
            var culture = currentUser.Culture;
            if (culture == null) {
                culture = "en-US";
            }
            var cartRules = _cartRuleRepository.Query().Include(x => x.vendor).Select(
                cartRule => new {
                    cartRule.Id,
                    cartRule.Name,
                    StartOn = cartRule.StartOn.HasValue ? cartRule.StartOn.Value.ToString("MMM dd, yyyy HH:mm:ss tt", new CultureInfo(culture)) : "",
                    EndOn = cartRule.EndOn.HasValue ? cartRule.EndOn.Value.ToString("MMM dd, yyyy HH:mm:ss tt", new CultureInfo(culture)) : "",
                    cartRule.IsActive,
                    vendorName = cartRule.vendor.Name
                }).OrderBy(x => x.vendorName).ToList();

            return Json(cartRules);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id) {
            var catrtRule = await _cartRuleRepository.Query()
                .Include(x => x.Image)
                .Include(x => x.Coupons)
                .Include(x => x.Products).ThenInclude(p => p.Product)
                .Include(x => x.vendor)
                .FirstOrDefaultAsync(x => x.Id == id);
            var model = new CartRuleVm {
                Id = catrtRule.Id,
                Name = catrtRule.Name,
                Description = catrtRule.Description,
                IsActive = catrtRule.IsActive,
                StartOn = catrtRule.StartOn,
                EndOn = catrtRule.EndOn,
                IsCouponRequired = catrtRule.IsCouponRequired,
                RuleToApply = catrtRule.RuleToApply,
                DiscountAmount = catrtRule.DiscountAmount,
                DiscountStep = catrtRule.DiscountStep,
                MaxDiscountAmount = catrtRule.MaxDiscountAmount,
                UsageLimitPerCoupon = catrtRule.UsageLimitPerCoupon,
                UsageLimitPerCustomer = catrtRule.UsageLimitPerCustomer,
                ThumbnailImageUrl = _mediaService.GetThumbnailUrl(catrtRule.Image),
                Products = catrtRule.Products.Select(x => new CartRuleProductVm { Id = x.ProductId, Name = x.Product.Name, IsPublished = x.Product.IsPublished }).ToList(),
                VendorId = catrtRule.vendor == null ? 0 : catrtRule.vendor.Id
            };

            if (catrtRule.IsCouponRequired) {
                var coupon = catrtRule.Coupons.FirstOrDefault();
                if (coupon != null) {
                    model.CouponCode = coupon.Code;
                }
            }

            return Json(model);
        }

        [HttpPost]
        public async Task<IActionResult> Post(CartRuleForm model) {
            if (ModelState.IsValid) {
                var cartRule = new CartRule {
                    Name = model.CartRule.Name,
                    Description = model.CartRule.Description,
                    IsActive = model.CartRule.IsActive,
                    StartOn = model.CartRule.StartOn,
                    EndOn = model.CartRule.EndOn,
                    IsCouponRequired = model.CartRule.IsCouponRequired,
                    RuleToApply = model.CartRule.RuleToApply,
                    DiscountAmount = model.CartRule.DiscountAmount,
                    DiscountStep = model.CartRule.DiscountStep,
                    MaxDiscountAmount = model.CartRule.MaxDiscountAmount,
                    UsageLimitPerCoupon = model.CartRule.UsageLimitPerCoupon,
                    UsageLimitPerCustomer = model.CartRule.UsageLimitPerCustomer,
                    VendorId = model.CartRule.VendorId
                };

                if (model.CartRule.IsCouponRequired && !string.IsNullOrWhiteSpace(model.CartRule.CouponCode)) {
                    var coupon = new Coupon {
                        CartRule = cartRule,
                        Code = model.CartRule.CouponCode
                    };

                    cartRule.Coupons.Add(coupon);
                }

                foreach (var item in model.CartRule.Products) {
                    var cartRuleProduct = new CartRuleProduct {
                        CartRule = cartRule,
                        ProductId = item.Id
                    };
                    cartRule.Products.Add(cartRuleProduct);
                }

                await SaveDiscountMedia(model, cartRule);

                _cartRuleRepository.Add(cartRule);
                await _cartRuleRepository.SaveChangesAsync();

                return CreatedAtAction(nameof(Get), new { id = cartRule.Id }, null);
            }
            return BadRequest(ModelState);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, CartRuleForm model) {
            //if (ModelState.IsValid) {
                var cartRule = await _cartRuleRepository.Query()
                    .Include(x => x.Coupons)
                    .Include(x => x.Products)
                    .FirstOrDefaultAsync(x => x.Id == id);
                if (cartRule == null) {
                    return NotFound();
                }

                cartRule.Name = model.CartRule.Name;
                cartRule.Description = model.CartRule.Description;
                cartRule.StartOn = model.CartRule.StartOn;
                cartRule.EndOn = model.CartRule.EndOn;
                cartRule.IsActive = model.CartRule.IsActive;
                cartRule.IsCouponRequired = model.CartRule.IsCouponRequired;
                cartRule.RuleToApply = model.CartRule.RuleToApply;
                cartRule.DiscountAmount = model.CartRule.DiscountAmount;
                cartRule.DiscountStep = model.CartRule.DiscountStep;
                cartRule.MaxDiscountAmount = model.CartRule.MaxDiscountAmount;
                cartRule.UsageLimitPerCoupon = model.CartRule.UsageLimitPerCoupon;
                cartRule.UsageLimitPerCustomer = model.CartRule.UsageLimitPerCustomer;
            cartRule.VendorId = model.CartRule.VendorId;

                if (model.CartRule.IsCouponRequired && !string.IsNullOrWhiteSpace(model.CartRule.CouponCode)) {
                    var coupon = cartRule.Coupons.FirstOrDefault();
                    if (coupon == null) {
                        coupon = new Coupon {
                            CartRule = cartRule,
                            Code = model.CartRule.CouponCode
                        };

                        cartRule.Coupons.Add(coupon);
                    }
                    else {
                        coupon.Code = model.CartRule.CouponCode;
                    }
                }

                foreach (var item in model.CartRule.Products) {
                    var cartRuleProduct = cartRule.Products.FirstOrDefault(x => x.ProductId == item.Id);
                    if (cartRuleProduct == null) {
                        cartRuleProduct = new CartRuleProduct {
                            CartRule = cartRule,
                            ProductId = item.Id
                        };
                        cartRule.Products.Add(cartRuleProduct);
                    }
                }

                var modelProductIds = model.CartRule.Products.Select(x => x.Id);
                var deletedProducts = cartRule.Products.Where(x => !modelProductIds.Contains(x.ProductId)).ToList();
                foreach (var item in deletedProducts) {
                    item.CartRule = null;
                    cartRule.Products.Remove(item);
                }

                await SaveDiscountMedia(model, cartRule);

                await _cartRuleRepository.SaveChangesAsync();
                return Accepted();
            //}
            //return BadRequest(ModelState);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id) {
            var cartRule = await _cartRuleRepository.Query().FirstOrDefaultAsync(x => x.Id == id);
            if (cartRule == null) {
                return NotFound();
            }

            try {
                _cartRuleRepository.Remove(cartRule);
                await _cartRuleRepository.SaveChangesAsync();
            }
            catch (DbUpdateException) {
                return BadRequest(new { Error = $"The cart rule {cartRule.Name} can't not be deleted because it has been used" });
            }

            return NoContent();
        }

        private async Task SaveDiscountMedia(CartRuleForm model, CartRule cartRule) {
            if (model.ThumbnailImage != null) {
                var fileName = await SaveFile(model.ThumbnailImage);
                if (cartRule.Image != null) {
                    cartRule.Image.FileName = fileName;
                }
                else {
                    cartRule.Image = new Media { FileName = fileName };
                }
            }
        }

        private async Task<string> SaveFile(IFormFile file) {
            var originalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Value.Trim('"');
            var fileName = $"{Guid.NewGuid()}{Path.GetExtension(originalFileName)}";
            await _mediaService.SaveMediaAsync(file.OpenReadStream(), fileName, file.ContentType);
            return fileName;
        }
    }
}
