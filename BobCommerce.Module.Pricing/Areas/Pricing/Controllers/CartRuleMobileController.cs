﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Infrastructure.Web.SmartTable;
using BobCommerce.Module.Pricing.Areas.Pricing.ViewModels;
using BobCommerce.Module.Pricing.Models;
using BobCommerce.Module.Pricing.ViewModels;
using BobCommerce.Module.Core.Services;
using System.Collections.Generic;
using System;
using Microsoft.Extensions.Configuration;

namespace BobCommerce.Module.Pricing.Areas.Pricing.Controllers
{
    [Area("Pricing")]
    //[Authorize(Roles = "admin")]
    [Route("cart-rules")]
    public class CartRuleMobileController : Controller {
        private readonly IRepository<CartRule> _cartRuleRepository;
        private readonly IMediaService _mediaService;
        private readonly IConfiguration _configuration;

        public CartRuleMobileController(IRepository<CartRule> cartRuleRepository, IMediaService mediaService, IConfiguration config) {
            _cartRuleRepository = cartRuleRepository;
            _mediaService = mediaService;
            _configuration = config;
        }

        public static List<T> RandomizeGenericList<T>(IList<T> originalList) {
            List<T> randomList = new List<T>();
            Random random = new Random();
            T value = default(T);

            //now loop through all the values in the list
            while (originalList.Count() > 0) {
                //pick a random item from th original list
                var nextIndex = random.Next(0, originalList.Count());
                //get the value for that random index
                value = originalList[nextIndex];
                //add item to the new randomized list
                randomList.Add(value);
                //remove value from original list (prevents
                //getting duplicates
                originalList.RemoveAt(nextIndex);
            }

            //return the randomized list
            return randomList;
        }

        [HttpGet("allrandom/{vendorId}")]
        public async Task<IActionResult> GetAllByVendor(long vendorId) {
            var discounts = await _cartRuleRepository.Query().Include(x => x.Image).Where(x => x.IsActive && x.VendorId == vendorId).Select(x => new CartRuleVm {
                Id = x.Id,
                Name = x.Name,
                StartOn = x.StartOn,
                EndOn = x.EndOn,
                ThumbnailImageUrl = _configuration["MySettings:AbsoluteUrl"] + _mediaService.GetThumbnailUrl(x.Image)
            }).ToListAsync();
            return Json(RandomizeGenericList(discounts));
        }

        [HttpGet("random/{vendorId}")]
        public async Task<IActionResult> GetOneByVendor(long vendorId) {
            var discounts = await _cartRuleRepository.Query().Include(x => x.Image).Where(x => x.IsActive && x.VendorId == vendorId).Select(x => new CartRuleVm {
                Id = x.Id,
                Name = x.Name,
                StartOn = x.StartOn,
                EndOn = x.EndOn,
                ThumbnailImageUrl = _configuration["MySettings:AbsoluteUrl"] + _mediaService.GetThumbnailUrl(x.Image)
            }).ToListAsync();
            return Json(RandomizeGenericList(discounts).FirstOrDefault());
        }

        [HttpGet("all")]
        public async Task<IActionResult> GetRandom() {
            var discounts = await _cartRuleRepository.Query().Include(x => x.Image).Where(x => x.IsActive && (x.VendorId == 0 || x.VendorId == null)).Take(5).Select(x => new CartRuleVm {
                Id = x.Id,
                Name = x.Name,
                StartOn = x.StartOn,
                EndOn = x.EndOn,
                ThumbnailImageUrl = _configuration["MySettings:AbsoluteUrl"] + _mediaService.GetThumbnailUrl(x.Image)
            }).ToListAsync();
            return Json(RandomizeGenericList(discounts));
        }

        [HttpPost("grid")]
        public IActionResult List([FromBody] SmartTableParam param) {
            IQueryable<CartRule> query = _cartRuleRepository
                .Query();

            var cartRules = query.ToSmartTableResult(
                param,
                cartRule => new {
                    cartRule.Id,
                    cartRule.Name,
                    cartRule.StartOn,
                    cartRule.EndOn,
                    cartRule.IsActive
                });

            return Json(cartRules);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id) {
            var catrtRule = await _cartRuleRepository.Query()
                .Include(x => x.Coupons)
                .Include(x => x.Products).ThenInclude(p => p.Product)
                .FirstOrDefaultAsync(x => x.Id == id);
            var model = new CartRuleVm {
                Id = catrtRule.Id,
                Name = catrtRule.Name,
                Description = catrtRule.Description,
                IsActive = catrtRule.IsActive,
                StartOn = catrtRule.StartOn,
                EndOn = catrtRule.EndOn,
                IsCouponRequired = catrtRule.IsCouponRequired,
                RuleToApply = catrtRule.RuleToApply,
                DiscountAmount = catrtRule.DiscountAmount,
                DiscountStep = catrtRule.DiscountStep,
                MaxDiscountAmount = catrtRule.MaxDiscountAmount,
                UsageLimitPerCoupon = catrtRule.UsageLimitPerCoupon,
                UsageLimitPerCustomer = catrtRule.UsageLimitPerCustomer,
                Products = catrtRule.Products.Select(x => new CartRuleProductVm { Id = x.ProductId, Name = x.Product.Name, IsPublished = x.Product.IsPublished }).ToList()
            };

            if (catrtRule.IsCouponRequired) {
                var coupon = catrtRule.Coupons.FirstOrDefault();
                if (coupon != null) {
                    model.CouponCode = coupon.Code;
                }
            }

            return Json(model);
        }
    }
}
