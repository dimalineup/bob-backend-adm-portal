﻿using BobCommerce.Module.Pricing.Areas.Pricing.ViewModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BobCommerce.Module.Pricing.ViewModels
{
    public class CartRuleForm {
        public CartRuleVm CartRule { get; set; }

        public IFormFile ThumbnailImage { get; set; }
    }
}
