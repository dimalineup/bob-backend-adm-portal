﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using BobCommerce.Infrastructure.Modules;
using BobCommerce.Module.Tax.Services;

namespace BobCommerce.Module.Tax
{
    public class ModuleInitializer : IModuleInitializer {
        public void ConfigureServices(IServiceCollection serviceCollection) {
            serviceCollection.AddTransient<ITaxService, TaxService>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env) {

        }
    }
}
