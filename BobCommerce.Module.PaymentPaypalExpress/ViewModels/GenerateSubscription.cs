﻿namespace BobCommerce.Module.PaymentPaypalExpress.ViewModels
{
    public class GenerateSubscription {
        public string seller_payme_id { get; set; }

        public decimal sale_price { get; set; }

        public string currency { get; set; }

        public string product_name { get; set; }

        public int installments { get; set; }

        public string sale_callback_url { get; set; }

        public string sale_return_url { get; set; }

        public int capture_buyer { get; set; }

        public string sale_type { get; set; }

        public string buyer_key { get; set; }

    }
}
