﻿namespace BobCommerce.Module.PaymentPaypalExpress.ViewModels
{
    public class GenerateSubscriptionCharge {
        public string seller_payme_id { get; set; }

        public decimal sale_price { get; set; }

        public string currency { get; set; }

        public string product_name { get; set; }

        public int installments { get; set; }

        public string sale_callback_url { get; set; }

        public string sale_return_url { get; set; }

        public string buyer_key { get; set; }

    }
}
