﻿using BobCommerce.Module.PaymentPaypalExpress.Areas.PaymentPaypalExpress.Controllers;

namespace BobCommerce.Module.PaymentPaypalExpress.ViewModels
{
    public class IsracardCreditCardVm {
        public int UserID { get; set; }

        public string CouponCode { get; set; }

        public decimal Tips { get; set; }

        public int Remember { get; set; }

        public CreditCardItem Card { get; set; }
    }
}
