﻿using System;

namespace BobCommerce.Module.PaymentPaypalExpress.ViewModels
{
    public class Callback {
        public int status_code { get; set; }
        public string status_error_code { get; set; }
        public string status_error_details { get; set; }
        public string notify_type { get; set; }
        public DateTime sale_created { get; set; }
        public string transaction_id { get; set; }
        public string payme_sale_id { get; set; }
        public string payme_sale_code { get; set; }
        public string payme_transaction_id { get; set; }
        public decimal price { get; set; }
        public string currency { get; set; }
        public string sale_status { get; set; }
        public string payme_transaction_card_brand { get; set; }
        public string payme_transaction_auth_number { get; set; }
        public string buyer_card_mask { get; set; }
        public string buyer_card_exp { get; set; }
        public string buyer_name { get; set; }
        public string buyer_email { get; set; }
        public string buyer_phone { get; set; }
        public string buyer_social_id { get; set; }
        public string buyer_key { get; set; }
        public int installments { get; set; }
        public DateTime sale_paid_date { get; set; }
        public DateTime sale_release_date { get; set; }
        public bool is_token_sale { get; set; }
        public string payme_signature { get; set; }
        public string sale_invoice_url { get; set; }
    }
}
