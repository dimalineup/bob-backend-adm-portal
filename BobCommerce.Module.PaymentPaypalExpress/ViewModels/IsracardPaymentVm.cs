﻿namespace BobCommerce.Module.PaymentPaypalExpress.ViewModels
{
    public class IsracardPaymentVm {
        public string payme_signature { get; set; }

        public string payme_sale_id { get; set; }

        public string payme_transaction_id { get; set; }
    }
}
