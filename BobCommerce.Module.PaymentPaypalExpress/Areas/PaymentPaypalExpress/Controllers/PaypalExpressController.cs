﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BobCommerce.Infrastructure.Data;
using BobCommerce.Infrastructure.Extensions;
using BobCommerce.Module.Core.Extensions;
using BobCommerce.Module.Orders.Models;
using BobCommerce.Module.Orders.Services;
using BobCommerce.Module.PaymentPaypalExpress.Models;
using BobCommerce.Module.PaymentPaypalExpress.ViewModels;
using BobCommerce.Module.Payments.Models;
using BobCommerce.Module.ShoppingCart.Services;
using System.IO;
using BobCommerce.Module.ShoppingCart.Models;
using Microsoft.EntityFrameworkCore;
using BobCommerce.Module.Core.Models;
using System.Collections.Generic;
using BobCommerce.Module.Pricing.Models;
using Microsoft.Extensions.Configuration;

namespace BobCommerce.Module.PaymentPaypalExpress.Areas.PaymentPaypalExpress.Controllers
{

    public class CreditCardItem {
        public string buyer_card_mask { get; set; }
        public string buyer_card_exp { get; set; }
        public string buyer_name { get; set; }
    }

    public class LinkItem {
        public string href { get; set; }
        public string rel { get; set; }
        public string method { get; set; }
    }

    public class GenerateSaleItem {
        public int status_code { get; set; }
        public string sale_url { get; set; }
        public string payme_sale_id { get; set; }
        public string payme_sale_code { get; set; }
        public int metpricehod { get; set; }
        public string transaction_id { get; set; }
        public string currency { get; set; }
        public long order_id { get; set; }
        public string done { get; set; }
    }

    public class GenerateSaleChargeItem {
        public int status_code { get; set; }
        public string payme_sale_id { get; set; }
        public string payme_sale_code { get; set; }
        public decimal price { get; set; }
        public string transaction_id { get; set; }
        public string currency { get; set; }
        public string sale_payment_method { get; set; }
        public int status_error_code { get; set; }
        public string status_error_details { get; set; }
        public string redirect_url { get; set; }
        public string transaction_cc_auth_number { get; set; }
        public string payme_transaction_auth_number { get; set; }
        public string sale_status { get; set; }
        public string payme_status { get; set; }
        public string sale_created { get; set; }
        public string payme_sale_status { get; set; }
        public bool is_token_sale { get; set; }
        public string payme_signature { get; set; }
        public string payme_transaction_id { get; set; }
        public string payme_transaction_total { get; set; }
        public string payme_transaction_card_brand { get; set; }
        public string payme_transaction_voucher { get; set; }
        public string buyer_name { get; set; }
        public string buyer_email { get; set; }
        public string buyer_phone { get; set; }
        public string buyer_card_mask { get; set; }
        public string buyer_card_exp { get; set; }
        public string buyer_card_is_foreign { get; set; }
        public string buyer_social_id { get; set; }
        public int installments { get; set; }
        public string sale_paid_date { get; set; }
        public string sale_release_date { get; set; }
        public long order_id { get; set; }

    }

    [Area("PaymentPaypalExpress")]
    //[ApiExplorerSettings(IgnoreApi = true)]
    public class PaypalExpressController : Controller
    {
        private const string IsracardSellerPaymeID = "MPL15629-2227404M-F01PR6TN-AQGTC6HJ";
        private const string IsracardCurrency = "ILS";
        private const string IsracardProductName = "Bob Order";
        private const int IsracardInstallments = 1;
        private const string IsracardReturnUrl = "http://bob.justplus.eu/app/index.html";
        private const string IsracardGenerateSaleUrl = "https://preprod.paymeservice.com/api/generate-sale";
        private readonly ICartService _cartService;
        private readonly IOrderService _orderService;
        private readonly IWorkContext _workContext;
        private readonly IRepositoryWithTypedId<PaymentProvider, string> _paymentProviderRepository;
        private readonly IRepository<Payment> _paymentRepository;
        private Lazy<PaypalExpressConfigForm> _setting;
        private readonly IHttpClientFactory _httpClientFactory;
        //private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<Cart> _cartRepository;
        private readonly IRepository<Vendor> _vendorRepository;
        private readonly IRepository<CartRule> _cartRuleRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Order> _orderRepository;
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;

        public PaypalExpressController(
            ICartService cartService,
            IOrderService orderService,
            IWorkContext workContext,
            IRepositoryWithTypedId<PaymentProvider, string> paymentProviderRepository,
            IRepository<Payment> paymentRepository,
            IHttpClientFactory httpClientFactory/*, IRepository<Order> orderRepository*/, 
            IRepository<Cart> cartRepository, 
            IRepository<Vendor> vendorRepository, 
            IRepository<CartRule> cartRuleRepository,
            IRepository<User> userRepository,
            IRepository<Order> orderRepository,
            IConfiguration config)
        {
            _cartService = cartService;
            _orderService = orderService;
            _workContext = workContext;
            _paymentProviderRepository = paymentProviderRepository;
            _paymentRepository = paymentRepository;
            _setting = new Lazy<PaypalExpressConfigForm>(GetSetting());
            _httpClientFactory = httpClientFactory;
            //_orderRepository = orderRepository;
            _cartRepository = cartRepository;
            _vendorRepository = vendorRepository;
            _cartRuleRepository = cartRuleRepository;
            _httpClient = new HttpClient();
            _userRepository = userRepository;
            _orderRepository = orderRepository;
            _configuration = config;
        }

        [HttpPost("PaypalExpress/CreatePayment/{userId}")]
        public async Task<IActionResult> CreatePayment(int userId) {
            var hostingDomain = Request.Host.Value;
            var accessToken = await GetAccessToken();
            //var currentUser = await _workContext.GetCurrentUser();
            var cart = await _cartService.GetActiveCartDetails(userId);
            if (cart == null) {
                return NotFound();
            }

            var cartRule = _cartRuleRepository.Query().Include(x => x.Coupons).Where(x => x.StartOn <= DateTime.Now && x.EndOn >= DateTime.Now).FirstOrDefault();
            if (cartRule != null) {
                var validationResult = await _cartService.ApplyCoupon(cart.Id, cartRule.Coupons.FirstOrDefault().Code);
                if (validationResult.Succeeded) {
                    WriteToLog("PaypalExpress CreatePayment - Discount applied successfully");

                }
            }

            var regionInfo = new RegionInfo(CultureInfo.CurrentCulture.LCID);
            var experienceProfileId = await CreateExperienceProfile(accessToken);

            var httpClient = _httpClientFactory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var paypalAcceptedNumericFormatCulture = CultureInfo.CreateSpecificCulture("en-US");

            Item_List itemList = new Item_List();
            
            if (cart.LocationDiscount > 0) {
                itemList.items = new Item[2];

                Item orderItem = new Item();
                orderItem.name = "Order";
                orderItem.quantity = "1";
                orderItem.price = (cart.OrderTotal + cart.LocationDiscount).ToString("N2", paypalAcceptedNumericFormatCulture);
                orderItem.currency = regionInfo.ISOCurrencySymbol;
                itemList.items[0] = orderItem;

                Item discItem = new Item();
                discItem.name = "Discount";
                discItem.quantity = "1";
                discItem.price = (-cart.LocationDiscount).ToString("N2", paypalAcceptedNumericFormatCulture);
                discItem.currency = regionInfo.ISOCurrencySymbol;
                itemList.items[1] = discItem;
            }
            else {

                itemList.items = new Item[1];

                Item orderItem = new Item();
                orderItem.name = "Order";
                orderItem.quantity = "1";
                orderItem.price = (cart.OrderTotal).ToString("N2", paypalAcceptedNumericFormatCulture);
                orderItem.currency = regionInfo.ISOCurrencySymbol;
                itemList.items[0] = orderItem;
            }

            var paymentCreateRequest = new PaymentCreateRequest
            {
                experience_profile_id = experienceProfileId,
                intent = "sale",
                payer = new Payer
                {
                    payment_method = "paypal"
                },
                transactions = new Transaction[]
                {
                    new Transaction {
                        amount = new Amount
                        {
                            total = (cart.OrderTotal + CalculatePaymentFee(cart.OrderTotal)).ToString("N2", paypalAcceptedNumericFormatCulture),
                            currency = regionInfo.ISOCurrencySymbol,
                            details = new Details
                            {
                                handling_fee = CalculatePaymentFee(cart.OrderTotal).ToString("N2", paypalAcceptedNumericFormatCulture),
                                subtotal = (cart.OrderTotal + CalculatePaymentFee(cart.OrderTotal)).ToString("N2", paypalAcceptedNumericFormatCulture),
                                tax = cart.TaxAmount?.ToString("N2", paypalAcceptedNumericFormatCulture) ?? "0",
                                shipping = cart.ShippingAmount?.ToString("N2", paypalAcceptedNumericFormatCulture) ?? "0"
                            }
                        }
                        , item_list = itemList
                    }
                },
                redirect_urls = new Redirect_Urls
                {
                    cancel_url = $"https://gtbob.com/paypalexpresscancel", //Haven't seen it being used anywhere
                    return_url = $"https://gtbob.com/paypalexpresssuccess", //Haven't seen it being used anywhere
                }
            };

            var response = await httpClient.PostJsonAsync($"https://api{_setting.Value.EnvironmentUrlPart}.paypal.com/v1/payments/payment", paymentCreateRequest);
            var responseBody = await response.Content.ReadAsStringAsync();
            dynamic payment = JObject.Parse(responseBody);
            if (response.IsSuccessStatusCode)
            {
                string paymentId = payment.id;
                LinkItem[] links = payment.links.ToObject<LinkItem[]>();
                string approvalUrl = string.Empty;
                foreach(LinkItem link in links) {
                    if(link.rel == "approval_url") {
                        approvalUrl = link.href;
                    }
                }
                //return Ok(new { PaymentId = paymentId, ApprovalUrl = approvalUrl });
                WriteToLog("PaypalExpress CreatePayment - PaymentId: " + paymentId + " ApprovalUrl: " + approvalUrl);
                return Json(new { PaymentId = paymentId, ApprovalUrl = approvalUrl });
                //return Json(approvalUrl);
            }

            return BadRequest(responseBody);
        }

        [HttpPost("PaypalExpress/ExecutePayment/{userId}")]
        public async Task<ActionResult> ExecutePayment([FromBody] PaymentExecuteVm model, int userId)
        {
            //var existing = _orderRepository.Query().Where(x => x.PayerID == model.payerID && x.PaymentID == model.paymentID).FirstOrDefault();
            //if(existing != null) {

            //    //do nothing, just return the id
            //    return Ok(new { Status = "success", OrderId = existing.Id });
            //}
            var accessToken = await GetAccessToken();
            var currentUser = await _workContext.GetCurrentUser();
            //var cart = await _cartService.GetActiveCartDetails(currentUser.Id);
            var cart = await _cartService.GetActiveCartDetails(userId);

            var orderCreateResult = await _orderService.CreateOrder(cart.Id, "PaypalExpress", CalculatePaymentFee(cart.OrderTotal), model.payerID, model.paymentID, OrderStatus.PendingPayment);
            if (!orderCreateResult.Success)
            {
                return BadRequest(orderCreateResult.Error);
            }

            var order = orderCreateResult.Value;

            var payment = new Payment()
            {
                OrderId = order.Id,
                PaymentFee = order.PaymentFeeAmount,
                Amount = order.OrderTotal,
                PaymentMethod = "Paypal Express",
                CreatedOn = DateTimeOffset.UtcNow,
            };

            var httpClient = _httpClientFactory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var paymentExecuteRequest = new PaymentExecuteRequest
            {
                payer_id = model.payerID
            };

            var response = await httpClient.PostJsonAsync($"https://api{_setting.Value.EnvironmentUrlPart}.paypal.com/v1/payments/payment/{model.paymentID}/execute", paymentExecuteRequest);

            WriteToLog("PaypalExpress ExecutePayment - PaymentId: " + model.paymentID + " PayerID: " + model.payerID);

            var responseBody = await response.Content.ReadAsStringAsync();
            dynamic responseObject = JObject.Parse(responseBody);
            if (response.IsSuccessStatusCode)
            {
                // Has to explicitly declare the type to be able to get the propery
                string payPalPaymentId = responseObject.id;
                payment.Status = PaymentStatus.Succeeded;
                payment.GatewayTransactionId = payPalPaymentId;
                _paymentRepository.Add(payment);
                order.OrderStatus = OrderStatus.PaymentReceived;
                await _paymentRepository.SaveChangesAsync();
                return Ok(new { Status = "success", OrderId = order.Id });
            }

            payment.Status = PaymentStatus.Failed;
            payment.FailureMessage = responseBody;
            _paymentRepository.Add(payment);
            order.OrderStatus = OrderStatus.PaymentFailed;
            await _paymentRepository.SaveChangesAsync();

            string errorName = responseObject.name;
            string errorDescription = responseObject.message;
            return BadRequest($"{errorName} - {errorDescription}");
        }

        private async Task<string> GetAccessToken()
        {
            var httpClient = _httpClientFactory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                "Basic",
                Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes($"{_setting.Value.ClientId}:{_setting.Value.ClientSecret}")));
            var requestBody = new StringContent("grant_type=client_credentials");
            requestBody.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            var response = await httpClient.PostAsync($"https://api{_setting.Value.EnvironmentUrlPart}.paypal.com/v1/oauth2/token", requestBody);
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();
            dynamic token = JObject.Parse(responseBody);
            string accessToken = token.access_token;
            return accessToken;
        }

        private async Task<string> CreateExperienceProfile(string accessToken)
        {
            var httpClient = _httpClientFactory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var experienceRequest = new ExperienceProfile
            {
                name = $"simpl_{Guid.NewGuid()}",
                input_fields = new InputFields
                {
                    no_shipping = 1
                },
                temporary = true
            };
            var response = await httpClient.PostJsonAsync($"https://api{_setting.Value.EnvironmentUrlPart}.paypal.com/v1/payment-experience/web-profiles", experienceRequest);
            var responseBody = await response.Content.ReadAsStringAsync();
            dynamic experience = JObject.Parse(responseBody);
            // Has to explicitly declare the type to be able to get the propery
            string profileId = experience.id;
            return profileId;
        }

        private decimal CalculatePaymentFee(decimal total)
        {
            var percent = _setting.Value.PaymentFee;
            return (total / 100) * percent;
        }

        private PaypalExpressConfigForm GetSetting()
        {
            var paypalExpressProvider = _paymentProviderRepository.Query().FirstOrDefault(x => x.Id == PaymentProviderHelper.PaypalExpressProviderId);
            var paypalExpressSetting = JsonConvert.DeserializeObject<PaypalExpressConfigForm>(paypalExpressProvider.AdditionalSettings);
            return paypalExpressSetting;
        }

        public static void WriteToLog(string log) {
            try {
                string path = Path.Combine(Directory.GetCurrentDirectory(), "applogs", DateTime.Now.ToString("yyyy-MM-dd_") + "log.txt");
                System.IO.File.AppendAllText(path, DateTime.Now + "\r\n" + log + "\r\n");
            }
            catch (Exception ex) { }
        }

        //[HttpPost("Isracard/CreatePayment/{userId}/{couponCode}/{tips}/remember")]
        //public async Task<IActionResult> IsracardCreatePayment(int userId, string couponCode, decimal tips, int remember) {
        //    var cart = await _cartService.GetActiveCartDetailsWithTips(userId, tips);
        //    if (cart == null) {
        //        return NotFound();
        //    }

        //    if (!string.IsNullOrEmpty(couponCode)) {
        //        var validationResult = await _cartService.ApplyCoupon(cart.Id, couponCode);
        //        if (validationResult.Succeeded) {
        //            WriteToLog("Isracard CreatePayment - Discount applied successfully");

        //        }
        //    }

        //    var user = await _userRepository.Query().Where(x => x.Id == userId).FirstOrDefaultAsync();

        //    //create the order
        //    var orderCreateResult = await _orderService.CreateOrderIsracard(cart.Id, "Isracard", CalculatePaymentFee(cart.OrderTotal), tips, OrderStatus.New);
        //    if (!orderCreateResult.Success) {
        //        return BadRequest(orderCreateResult.Error);
        //    }
        //    var order = orderCreateResult.Value;

        //    GenerateSubscription model = new GenerateSubscription();
        //    model.seller_payme_id = IsracardSellerPaymeID;
        //    model.sale_price = cart.OrderTotal * 100;
        //    model.currency = IsracardCurrency;
        //    model.product_name = IsracardProductName;
        //    model.installments = IsracardInstallments;

        //    model.sale_callback_url = _configuration["MySettings:AbsoluteUrl"] + "/Isracard?page=isracard&orderId=" + order.Id + "&remember=" + remember;
        //    model.sale_return_url = IsracardReturnUrl;
        //    //model.capture_buyer = 1;
        //    model.sale_type = "token";
        //    var response = await _httpClient.PostJsonAsync(IsracardGenerateSaleUrl, model);
        //    response.EnsureSuccessStatusCode();
        //    var data = response.Content.ReadAsStringAsync();
        //    dynamic generateSale = JObject.Parse(data.Result);
        //    GenerateSaleItem item = generateSale.ToObject<GenerateSaleItem>();

        //    if(item != null) {

        //        order.PaymentID = item.payme_sale_id;
        //        order.PayerID = item.transaction_id;

        //    }
        //    if (user != null) {
        //        item.sale_url = item.sale_url + "?first_name=" + user.FullName + "&last_name=" + user.FullName + "&phone=" + user.PhoneNumber + "&email=" + user.Email;
        //    }

        //    item.order_id = order.Id;

        //    return Json(item);
        //}

        [HttpPost("Isracard/CreatePayment")]
        public async Task<IActionResult> IsracardCreatePayment([FromBody] IsracardCreditCardVm creditCardVm) {

            var cart = await _cartService.GetActiveCartDetailsWithTips(creditCardVm.UserID, creditCardVm.Tips);
            if (cart == null) {
                return NotFound();
            }

            if (!string.IsNullOrEmpty(creditCardVm.CouponCode)) {
                var validationResult = await _cartService.ApplyCoupon(cart.Id, creditCardVm.CouponCode);
                if (validationResult.Succeeded) {
                    WriteToLog("Isracard CreatePayment - Discount applied successfully");

                }
            }

            var user = await _userRepository.Query().Where(x => x.Id == creditCardVm.UserID).FirstOrDefaultAsync();

            //create the order
            var orderCreateResult = await _orderService.CreateOrderIsracard(cart.Id, "Isracard", CalculatePaymentFee(cart.OrderTotal), creditCardVm.Tips, OrderStatus.New);
            if (!orderCreateResult.Success) {
                return BadRequest(orderCreateResult.Error);
            }
            var order = orderCreateResult.Value;

            //if we have a credit card saved that the user wants to use, get the buyer_key 
            string buyer_key = "";
            if (creditCardVm.Card != null && (!string.IsNullOrEmpty(creditCardVm.Card.buyer_card_exp) && !string.IsNullOrEmpty(creditCardVm.Card.buyer_card_mask) && !string.IsNullOrEmpty(creditCardVm.Card.buyer_name))) {
                var orders = await _orderRepository.Query().Where(x => x.CustomerId == creditCardVm.UserID && x.Callback != null).ToListAsync();
                if (orders != null) {
                    foreach (var itm in orders) {
                        Callback cardSaved = JsonConvert.DeserializeObject<Callback>(itm.Callback);
                        if (cardSaved.buyer_card_exp == creditCardVm.Card.buyer_card_exp && cardSaved.buyer_card_mask == creditCardVm.Card.buyer_card_mask && cardSaved.buyer_name == creditCardVm.Card.buyer_name) {
                            buyer_key = itm.BuyerKey;
                            break;
                        }
                    }
                }

                GenerateSubscriptionCharge model = new GenerateSubscriptionCharge();
                model.seller_payme_id = IsracardSellerPaymeID;
                model.sale_price = order.OrderTotal * 100;
                model.currency = IsracardCurrency;
                model.product_name = IsracardProductName;
                model.installments = IsracardInstallments;
                model.sale_callback_url = "";
                model.sale_return_url = IsracardReturnUrl;
                model.buyer_key = buyer_key;
                var response = await _httpClient.PostJsonAsync(IsracardGenerateSaleUrl, model);
                response.EnsureSuccessStatusCode();
                var data = await response.Content.ReadAsStringAsync();
                dynamic generateSale = JObject.Parse(data);
                order.BuyerKey = buyer_key;
                if (response.IsSuccessStatusCode) {
                    GenerateSaleItem item = generateSale.ToObject<GenerateSaleItem>();
                    string payPalPaymentId = item.payme_sale_id;
                    order.OrderStatus = OrderStatus.PendingPayment;
                    await _orderRepository.SaveChangesAsync();
                    item.order_id = order.Id;
                    item.done = "true";
                    //return Ok(new { Status = "success", OrderId = order.Id });
                    return Json(item);
                }
                
                order.OrderStatus = OrderStatus.PaymentFailed;
                await _orderRepository.SaveChangesAsync();

                string errorName = generateSale.name;
                string errorDescription = generateSale.message;
                return BadRequest($"{errorName} - {errorDescription}");
            }
            else {
                GenerateSubscription model = new GenerateSubscription();
                model.seller_payme_id = IsracardSellerPaymeID;
                model.sale_price = cart.OrderTotal * 100;
                model.currency = IsracardCurrency;
                model.product_name = IsracardProductName;
                model.installments = IsracardInstallments;

                model.sale_callback_url = _configuration["MySettings:AbsoluteUrl"] + "/Isracard?page=isracard&orderId=" + order.Id + "&remember=" + creditCardVm.Remember;
                model.sale_return_url = IsracardReturnUrl;
                //model.capture_buyer = 1;
                model.sale_type = "token";
                var response = await _httpClient.PostJsonAsync(IsracardGenerateSaleUrl, model);
                response.EnsureSuccessStatusCode();
                var data = response.Content.ReadAsStringAsync();
                dynamic generateSale = JObject.Parse(data.Result);
                GenerateSaleItem item = generateSale.ToObject<GenerateSaleItem>();

                if (item != null) {

                    order.PaymentID = item.payme_sale_id;
                    order.PayerID = item.transaction_id;

                }
                if (user != null) {
                    item.sale_url = item.sale_url + "?first_name=" + user.FullName + "&last_name=" + user.FullName + "&phone=" + user.PhoneNumber + "&email=" + user.Email;
                }

                item.order_id = order.Id;

                return Json(item);
            }
        }

        [HttpPost("Isracard/ExecutePayment/{userId}/{orderId}")]
        public async Task<IActionResult> IsracardExecutePayment([FromBody] IsracardPaymentVm model, long userId, long orderId) {

            var order = await _orderRepository.Query().Where(x => x.Id == orderId).FirstOrDefaultAsync();

            if(order != null) {
                if (!string.IsNullOrEmpty(model.payme_sale_id) && !string.IsNullOrEmpty(model.payme_signature) && !string.IsNullOrEmpty(model.payme_transaction_id)) {
                    if (order.OrderStatus == OrderStatus.New && order.CustomerId == userId) {
                        order.OrderStatus = OrderStatus.PendingPayment;
                        
                        await _orderRepository.SaveChangesAsync();
                        return Ok(new { Status = "success", OrderId = order.Id });
                    }
                }
                else {
                    order.OrderStatus = OrderStatus.PaymentFailed;
                    await _orderRepository.SaveChangesAsync();
                    return Ok(new { Status = "error", OrderId = order.Id });
                }
            }

            string errorName = "error";
            string errorDescription = "error";
            return BadRequest($"{errorName} - {errorDescription}");
        }

        [HttpPost]
        [Route("Isracard")]
        public async Task<IActionResult> Isracard([FromForm] Callback model, string page, long orderId, int remember) {
            string callbackJson = string.Empty;
            if (page == "isracard" && model != null) {
                callbackJson = JsonConvert.SerializeObject(model);
                WriteToLog("Isracard callback: " + callbackJson);

                var order = _orderRepository.Query().Where(x => x.Id == orderId).FirstOrDefault();

                if(order != null) {
                    order.BuyerKey = model.buyer_key;
                    if (remember == 1) {
                        order.Callback = callbackJson;
                    }
                    await _orderRepository.SaveChangesAsync();
                }
            }
            return Ok(orderId);
        }

        [HttpPost("Isracard/ChargeBuyer/{orderId}")]
        public async Task<IActionResult> IsracardChargeBuyer(int orderId) {

            var order = _orderRepository.Query().Where(x => x.Id == orderId).FirstOrDefault();

            var payment = new Payment() {
                OrderId = orderId,
                PaymentFee = order.PaymentFeeAmount,
                Amount = order.OrderTotal,
                PaymentMethod = "Isracard",
                CreatedOn = DateTimeOffset.UtcNow,
            };

            GenerateSubscriptionCharge model = new GenerateSubscriptionCharge();
            model.seller_payme_id = IsracardSellerPaymeID;
            model.sale_price = order.OrderTotal * 100;
            model.currency = IsracardCurrency;
            model.product_name = IsracardProductName;
            model.installments = IsracardInstallments;
            model.sale_callback_url = "";
            model.sale_return_url = IsracardReturnUrl;
            model.buyer_key = order.BuyerKey;
            var response = await _httpClient.PostJsonAsync(IsracardGenerateSaleUrl, model);
            response.EnsureSuccessStatusCode();
            var data = await response.Content.ReadAsStringAsync();
            dynamic generateSale = JObject.Parse(data);
            if (response.IsSuccessStatusCode) {
                GenerateSaleChargeItem item = generateSale.ToObject<GenerateSaleChargeItem>();
                string payPalPaymentId = item.payme_sale_id;
                payment.Status = PaymentStatus.Succeeded;
                payment.GatewayTransactionId = payPalPaymentId;
                _paymentRepository.Add(payment);
                order.OrderStatus = OrderStatus.PaymentReceived;
                await _orderRepository.SaveChangesAsync();
                await _paymentRepository.SaveChangesAsync();
                return Ok(new { Status = "success", OrderId = order.Id });
            }
            payment.Status = PaymentStatus.Failed;
            payment.FailureMessage = data;
            _paymentRepository.Add(payment);
            order.OrderStatus = OrderStatus.PaymentFailed;
            await _orderRepository.SaveChangesAsync();
            await _paymentRepository.SaveChangesAsync();

            string errorName = generateSale.name;
            string errorDescription = generateSale.message;
            return BadRequest($"{errorName} - {errorDescription}");
        }

        [HttpGet("Isracard/Cards/{userId}")]
        public async Task<IActionResult> IsracardGetSavedCards(long userId) {
            List<CreditCardItem> cards = new List<CreditCardItem>();
            var orders = await _orderRepository.Query().Where(x => x.CustomerId == userId && x.Callback != null).ToListAsync();
            if(orders != null) {
                foreach(var order in orders) {
                    Callback cardSaved = JsonConvert.DeserializeObject<Callback>(order.Callback);
                    CreditCardItem cardItem = new CreditCardItem();
                    cardItem.buyer_card_exp = cardSaved.buyer_card_exp;
                    cardItem.buyer_card_mask = cardSaved.buyer_card_mask;
                    cardItem.buyer_name = cardSaved.buyer_name;
                    cards.Add(cardItem);
                }
            }

            return Json(cards);
        }

        [HttpPost("PaypalExpress/Delivery/CreatePayment/{userId}")]
        public async Task<IActionResult> DeliveryCreatePayment(int userId) {
            var hostingDomain = Request.Host.Value;
            var accessToken = await GetAccessToken();
            //var currentUser = await _workContext.GetCurrentUser();
            var cart = await _cartService.GetActiveCartDetails(userId);
            if (cart == null) {
                return NotFound();
            }

            var cartRule = _cartRuleRepository.Query().Include(x => x.Coupons).Where(x => x.StartOn <= DateTime.Now && x.EndOn >= DateTime.Now).FirstOrDefault();
            if (cartRule != null) {
                var validationResult = await _cartService.ApplyCoupon(cart.Id, cartRule.Coupons.FirstOrDefault().Code);
                if (validationResult.Succeeded) {
                    WriteToLog("PaypalExpress CreatePayment - Discount applied successfully");

                }
            }

            var regionInfo = new RegionInfo(CultureInfo.CurrentCulture.LCID);
            var experienceProfileId = await CreateExperienceProfile(accessToken);

            var httpClient = _httpClientFactory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var paypalAcceptedNumericFormatCulture = CultureInfo.CreateSpecificCulture("en-US");

            Item_List itemList = new Item_List();

            if (cart.LocationDiscount > 0) {
                itemList.items = new Item[2];

                Item orderItem = new Item();
                orderItem.name = "Order";
                orderItem.quantity = "1";
                orderItem.price = (cart.OrderTotal + cart.LocationDiscount).ToString("N2", paypalAcceptedNumericFormatCulture);
                orderItem.currency = regionInfo.ISOCurrencySymbol;
                itemList.items[0] = orderItem;

                Item discItem = new Item();
                discItem.name = "Discount";
                discItem.quantity = "1";
                discItem.price = (-cart.LocationDiscount).ToString("N2", paypalAcceptedNumericFormatCulture);
                discItem.currency = regionInfo.ISOCurrencySymbol;
                itemList.items[1] = discItem;
            }
            else {

                itemList.items = new Item[1];

                Item orderItem = new Item();
                orderItem.name = "Order";
                orderItem.quantity = "1";
                orderItem.price = (cart.OrderTotal).ToString("N2", paypalAcceptedNumericFormatCulture);
                orderItem.currency = regionInfo.ISOCurrencySymbol;
                itemList.items[0] = orderItem;
            }

            var paymentCreateRequest = new PaymentCreateRequest {
                experience_profile_id = experienceProfileId,
                intent = "sale",
                payer = new Payer {
                    payment_method = "paypal"
                },
                transactions = new Transaction[]
                {
                    new Transaction {
                        amount = new Amount
                        {
                            total = (cart.OrderTotal + CalculatePaymentFee(cart.OrderTotal)).ToString("N2", paypalAcceptedNumericFormatCulture),
                            currency = regionInfo.ISOCurrencySymbol,
                            details = new Details
                            {
                                handling_fee = CalculatePaymentFee(cart.OrderTotal).ToString("N2", paypalAcceptedNumericFormatCulture),
                                subtotal = (cart.OrderTotal + CalculatePaymentFee(cart.OrderTotal)).ToString("N2", paypalAcceptedNumericFormatCulture),
                                tax = cart.TaxAmount?.ToString("N2", paypalAcceptedNumericFormatCulture) ?? "0",
                                shipping = cart.ShippingAmount?.ToString("N2", paypalAcceptedNumericFormatCulture) ?? "0"
                            }
                        }
                        , item_list = itemList
                    }
                },
                redirect_urls = new Redirect_Urls {
                    cancel_url = $"https://gtbob.com/paypalexpresscancel", //Haven't seen it being used anywhere
                    return_url = $"https://gtbob.com/paypalexpresssuccess", //Haven't seen it being used anywhere
                }
            };

            var response = await httpClient.PostJsonAsync($"https://api{_setting.Value.EnvironmentUrlPart}.paypal.com/v1/payments/payment", paymentCreateRequest);
            var responseBody = await response.Content.ReadAsStringAsync();
            dynamic payment = JObject.Parse(responseBody);
            if (response.IsSuccessStatusCode) {
                string paymentId = payment.id;
                LinkItem[] links = payment.links.ToObject<LinkItem[]>();
                string approvalUrl = string.Empty;
                foreach (LinkItem link in links) {
                    if (link.rel == "approval_url") {
                        approvalUrl = link.href;
                    }
                }
                //return Ok(new { PaymentId = paymentId, ApprovalUrl = approvalUrl });
                WriteToLog("PaypalExpress CreatePayment - PaymentId: " + paymentId + " ApprovalUrl: " + approvalUrl);
                return Json(new { PaymentId = paymentId, ApprovalUrl = approvalUrl });
                //return Json(approvalUrl);
            }

            return BadRequest(responseBody);
        }

        [HttpPost("PaypalExpress/Delivery/ExecutePayment/{userId}")]
        public async Task<ActionResult> DeliveryExecutePayment([FromBody] PaymentExecuteVm model, int userId) {
            var accessToken = await GetAccessToken();
            var currentUser = await _workContext.GetCurrentUser();
            var cart = await _cartService.GetActiveCartDetails(userId);

            var orderCreateResult = await _orderService.CreateOrderWithDelivery(cart.Id, "PaypalExpress", CalculatePaymentFee(cart.OrderTotal), model.payerID, model.paymentID, OrderStatus.PendingPayment);
            if (!orderCreateResult.Success) {
                return BadRequest(orderCreateResult.Error);
            }

            var order = orderCreateResult.Value;

            var payment = new Payment() {
                OrderId = order.Id,
                PaymentFee = order.PaymentFeeAmount,
                Amount = order.OrderTotal,
                PaymentMethod = "Paypal Express",
                CreatedOn = DateTimeOffset.UtcNow,
            };

            var httpClient = _httpClientFactory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var paymentExecuteRequest = new PaymentExecuteRequest {
                payer_id = model.payerID
            };

            var response = await httpClient.PostJsonAsync($"https://api{_setting.Value.EnvironmentUrlPart}.paypal.com/v1/payments/payment/{model.paymentID}/execute", paymentExecuteRequest);

            WriteToLog("PaypalExpress ExecutePayment - PaymentId: " + model.paymentID + " PayerID: " + model.payerID);

            var responseBody = await response.Content.ReadAsStringAsync();
            dynamic responseObject = JObject.Parse(responseBody);
            if (response.IsSuccessStatusCode) {
                // Has to explicitly declare the type to be able to get the propery
                string payPalPaymentId = responseObject.id;
                payment.Status = PaymentStatus.Succeeded;
                payment.GatewayTransactionId = payPalPaymentId;
                _paymentRepository.Add(payment);
                order.OrderStatus = OrderStatus.PaymentReceived;
                await _paymentRepository.SaveChangesAsync();
                return Ok(new { Status = "success", OrderId = order.Id });
            }

            payment.Status = PaymentStatus.Failed;
            payment.FailureMessage = responseBody;
            _paymentRepository.Add(payment);
            order.OrderStatus = OrderStatus.PaymentFailed;
            await _paymentRepository.SaveChangesAsync();

            string errorName = responseObject.name;
            string errorDescription = responseObject.message;
            return BadRequest($"{errorName} - {errorDescription}");
        }

        [HttpPost("Isracard/Delivery/CreatePayment")]
        public async Task<IActionResult> DeliveryIsracardCreatePayment([FromBody] IsracardCreditCardVm creditCardVm) {

            var cart = await _cartService.GetActiveCartDetailsWithTips(creditCardVm.UserID, creditCardVm.Tips);
            if (cart == null) {
                return NotFound();
            }

            if (!string.IsNullOrEmpty(creditCardVm.CouponCode)) {
                var validationResult = await _cartService.ApplyCoupon(cart.Id, creditCardVm.CouponCode);
                if (validationResult.Succeeded) {
                    WriteToLog("Isracard CreatePayment - Discount applied successfully");

                }
            }

            var user = await _userRepository.Query().Where(x => x.Id == creditCardVm.UserID).FirstOrDefaultAsync();

            //create the order
            var orderCreateResult = await _orderService.CreateOrderIsracardWithDelivery(cart.Id, "Isracard", CalculatePaymentFee(cart.OrderTotal), creditCardVm.Tips, OrderStatus.New);
            if (!orderCreateResult.Success) {
                return BadRequest(orderCreateResult.Error);
            }
            var order = orderCreateResult.Value;

            //if we have a credit card saved that the user wants to use, get the buyer_key 
            string buyer_key = "";
            if (creditCardVm.Card != null && (!string.IsNullOrEmpty(creditCardVm.Card.buyer_card_exp) && !string.IsNullOrEmpty(creditCardVm.Card.buyer_card_mask) && !string.IsNullOrEmpty(creditCardVm.Card.buyer_name))) {
                var orders = await _orderRepository.Query().Where(x => x.CustomerId == creditCardVm.UserID && x.Callback != null).ToListAsync();
                if (orders != null) {
                    foreach (var itm in orders) {
                        Callback cardSaved = JsonConvert.DeserializeObject<Callback>(itm.Callback);
                        if (cardSaved.buyer_card_exp == creditCardVm.Card.buyer_card_exp && cardSaved.buyer_card_mask == creditCardVm.Card.buyer_card_mask && cardSaved.buyer_name == creditCardVm.Card.buyer_name) {
                            buyer_key = itm.BuyerKey;
                            break;
                        }
                    }
                }

                GenerateSubscriptionCharge model = new GenerateSubscriptionCharge();
                model.seller_payme_id = IsracardSellerPaymeID;
                model.sale_price = order.OrderTotal * 100;
                model.currency = IsracardCurrency;
                model.product_name = IsracardProductName;
                model.installments = IsracardInstallments;
                model.sale_callback_url = "";
                model.sale_return_url = IsracardReturnUrl;
                model.buyer_key = buyer_key;
                var response = await _httpClient.PostJsonAsync(IsracardGenerateSaleUrl, model);
                response.EnsureSuccessStatusCode();
                var data = await response.Content.ReadAsStringAsync();
                dynamic generateSale = JObject.Parse(data);
                order.BuyerKey = buyer_key;
                if (response.IsSuccessStatusCode) {
                    GenerateSaleItem item = generateSale.ToObject<GenerateSaleItem>();
                    string payPalPaymentId = item.payme_sale_id;
                    order.OrderStatus = OrderStatus.PendingPayment;
                    await _orderRepository.SaveChangesAsync();
                    item.order_id = order.Id;
                    item.done = "true";
                    //return Ok(new { Status = "success", OrderId = order.Id });
                    return Json(item);
                }

                order.OrderStatus = OrderStatus.PaymentFailed;
                await _orderRepository.SaveChangesAsync();

                string errorName = generateSale.name;
                string errorDescription = generateSale.message;
                return BadRequest($"{errorName} - {errorDescription}");
            }
            else {
                GenerateSubscription model = new GenerateSubscription();
                model.seller_payme_id = IsracardSellerPaymeID;
                model.sale_price = cart.OrderTotal * 100;
                model.currency = IsracardCurrency;
                model.product_name = IsracardProductName;
                model.installments = IsracardInstallments;

                model.sale_callback_url = _configuration["MySettings:AbsoluteUrl"] + "/Isracard?page=isracard&orderId=" + order.Id + "&remember=" + creditCardVm.Remember;
                model.sale_return_url = IsracardReturnUrl;
                //model.capture_buyer = 1;
                model.sale_type = "token";
                var response = await _httpClient.PostJsonAsync(IsracardGenerateSaleUrl, model);
                response.EnsureSuccessStatusCode();
                var data = response.Content.ReadAsStringAsync();
                dynamic generateSale = JObject.Parse(data.Result);
                GenerateSaleItem item = generateSale.ToObject<GenerateSaleItem>();

                if (item != null) {

                    order.PaymentID = item.payme_sale_id;
                    order.PayerID = item.transaction_id;

                }
                if (user != null) {
                    item.sale_url = item.sale_url + "?first_name=" + user.FullName + "&last_name=" + user.FullName + "&phone=" + user.PhoneNumber + "&email=" + user.Email;
                }

                item.order_id = order.Id;

                return Json(item);
            }
        }

    }
}
